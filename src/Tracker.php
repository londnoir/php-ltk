<?php

	/*
	* Libraries/php/LTK/Tracker.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	use PDO;

	/**
	* Permet de suivre des statistiques sur les visiteurs et les membres.
	* Cette classe tient à jour un log sur les connexions entrantes sur le site, les IP, d'où le visiteur vient,
	* par quelle URL il est arrivé, ... Et on peut aussi rajouter des données personnalisées.
	* Basé sur les IP, il faut vider la table une fois par jour en transformant les données pour un résultat fiable.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class Tracker
	{
		const CookieAName = 'ltk_tracker_a';
		const CookieBName = 'ltk_tracker_b';

		const TableNameKey = 'tracker_table_name';
		const DefaultTableName = 'ltk_trackers';

		private Database $database;
		/* Check if we need to update the data base. */
		private bool $isUpToDate = false;
		private array $data = [
			'tracker_id' => '',
			'c_date' => 0,
			'ip' => 0,
			'entry_point' => '',
			'referer' => '',
			'is_member' => false,
			'is_saved' => false
		];
		private array $customData = [];
		
		/**
		 * Le constructeur analyse les données du client pour l'identifier ou créer un nouveau tracker.
		 *
		 * @param Database $database Un accès actif à la base de données.
		 */
		public function  __construct (Database $database)
		{
			$this->database = $database;

			if ( !$this->database->isConnected() )
			{
				trigger_error('Tracker need a connected database !', E_USER_WARNING);

				return;
			}

			/* If no remote IP, tracker is useless. */
			if ( !isset($_SERVER['REMOTE_ADDR']) )
				return;
			
			/* Previous visit recorded. */
			if ( $this->decodeData() )
			{
				$this->isUpToDate = true;

				return;
			}

			/* New visitor. */
			$this->data['tracker_id'] = md5($_SERVER['REMOTE_ADDR']);
			
			$query = 
				'SELECT `c_date`, `ip`, `entry_point`, `referer`, `is_member`, `custom_data` '.
				'FROM `trackers` '.
				'WHERE `tracker_id` = "'.$this->data['tracker_id'].'" '.
				'LIMIT 1;';
			
			if ( $data = $this->database->getRow($query) )
			{
				$this->data['c_date'] = intval($data['c_date']);
				$this->data['ip'] = intval($data['ip']);
				$this->data['entry_point'] = $data['entry_point'];
				$this->data['referer'] = $data['referer'];
				$this->data['is_member'] = (bool)$data['is_member'];
				$this->data['is_saved'] = true;
				
				$this->customData = unserialize($data['custom_data']);
				
				$this->isUpToDate = true;
			}
			else
			{
				$this->data['c_date'] = time();
				$this->data['ip'] = ip2long($_SERVER['REMOTE_ADDR']);
				$this->data['entry_point'] = Path::getAbsoluteURL();
				$this->data['referer'] = (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'NO_REFERER';
			}
		}
		
		private function encodeData ()
		{
			$domain = '';

			if ( isset($_SERVER['SERVER_NAME']) && $tmp = explode('.', $_SERVER['SERVER_NAME']) )
				$domain = $tmp[1].'.'.$tmp[2];
			
			/* Encode the Tracker common data.
			 * Simplify. */
			$tmp = [];

			foreach ( $this->data as $value )
				$tmp[] = $value;
			
			$encoded = serialize($tmp);
			$encoded = base64_encode($encoded);
			
			setcookie(self::CookieAName, $encoded, time() + 86400, '/', $domain);
			
			/* Encode the Tracker custom data (Optionnal). */
			if ( !empty($this->customData) )
			{
				$encoded = serialize($this->customData);
				$encoded = base64_encode($encoded);

				setcookie(self::CookieBName, $encoded, time() + 86400, '/', $domain);
			}
		}

		/**
		 * Decode data from the cookie.
		 *
		 * @return bool
		 */
		private function decodeData (): bool
		{
			$raw = Tools::getValue(self::CookieAName, 'c');

			/* Decode the Tracker common data (if not exists, no previous tracker recorded) */
			if ( empty($raw) )
				return false;

			$tmp = unserialize(base64_decode($raw));
			
			/* Reconnect to data. */
			$i = 0;

			foreach ( $this->data as $key => $value )
			{
				$this->data[$key] = $tmp[$i];
				$i++;
			}
			
			/* Decode the Tracker custom data. */
			if ( $raw = Tools::getValue(self::CookieBName, 'c') )
				$this->customData = unserialize(base64_decode($raw));
			
			return true;
		}

		/**
		 * Saves the data in the database.
		 */
		private function save (): void
		{
			$customData = serialize($this->customData);

			$query = Database::writeInsertOrUpdateQuery('trackers', [
				'tracker_id' => $this->data['tracker_id'],
				'c_date' => $this->data['c_date'],
				'ip' => $this->data['ip'],
				'entry_point' => $this->data['entry_point'],
				'referer' => $this->data['referer'],
				'is_member' => $this->data['is_member'] ? 1 : 0,
				'custom_data' => $customData
			], [
				'm_date' => $this->data['m_date'],
				'is_member' => $this->data['is_member'] ? 1 : 0,
				'custom_data' => $customData
			]);

			if ( $this->database->execute($query) !== false )
			{
				$this->isUpToDate = true;
				$this->data['is_saved'] = true;
			}
		}
		
		/**
		 * Indique l'ID du tracker du client.
		 *
		 * @return string
		 */
		public function trackerID (): string
		{
			return $this->data['tracker_id'];
		}
		
		/**
		 * Indique la date de créaction du tracker du client.
		 *
		 * @return int
		 */
		public function creationDate (): int
		{
			return intval($this->data['c_date']);
		}

		/**
		 * Indique la date de créaction du tracker du client.
		 *
		 * @return int.
		 */
		public function lastModificationDate (): int
		{
			return intval($this->data['m_date']);
		}
		
		/**
		 * Indique l'adresse IP du client lors de sa première visite.
		 *
		 * @return int
		 */
		public function ip (): int
		{
			return intval($this->data['ip']);
		}
		
		/**
		 * Indique la première page de l'application visitée par le client.
		 *
		 * @return string
		 */
		public function entryPoint (): string
		{
			return $this->data['entry_point'];
		}
		
		/**
		 * Indique le site référant du client au moment de sa première arrivée sur l'application.
		 *
		 * @return string
		 */
		public function referer (): string
		{
			return $this->data['referer'];
		}
		
		/**
		 * Indique si le client est déclaré comme membre de l'application ou non.
		 *
		 * @return bool
		 */
		public function isMember (): bool
		{
			return (bool)$this->data['is_member'];
		}
		
		/** Déclare le client en cours comme membre de l'application. */
		public function setAsMember ()
		{
			if ( $this->data['is_member'] )
				return;

			$this->data['is_member'] = true;
			$this->isUpToDate = false;
		}
		
		/**
		 * Enregistre une information sur le client en cours.
		 *
		 * @param string $field Une chaîne de caractères contenant le nom de la variable
		 * @param string $value Une chaîne de caractères contenant la valeur de la variable
		 */
		public function setParameter (string $field, string $value)
		{			
			$this->customData[$field] = $value;
			$this->isUpToDate = false;
		}
		
		/**
		 * Retourne une information sur le client en cours.
		 *
		 * @param string $key Une chaîne de caractères contenant le nom de la variable.
		 * @return string
		 */
		public function parameter (string $key): string
		{
			if ( array_key_exists($key, $this->customData) )
				return $this->customData[$key];
			
			return '';
		}
		
		/**
		* Sauvegarde les données du Tracker en base de données et dans le cookie client.
		* Utiliser cette fonction avant d'envoyer des données au client.
		*/
		public function saveTracking ()
		{
			if ( $this->data['tracker_id'] )
			{
				if ( !$this->data['is_saved'] || !$this->isUpToDate )
					$this->save();
				
				$this->encodeData();
			}
		}

		/**
		 * Une méthode statique pour créer la table dans la base de données ou afficher la requête équivalente.
		 *
		 * @param PDO|null $database Un objet PDO. Par défaut à null.
		 * @param AbstractSettings|null $settings Un objet Settings. Par défaut à null.
		 * @return bool|string
		 */
		public static function buildTable (PDO $database = null, AbstractSettings $settings = null): bool|string
		{
			if ( is_null($settings) )
				$settings = StaticSettings::instance();

			$tableName = $settings->get(self::TableNameKey, self::DefaultTableName);

			$statement = 
				'CREATE TABLE IF NOT EXISTS `' . $tableName . '` ('.
				'`id` mediumint(8) NOT NULL AUTO_INCREMENT, '.
				'`tracker_id` varchar(32) NOT NULL, '.
				'`c_date` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`m_date` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`ip` int(10) unsigned NOT NULL DEFAULT "0", '.
				'`entry_point` varchar(255) NOT NULL, '.
				'`referer` varchar(255) NOT NULL, '.
				'`is_member` tinyint(1) unsigned NOT NULL DEFAULT "0", '.
				'`custom_data` text NOT NULL, '.
				'PRIMARY KEY (`id`), '.
				'UNIQUE KEY (`tracker_id`)'.
				') ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4';

			if ( is_null($database) )
				return $statement;

			if ( $database->exec($statement) !== false )
				return true;

			return false;
		}
	}
