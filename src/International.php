<?php

	/*
	* Libraries/php/LTK/International.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Série d'outil concernant des vérification internationalisées.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class International
	{
		/** @internal */
		private function __construct () {}

		/**
		 * Vérifie un numéro de registre belge.
		 *
		 * @param string $input Le numéro à vérifier.
		 * @return bool
		 */
		static public function checkBelgianNIR (string $input): bool
		{
			if ( strlen($input) !== 15 )
				return false;

			$number = intval(substr($input, 0, 13));
			$check  = intval(substr($input, 13, 2));

			if ( (97 - fmod($number, 97)) == $check )
				return true;

			if ( (97 - fmod(2000000000 + $number, 97)) == $check )
				return true;

			return false;
		}

		/**
		 * Vérifie un numéro de sécurité sociale belge.
		 *
		 * @param string $input Le numéro à vérifier.
		 * @return bool
		 */
		static public function checkBelgianSSN (string $input): bool
		{
			$input = preg_replace('#^([^0-9]*)$#', null, $input);

			if ( strlen($input) !== 11 )
				return false;

			$year   = substr($input, 0, 2);
			$month  = substr($input, 2, 2);
			$day    = substr($input, 4, 2);

			$number = intval(substr($input, 0, 9));
			$check  = intval(substr($input, 9, 2));

			$dateChecked = false;

			foreach ( array('19', '20', '21') as $prefix )
			{
				if ( checkdate(intval($month), intval($day), intval($prefix.$year)) )
				{
					$dateChecked = true;

					break;
				}
			}

			if ( $dateChecked == false )
				return false;

			if ( (97 - fmod($number, 97)) == $check )
				return true;

			if ( (97 - fmod(2000000000 + $number, 97)) == $check )
				return true;

			return false;
		}

		/**
		 * Vérifie un numéro de sécurité sociale français.
		 *
		 * @param string $input Le numéro à vérifier.
		 * @return bool
		 */
		static public function checkFrenchSSN (string $input): bool
		{
			if ( strlen($input) !== 12 )
				return false;

			$number = intval(substr($input, 0, 10));
			$check  = intval(substr($input, 10, 2));

			if ( (10 - fmod($number, 10)) == $check )
				return true;

			if ( (10 - fmod(2000000000 + $number, 10)) == $check )
				return true;

			return false;
		}

		/**
		 * Retourne un tableau associatif ou la clé constitue le code ISO 3166 du pays et la valeur le nom du pays en anglais.
		 *
		 * @return array
		 */
		static public function getCountriesEN (): array
		{
			return [
				'af' => 'Afghanistan',
				'ax' => 'Åland Islands',
				'al' => 'Albania',
				'dz' => 'Algeria',
				'as' => 'American Samoa',
				'ad' => 'Andorra',
				'ao' => 'Angola',
				'ai' => 'Anguilla',
				'aq' => 'Antarctica',
				'ag' => 'Antigua and Barbuda',
				'ar' => 'Argentina',
				'am' => 'Armenia',
				'aw' => 'Aruba',
				'au' => 'Australia',
				'at' => 'Austria',
				'az' => 'Azerbaijan',
				'bs' => 'Bahamas',
				'bh' => 'Bahrain',
				'bd' => 'Bangladesh',
				'bb' => 'Barbados',
				'by' => 'Belarus',
				'be' => 'Belgium',
				'bz' => 'Belize',
				'bj' => 'Benin',
				'bm' => 'Bermuda',
				'bt' => 'Bhutan',
				'bo' => 'Bolivia',
				'ba' => 'Bosnia and Herzegovina',
				'bw' => 'Botswana',
				'bv' => 'Bouvet Island',
				'br' => 'Brazil',
				'io' => 'British Indian Ocean Territory',
				'bn' => 'Brunei Darussalam',
				'bg' => 'Bulgaria',
				'bf' => 'Burkina Faso',
				'bi' => 'Burundi',
				'kh' => 'Cambodia',
				'cm' => 'Cameroon',
				'ca' => 'Canada',
				'cv' => 'Cape Verde',
				'ky' => 'Cayman Islands',
				'cf' => 'Central African Republic',
				'td' => 'Chad',
				'cl' => 'Chile',
				'cn' => 'China',
				'cx' => 'Christmas Island',
				'cc' => 'Cocos (Keeling) Islands',
				'co' => 'Colombia',
				'km' => 'Comoros',
				'cg' => 'Congo',
				'cd' => 'Congo, The Democratic Republic of The',
				'ck' => 'Cook Islands',
				'cr' => 'Costa Rica',
				'ci' => 'Cote D\'ivoire',
				'hr' => 'Croatia',
				'cu' => 'Cuba',
				'cy' => 'Cyprus',
				'cz' => 'Czech Republic',
				'dk' => 'Denmark',
				'dj' => 'Djibouti',
				'dm' => 'Dominica',
				'do' => 'Dominican Republic',
				'ec' => 'Ecuador',
				'eg' => 'Egypt',
				'sv' => 'El Salvador',
				'gq' => 'Equatorial Guinea',
				'er' => 'Eritrea',
				'ee' => 'Estonia',
				'et' => 'Ethiopia',
				'fk' => 'Falkland Islands (Malvinas)',
				'fo' => 'Faroe Islands',
				'fj' => 'Fiji',
				'fi' => 'Finland',
				'fr' => 'France',
				'gf' => 'French Guiana',
				'pf' => 'French Polynesia',
				'tf' => 'French Southern Territories',
				'ga' => 'Gabon',
				'gm' => 'Gambia',
				'ge' => 'Georgia',
				'de' => 'Germany',
				'gh' => 'Ghana',
				'gi' => 'Gibraltar',
				'gr' => 'Greece',
				'gl' => 'Greenland',
				'gd' => 'Grenada',
				'gp' => 'Guadeloupe',
				'gu' => 'Guam',
				'gt' => 'Guatemala',
				'gg' => 'Guernsey',
				'gn' => 'Guinea',
				'gw' => 'Guinea-bissau',
				'gy' => 'Guyana',
				'ht' => 'Haiti',
				'hm' => 'Heard Island and Mcdonald Islands',
				'va' => 'Holy See (Vatican City State)',
				'hn' => 'Honduras',
				'hk' => 'Hong Kong',
				'hu' => 'Hungary',
				'is' => 'Iceland',
				'in' => 'India',
				'id' => 'Indonesia',
				'ir' => 'Iran, Islamic Republic of',
				'iq' => 'Iraq',
				'ie' => 'Ireland',
				'im' => 'Isle of Man',
				'il' => 'Israel',
				'it' => 'Italy',
				'jm' => 'Jamaica',
				'jp' => 'Japan',
				'je' => 'Jersey',
				'jo' => 'Jordan',
				'kz' => 'Kazakhstan',
				'ke' => 'Kenya',
				'ki' => 'Kiribati',
				'kp' => 'Korea, Democratic People\'s Republic of',
				'kr' => 'Korea, Republic of',
				'kw' => 'Kuwait',
				'kg' => 'Kyrgyzstan',
				'la' => 'Lao People\'s Democratic Republic',
				'lv' => 'Latvia',
				'lb' => 'Lebanon',
				'ls' => 'Lesotho',
				'lr' => 'Liberia',
				'ly' => 'Libyan Arab Jamahiriya',
				'li' => 'Liechtenstein',
				'lt' => 'Lithuania',
				'lu' => 'Luxembourg',
				'mo' => 'Macao',
				'mk' => 'Macedonia, The Former Yugoslav Republic of',
				'mg' => 'Madagascar',
				'mw' => 'Malawi',
				'my' => 'Malaysia',
				'mv' => 'Maldives',
				'ml' => 'Mali',
				'mt' => 'Malta',
				'mh' => 'Marshall Islands',
				'mq' => 'Martinique',
				'mr' => 'Mauritania',
				'mu' => 'Mauritius',
				'yt' => 'Mayotte',
				'mx' => 'Mexico',
				'fm' => 'Micronesia, Federated States of',
				'md' => 'Moldova, Republic of',
				'mc' => 'Monaco',
				'mn' => 'Mongolia',
				'me' => 'Montenegro',
				'ms' => 'Montserrat',
				'ma' => 'Morocco',
				'mz' => 'Mozambique',
				'mm' => 'Myanmar',
				'na' => 'Namibia',
				'nr' => 'Nauru',
				'np' => 'Nepal',
				'nl' => 'Netherlands',
				'an' => 'Netherlands Antilles',
				'nc' => 'New Caledonia',
				'nz' => 'New Zealand',
				'ni' => 'Nicaragua',
				'ne' => 'Niger',
				'ng' => 'Nigeria',
				'nu' => 'Niue',
				'nf' => 'Norfolk Island',
				'mp' => 'Northern Mariana Islands',
				'no' => 'Norway',
				'om' => 'Oman',
				'pk' => 'Pakistan',
				'pw' => 'Palau',
				'ps' => 'Palestinian Territory, Occupied',
				'pa' => 'Panama',
				'pg' => 'Papua New Guinea',
				'py' => 'Paraguay',
				'pe' => 'Peru',
				'ph' => 'Philippines',
				'pn' => 'Pitcairn',
				'pl' => 'Poland',
				'pt' => 'Portugal',
				'pr' => 'Puerto Rico',
				'qa' => 'Qatar',
				're' => 'Reunion',
				'ro' => 'Romania',
				'ru' => 'Russian Federation',
				'rw' => 'Rwanda',
				'sh' => 'Saint Helena',
				'kn' => 'Saint Kitts and Nevis',
				'lc' => 'Saint Lucia',
				'pm' => 'Saint Pierre and Miquelon',
				'vc' => 'Saint Vincent and The Grenadines',
				'ws' => 'Samoa',
				'sm' => 'San Marino',
				'st' => 'Sao Tome and Principe',
				'sa' => 'Saudi Arabia',
				'sn' => 'Senegal',
				'rs' => 'Serbia',
				'sc' => 'Seychelles',
				'sl' => 'Sierra Leone',
				'sg' => 'Singapore',
				'sk' => 'Slovakia',
				'si' => 'Slovenia',
				'sb' => 'Solomon Islands',
				'so' => 'Somalia',
				'za' => 'South Africa',
				'gs' => 'South Georgia and The South Sandwich Islands',
				'es' => 'Spain',
				'lk' => 'Sri Lanka',
				'sd' => 'Sudan',
				'sr' => 'Suriname',
				'sj' => 'Svalbard and Jan Mayen',
				'sz' => 'Swaziland',
				'se' => 'Sweden',
				'ch' => 'Switzerland',
				'sy' => 'Syrian Arab Republic',
				'tw' => 'Taiwan, Province of China',
				'tj' => 'Tajikistan',
				'tz' => 'Tanzania, United Republic of',
				'th' => 'Thailand',
				'tl' => 'Timor-leste',
				'tg' => 'Togo',
				'tk' => 'Tokelau',
				'to' => 'Tonga',
				'tt' => 'Trinidad and Tobago',
				'tn' => 'Tunisia',
				'tr' => 'Turkey',
				'tm' => 'Turkmenistan',
				'tc' => 'Turks and Caicos Islands',
				'tv' => 'Tuvalu',
				'ug' => 'Uganda',
				'ua' => 'Ukraine',
				'ae' => 'United Arab Emirates',
				'gb' => 'United Kingdom',
				'us' => 'United States',
				'um' => 'United States Minor Outlying Islands',
				'uy' => 'Uruguay',
				'uz' => 'Uzbekistan',
				'vu' => 'Vanuatu',
				've' => 'Venezuela',
				'vn' => 'Viet Nam',
				'vg' => 'Virgin Islands, British',
				'vi' => 'Virgin Islands, U.S.',
				'wf' => 'Wallis and Futuna',
				'eh' => 'Western Sahara',
				'ye' => 'Yemen',
				'zm' => 'Zambia',
				'zw' => 'Zimbabwe'
			];
		}

		/**
		 * Retourne un tableau associatif ou la clé constitue le code ISO 3166 du pays et la valeur le nom du pays en français.
		 *
		 * @return array
		 */
		static public function getCountriesFR (): array
		{
			return [
				'ad' => 'Andorre',
				'ae' => 'Émirats arabes unis',
				'af' => 'Afghanistan',
				'ag' => 'Antigua-et-Barbuda',
				'ai' => 'Anguilla',
				'al' => 'Albanie',
				'am' => 'Arménie',
				'an' => 'Antilles néerlandaises',
				'ao' => 'Angola',
				'aq' => 'Antarctique',
				'ar' => 'Argentine',
				'as' => 'American Samoa',
				'at' => 'Autriche',
				'au' => 'Australie',
				'aw' => 'Aruba',
				'ax' => 'Îles de Åland',
				'az' => 'Azerbaïdjan',
				'ba' => 'Bosnie-et-Herzégovine',
				'bb' => 'Les Barbade',
				'bd' => 'Bangladesh',
				'be' => 'Belgique',
				'bf' => 'Burkina Faso',
				'bg' => 'Bulgarie',
				'bh' => 'Bahrain',
				'bi' => 'Burundi',
				'bj' => 'Benin',
				'bl' => 'Saint Barthélemy',
				'bm' => 'Bermuda',
				'bn' => 'Brunei Darussalam',
				'bo' => 'Bolivie',
				'br' => 'Brésil',
				'bs' => 'Bahamas',
				'bt' => 'Bhutan',
				'bv' => 'Île de Bouvet',
				'bw' => 'Botswana',
				'by' => 'Biélorussie',
				'bz' => 'Belize',
				'ca' => 'Canada',
				'cc' => 'Îles de Cocos (Keeling)',
				'cf' => 'République centrafricaine',
				'cg' => 'Congo',
				'ch' => 'Suisse',
				'ci' => 'Côte d\'Ivoire',
				'ck' => 'Îles Cook',
				'cl' => 'Chili',
				'cm' => 'Cameroune',
				'cn' => 'Chine',
				'co' => 'Colombie',
				'cr' => 'Costa Rica',
				'cu' => 'Cuba',
				'cv' => 'Cap Vert',
				'cx' => 'Île Christmas',
				'cy' => 'Chypre',
				'cz' => 'République Tchèque',
				'de' => 'Allemagne',
				'dj' => 'Djibouti',
				'dk' => 'Danemark',
				'dm' => 'Dominica',
				'do' => 'République Dominicaine',
				'dz' => 'Algerie',
				'ec' => 'Equateur',
				'ee' => 'Estonie',
				'eg' => 'Egypte',
				'eh' => 'Sahara occidental',
				'er' => 'Eritrea',
				'es' => 'Espagne',
				'et' => 'Ethiopie',
				'fi' => 'Finlande',
				'fj' => 'Fiji',
				'fk' => 'Malouines (Malvinas)',
				'fm' => 'Micronésie',
				'fo' => 'Iles Féroé',
				'fr' => 'France',
				'ga' => 'Gabon',
				'gb' => 'Royaume-Uni',
				'gd' => 'Grenade',
				'ge' => 'Géorgie GE',
				'gf' => 'Guyane française française',
				'gg' => 'Guernesey',
				'gh' => 'Ghana',
				'gi' => 'Gibraltar',
				'gl' => 'Groenland',
				'gm' => 'Gambie',
				'gn' => 'Guinée',
				'gp' => 'Guadeloupe',
				'gq' => 'Guinée équatoriale',
				'gr' => 'Grèce',
				'gs' => 'La Géorgie du sud et les îles de sandwich du sud',
				'gt' => 'Guatemala',
				'gu' => 'Guam',
				'gw' => 'Guinea-Bissau',
				'gy' => 'Guyane',
				'hk' => 'Hong Kong',
				'hm' => 'Île et îles entendues de McDonald',
				'hn' => 'Honduras',
				'hr' => 'Croatie',
				'ht' => 'Haiti',
				'hu' => 'Hongrie',
				'id' => 'Indonésie',
				'ie' => 'Irlande',
				'il' => 'Israël',
				'im' => 'Île de l\'homme',
				'in' => 'Inde',
				'io' => 'Territoire d\'Océan Indien britannique',
				'iq' => 'Irak',
				'ir' => 'Iran',
				'is' => 'Islande',
				'it' => 'Italie',
				'je' => 'Jersey',
				'jm' => 'Jamaïque',
				'jo' => 'Jordanie',
				'jp' => 'Japon',
				'ke' => 'Kenya',
				'kg' => 'Kyrgyzstan',
				'kh' => 'Cambodge',
				'ki' => 'Kiribati',
				'km' => 'Comoros',
				'kp' => 'Corée du Nord',
				'kr' => 'Corée du Sud',
				'kw' => 'Kowéit',
				'ky' => 'Iles Cayman',
				'kz' => 'Kazakhstan',
				'la' => 'Lao People\'s Democratic Republic',
				'lb' => 'Lebanon',
				'lc' => 'Saint Lucia',
				'li' => 'Liechtenstein',
				'lk' => 'Sri Lanka',
				'lr' => 'Libéria',
				'ls' => 'Lesotho',
				'lt' => 'Lithuania',
				'lu' => 'Luxembourg',
				'lv' => 'Latvia',
				'ly' => 'Libyen',
				'ma' => 'Maroc',
				'mc' => 'Monaco',
				'md' => 'Moldau',
				'me' => 'Montenegro',
				'mf' => 'Saint Martin',
				'mg' => 'Madagascar',
				'mh' => 'Marshall Islands',
				'mk' => 'Macédoine',
				'ml' => 'Mali',
				'mm' => 'Myanmar',
				'mn' => 'Mongolia',
				'mo' => 'Macao',
				'mp' => 'Mariannes du Nord',
				'mq' => 'Martinique',
				'mr' => 'Mauritania',
				'ms' => 'Montserrat',
				'mt' => 'Malta',
				'mu' => 'Îles Maurice',
				'mv' => 'Maldives',
				'mw' => 'Malawi',
				'mx' => 'Mexique',
				'my' => 'Malaisie',
				'mz' => 'Mozambique',
				'na' => 'Namibie',
				'nc' => 'Nouvelle-Calédonie',
				'ne' => 'Niger',
				'nf' => 'Île de Norfolk',
				'ng' => 'Nigéria',
				'ni' => 'Nicaragua',
				'nl' => 'Pays Bas',
				'no' => 'Norvège',
				'np' => 'Nepal',
				'nr' => 'Nauru',
				'nu' => 'Niue',
				'nz' => 'Nouvelle Zélande',
				'om' => 'Oman',
				'pa' => 'Panama',
				'pe' => 'Peru',
				'pf' => 'Polynésie française',
				'pg' => 'Papouasie-Nouvelle Guinée',
				'ph' => 'Philippines',
				'pk' => 'Pakistan',
				'pl' => 'Poland',
				'pm' => 'Saint Pierre and Miquelon',
				'pn' => 'Pitcairn',
				'pr' => 'Porto Rico',
				'pt' => 'Portugal',
				'pw' => 'Palau',
				'py' => 'Paraguay',
				'qa' => 'Qatar',
				're' => 'Réunion',
				'ro' => 'Roumanie',
				'rs' => 'Serbie',
				'ru' => 'Fédération de Russie',
				'rw' => 'Rwanda',
				'sa' => 'Saudi Arabia',
				'sb' => 'Îles Salomon',
				'sc' => 'Seychelles',
				'sd' => 'Soudan',
				'se' => 'Suède',
				'sg' => 'Singapour',
				'si' => 'Slovénie',
				'sk' => 'Slovaquie SK',
				'sl' => 'Sierra Leone',
				'sm' => 'San Marino',
				'sn' => 'Sénégal',
				'so' => 'Somalie',
				'sr' => 'Suriname',
				'st' => 'Sao-Tomé-et-Principe',
				'sv' => 'Le Salvador',
				'sy' => 'République arabe syrienne',
				'sz' => 'Souaziland',
				'tc' => 'Turks and Caicos Islands',
				'td' => 'Chad',
				'tg' => 'Togo',
				'th' => 'Thailand',
				'tj' => 'Tajikistan',
				'tk' => 'Tokelau',
				'tl' => 'Timor-Leste',
				'tm' => 'Turkmenistan',
				'tn' => 'Tunisie',
				'to' => 'Tonga',
				'tr' => 'Turquie',
				'tt' => 'Trinité-et-Tabago',
				'tv' => 'Tuvalu',
				'tw' => 'Taiwan',
				'tz' => 'Tanzanie',
				'ua' => 'Ukraine',
				'ug' => 'Uganda',
				'us' => 'Etats-unis',
				'uy' => 'Uruguay',
				'uz' => 'Uzbekistan',
				'vc' => 'Saint Vincent And The Grenadines',
				've' => 'Venezuela',
				'vg' => 'Îles Vierges, britanniques',
				'vi' => 'Îles Vierges, États-Unis.',
				'vn' => 'Viet Nam',
				'vu' => 'Vanuatu précédemment',
				'wf' => 'Wallis et Futuna',
				'ws' => 'Western Samoa',
				'ye' => 'Yemen',
				'yt' => 'Mayotte',
				'za' => 'Afrique du Sud',
				'zm' => 'Zambie ',
				'zw' => 'Zimbabwe'
			];
		}

		/**
		 * Retourne un tableau associatif ou la clé constitue le code ISO 3166 du pays et la valeur le nom du pays en français.
		 *
		 * @param array $countriesList La liste des pays dans la langue désirée. Exemple: International::getCountriesEN().
		 * @param string $iso Le code ISO 3166 du pays.
		 * @return string
		 */
		static public function getCountryName (array $countriesList, string $iso): string
		{
			if ( strlen($iso) !== 2 )
			{
				trigger_error(__METHOD__.'(), ISO code "'.$iso.'" is not a valid ISO 3166 code !', E_USER_WARNING);

				return '';
			}

			$iso = strtolower($iso);

			if ( !array_key_exists($iso, $countriesList) )
			{
				trigger_error(__METHOD__.'(), ISO code "'.$iso.'" doesn\'t exists !', E_USER_NOTICE);

				return '';
			}

			return $countriesList[$iso];
		}
	}