<?php

	/*
	* Libraries/php/LTK/ServerAnswer.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	use JetBrains\PhpStorm\NoReturn;

	/**
	 * Classe permettant de générer une réponse serveur coté serveur. Elle sera utilisable par sa classe collège ServerResponse
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */	
	class ServerAnswer extends AbstractServerMessage
	{
		/**
		 * Le constructeur.
		 *
		 * @param int $code Le numéro d'erreur. 0 signifie qu'il n'y a eu aucun problème.
		 * @param string $message Description explicite du statut.
		 */
		public function __construct (int $code, string $message = '')
		{
			$this->code = $code;
			$this->message = $message;
		}

		/**
		 * Retourne directement le flux JSON d'une réponse.
		 *
		 * @param int $code Le numéro d'erreur. 0 signifie qu'il n'y a eu aucun problème.
		 * @param string $message Description explicite du statut.
		 * @param array $data Un tableau associatif pour les données personnalisées.
		 * @return string.
		 */
		static function get (int $code, string $message = '', array $data = []): string
		{
			$answer = new ServerAnswer($code, $message);

			foreach ( $data as $key => $value )
				$answer->setData($key, $value);

			return $answer->toJSON();
		}

		/**
		 * Affiche directement le flux JSON d'une réponse.
		 *
		 * @param int $code Le numéro d'erreur. 0 signifie qu'il n'y a eu aucun problème.
		 * @param string $message Description explicite du statut.
		 * @param array $data Un tableau associatif pour les données personnalisées.
		 */
		static function fire (int $code, string $message = '', array $data = [])
		{
			$answer = new ServerAnswer($code, $message);

			foreach ( $data as $key => $value )
				$answer->setData($key, $value);

			$answer->execute();
		}

		/**
		 * Permet d'ajouter une donnée supplémentaire à la réponse.
		 *
		 * @param string $key Le nom de la variable.
		 * @param mixed $value N'importe quel type de données sérialisable en JSON.
		 */
		public function setData (string $key, $value)
		{
			$this->data[$key] = $value;
		}

		/**
		 * Génération le flux JSON.
		 *
		 * @return string.
		 */
		public function toJSON (): string
		{
			return json_encode([
				'code' => $this->code,
				'message' => $this->message,
				'data' => $this->data
			]);
		}

		/**
		 * Génère une réponse serveur JSON.
		 *
		 * @note Le script PHP s'interrompera après l'appel de cette méthode.
		 */
		public function execute ()
		{
			header('Content-Type: application/json');

			echo $this->toJSON();

			exit(0);
		}
	}
