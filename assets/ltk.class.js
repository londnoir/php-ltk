/*global unescape*/

/*
* Libraries/js/ltk.class.js
* This file is part of PHP-LTK 
*
* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
*
* PHP-LTK is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* PHP-LTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

var LTK = {};

LTK.addToPostData = function (variableName, variableValue, data)
{
	"use strict";

	var i;

	if ( variableName.length === 0 )
	{
		return false;
	}

	/* On vérifie d'abord que la variable n'existe pas déjà. */
	for ( i = 0; i < data.length; i += 1 )
	{
		if ( variableName === data[i].name )
		{
			/* On vérifie que ce n'est pas déjà un tableau. */
			if ( typeof data[i].value === 'array' )
			{
				if ( data[i].value.indexOf(variableValue) === -1 )
				{
					data[i].value.push = variableValue;
				}
			}
			else if ( data[i].value !== variableValue )
			{
				data[i].value = [data[i].value, variableValue];
			}

			return true;
		}
	}

	/* Si la variable n'existe pas, on l'ajoute simplement */
	data.push({
		name:variableName,
		value:variableValue
	});

	return true;
};

LTK.getPostData = function (formObject)
{
	"use strict";

	var data = [];
	var i, j, field, variableName, variableValue;

	for ( i = 0; i < formObject.elements.length; i += 1 )
	{
		field = formObject.elements[i];

		variableName = field.name;

		if ( field.tagName === "SELECT" )
		{
			for ( j = 0; j < field.options.length; j += 1 )
			{
				if ( field.options[j].selected )
				{
					variableValue = field.options[j].value || field.options[j].text;

					addToPostData(variableName, variableValue, data);
				}
			}
		}
		else
		{
			if ( field.getAttribute("type").toLowerCase() === "radio" && field.checked === false )
			{
				continue;
			}

			variableValue = field.value;
			
			addToPostData(variableName, variableValue, data);
		}	
	}

	return data;
};

LTK.getSelectValue = function (selectInputID)
{
	"use strict";

	var object = document.getElementById(selectInputID);

	if ( object )
	{
		return object.options[object.selectedIndex].value;
	}
	else
	{
		return false;
	}
};

/**
 * Returns a random number between min and max
 */
LTK.randomFloat = function (min, max)
{
	"use strict";

	return Math.random() * (max - min) + min;
};

/**
 * Returns a random integer between min and max
 * Using Math.round() will give you a non-uniform distribution!
 */
LTK.random = function (min, max)
{
	"use strict";

	return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
* @brief Vérifie que la chaîne de caractères est bien une signature SHA1.
* @param $string Une chaîne de caractères à vérifier.
* @return Un booléen.
*/
LTK.isSHA1 = function (string)
{
	"use strict";

	if ( string.length != 40 )
	{
		return false;
	}

	var reg = new RegExp("^([0-9a-fA-F]{40,40})$", "g");

	return string.match(reg) ? true : false;
};

/**
* @brief Vérifie que la chaîne de caractères est bien une signature MD5.
* @param $string Une chaîne de caractères à vérifier.
* @return Un booléen.
*/
LTK.isMD5 = function (string)
{
	"use strict";

	if ( string.length != 32 )
	{
		return false;
	}

	var reg = new RegExp("^([0-9a-fA-F]{32,32})$", "g");

	return string.match(reg) ? true : false;
};

/**
* @brief Vérifie que la chaîne de caractères est encodée en base64.
* @warning Cette fonction peut retourner de faux positifs. Par contre, on peut-être certain qu'une chaîne n'est pas encodée en base64.
* @param $string Une chaîne de caractères à vérifier.
* @return Un booléen.
*/
LTK.isBase64 = function (string)
{
	"use strict";

	var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");

	return base64Matcher.test(string);
};

/**
* @brief Convertit un texte pour pouvoir être affiché dans les attributs de balises html.
* @param $plainText Une chaîne de caractères à vérifier.
* @return Une chaîne de caractères.
*/
LTK.safeHTML = function (plainText)
{
	"use strict";

	var reg = new RegExp("([\n\r\t]+)", "g");

	plainText = PHPEquiv.htmlspecialchars(plainText, "ENT_QUOTES");

	return plainText.replace(reg, '');
};

LTK.text2HTML = function (plainText)
{
	"use strict";

	plainText = PHPEquiv.htmlspecialchars(plainText);

	plainText.replace("\n", "<br />");
	plainText.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
	plainText.replace("    ", "&nbsp;&nbsp;&nbsp;&nbsp;");

	return "<p>" + plainText + "</p>\n";
};

/**
* @brief Permet de construire une URL avec un tableau de paramètre.
* @param $file Une chaîne de caractères pour l'URL relative ou absolue d'une page web. Dans le cas d'une URL relative, la variable 'site_url' de la class Config sera utilisée.
* @param $params Un tableau associatif où les clés formeront les noms de variables et seront affichées derrière l'URL conformément à la méthode GET.
* @param $isHTML Un booléen qui permet d'indiquer que l'URL sera afficher au sein d'un document html. Les caractères spéciaux seront convertit par des entités html. Par défaut, l'option est activée.
* @return Une chaîne de caractères.
*/
LTK.buildURL = function (file, params, isHTML)
{
	"use strict";

	/* Set the base absolute URL */
	var url = ( PHPEquiv.strstr(file, 'http://') === false ) ? "http://" + document.domain + "/" + file : file;
	/* the '&' char is restricted in HTML context */
	var andChr = ( typeof isHTML === 'undefined' || isHTML ) ? '&amp;' : '&';

	if ( typeof params === 'object' )
	{
		var variables = '';

		Object.keys(params).forEach(function (key) {
			if ( variables )
			{
				variables += andChr;
			}

			variables += key + '=' + params[key];
		});

		/* Check for the first character before adding variables */
		url += ( PHPEquiv.strstr(file, '?') === false ? '?' : andChr ) + variables;
	}

	return url;
};

/**
* @brief Raccourcit un texte.
* @fixme Retirer les fonctions "mb_xxx" quand l'UTF-8 sera correctement adopté par PHP.
* @param $buffer Une chaîne de caractères à raccourcir.
* @param $limit Un entier indiquant le nombre de caractères que le texte ne doit pas dépasser. Par défaut, la limit est fixée à 50.
* @param $disableFX Un booléen qui permet d'empêcher de rajouter '...' à la fin du texte. Par défaut, le paramètre est à false.
* @return Une chaîne de caractères.
*/
LTK.sum = function (buffer, limit, disableFX)
{
	"use strict";

	if ( typeof limit === 'undefined' )
	{
		limit = 50;
	}

	buffer = PHPEquiv.html_entity_decode(buffer, "ENT_QUOTES");

	if ( buffer.length > limit )
	{
		buffer = buffer.substr(0, limit);

		if ( disableFX === false )
		{
			buffer += "...";
		}
	}

	return PHPEquiv.htmlspecialchars(buffer, "ENT_COMPAT");
};

/**
* @brief Convertit un texte en une chaîne de caractères compatible avec les URL.
* @param $string Une chaîne de caractères à convertir.
* @return Une chaîne de caractères.
*/
LTK.stringToURL = function (string)
{
	"use strict";

	/* Remove all HTML entities from the string and lower case. */
	var newString = PHPEquiv.html_entity_decode(string, "ENT_QUOTES").toLowerCase();
	var input = ['á', 'à', 'ä', 'â', 'é', 'è', 'ë', 'ê', 'í', 'ì', 'ï', 'î', 'ó','ò','ö','ô', 'ú', 'ù', 'ü', 'û', 'ý', 'ÿ', 'ç', 'œ', '$', '€'];
	var output = ['a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'c', 'oe', 'dollars', 'euros'];
	var inputLenght = input.length;
	var i;

	for ( i = 0; i < inputLenght; i += 1 )
	{
		newString.replace(input[i], output[i]);
	}

	/* Replace all char wich is not alphanumeric by '-' char */
	newString.replace(new RegExp("([^A-Za-z0-9-]+)", "g"), '-');

	/* Remove doubled '-' char */
	newString.replace(new RegExp("[-]{2,}", "g"), '-');

	if ( newString[0] === '-' )
	{
		delete newString[0];
	}

	if ( newString[newString.length - 1] = '-' )
	{
		delete newString[newString.length - 1];
	}

	return newString;
};

/**
* @brief Vérifie la syntaxe d'une adresse email.
* @param $string Une chaîne de caractères contenant l'adresse email à vérifier.
* @return Une chaîne de caractères contenant l'adresse email valide ou null.
*/
LTK.checkEmailSyntax = function (string)
{
	"use strict";

	var i;
	var testAt;
	var testDot;
	
	if ( !string.match(new RegExp('^[a-zA-Z0-9@._\-]+$', 'g')) )
	{
		return false;
	}
	
	testAt = string.split('@');

	if ( testAt.length !== 2 || testAt[0].length <= 0 || testAt[1].length <= 0 )
	{
		return false;
	}

	testDot = testAt[1].split('.');

	if ( testDot.length < 2 )
	{
		return false;
	}

	for ( i = 0; i < testDot.length; i += 1 )
	{
		if ( testDot[i].length <= 0 )
		{
			return false;
		}
	}
	
	return true;
};

/**
* @brief Vérifie que l'adresse email vient du serveur.
* @warning Il faut configurer la variable 'site_domain' avec la classe Config.
* @param $email Une chaîne de caractères contenant l'adresse email à vérifier.
* @return Retourne true si le nom de domaine est identique, sinon false.
*/
LTK.isAddrUseSiteDomain = function (email)
{
	"use strict";

	var domainName = '';
	var tmp = document.domain.split('.');
	var count = tmp.length;

	if ( count == 2 )
	{
		domainName = document.domain;
	}
	else
	{
		var i;

		for ( i = 1; i < count; i += 1 )
		{
			if ( domainName )
			{
				domainName += '.';
			}

			domainName += tmp[i];
		}
	}

	tmp = email.split("@");

	return ( tmp[tmp.length - 1] === domainName );
};

/**
* @brief Encode un tableau PHP en chaîne de caractères fiable pour les URL.
* @param $cleanArray Un tableau à encoder.
* @return Une chaîne de caractères.
*/
/*LTK.arrayFormEncode = function (cleanArray)
{
	$encodedString = serialize($cleanArray); // Array to string
	$encodedString = gzcompress($encodedString, 9); // Text compression
	$encodedString = base64_encode($encodedString); // Base 64 encoding
	$encodedString = strtr($encodedString, '+/=', '-_,'); // Safe URL conversion

	return $encodedString;
};*/

/**
* @brief Décode une chaîne de caractères précédemment encodée avec Path::arrayFormEncode() pour récupérer le tableau.
* @param $encodedString Une chaîne de caractères à décoder.
* @return Un tableau.
*/
/*LTK.arrayFormDecode = function (encodedString)
{
			
	$cleanArray = strtr($encodedString, '-_,', '+/='); // Get back base64 chars
	$cleanArray = base64_decode($cleanArray); // Base 64 decoding
	$cleanArray = gzuncompress($cleanArray); // Text decompression
	$cleanArray = unserialize($cleanArray); // String to array

	return $cleanArray;
};*/

/**
* @brief Retire les variables d'une URL.
* @param $url une chaîne de caractères contenant le lien à nettoyer.
* @return Une chaîne de caractères.
*/
LTK.removeVarsFromURL = function (url)
{
	"use strict";

	var position;

	// Check for HTML format URL
	url = PHPEquiv.htmlspecialchars_decode(url, "ENT_QUOTES");

	// Look about the '?' character
	position = url.indexOf('?');

	return ( position != -1 ) ? url.substr(0, position) : url;
};

/**
* @brief Retourne un tableau contenant les variables d'une URL.
* @param $url une chaîne de caractères contenant le lien.
* @return Un tableau.
*/
LTK.extractVarsFromURL = function (url)
{
	"use strict";

	var position = url.indexOf('?');

	if ( position > -1 )
	{
		var variables = {};
		var chunks = url.substr(position + 1).split('&');

		var i;
		var variable;
		var key;
		var tmp;
		var subKey;

		for ( i = 0; i < chunks.length; i += 1 )
		{
			variable = chunks[i].split('=');

			if ( variable.length >= 2 )
			{
				position = variable[0].indexOf('[');

				if ( position > -1 )
				{
					key = variable[0].substr(0, position);

					tmp = variable[0].match('\\[[^\\]]*\\]');

					if ( tmp && tmp.length > 0 && tmp[0].length > 2 )
					{
						subKey = tmp[0].substr(1, tmp[0].length - 2);

						if ( variables[key] == undefined )
						{
							variables[key] = {};
						}

						variables[key][subKey] = variable[1];
					}
					else
					{
						if ( variables[key] == undefined )
						{
							variables[key] = [];
						}

						variables[key].push(variable[1]);
					}
				}
				else
				{
					variables[variable[0]] = variable[1];
				}
			}
		}

		return variables;
	}

	return false;
};

/**
* @brief Retourne le nom du fichier en fin d'un chemin ou d'une URL.
* @warning La fonction ne retournera rien dans le cas d'un chemin ou d'une URL finissant par "\".
* @param $path Une chaîne de caractères contenant un chemin ou une URL.
* @return Une chaîne de caractères.
*/
LTK.extractFilename = function (path)
{
	"use strict";

	/* We don't want any variables if URL is passed to this function */
	var tmp = LTK.removeVarsFromURL(path);
	
	/* Explode by slashes */
	var chunks = tmp.split('/');

	if ( chunks.length > 0 )
	{
		return chunks[chunks.length - 1];
	}

	return '';
};

/**
* @brief Retourne l'extension d'un fichier ou d'un nom de domaine.
* @warning La fonction ne retournera s'il n'y a effectivement pas d'extension.
* @param $path Une chaîne de caractères contenant le nom d'un fichier, un chemin complet ou une URL.
* @return Une chaîne de caractères.
*/
LTK.extractExtension = function (path)
{
	"use strict";

	var tmp = LTK.extractFilename(path);

	if ( tmp.length )
	{
		var chunks = tmp.split('.');
			
		return ( tmp.length > 1 ) ? chunks[chunks.length - 1] : '';
	}

	return '';
};

/**
* @brief Retourne le chemin d'un fichier (ou une URL) en retirant le nom du fichier.
* @param $path Une chaîne de caractères contenant le chemin.
* @return Une chaîne de caractères.
*/
LTK.extractPath = function (path)
{
	"use strict";

	var chunks = path.split('/');

	if ( chunks.length > 0 )
	{
		delete chunks[chunks.length - 1];

		return chunks.join('/') + '/';
	}
	
	return '';
};

/**
* @brief Cette fonction prend un nombre de secondes et retourne l'équivalant en heures, minutes, jours, ... Totals ou combinés.
* @param $duration Un entier pour représenter le temps en secondes à détailler.
* @return Un objet.
*/
LTK.detailTime = function (duration)
{
	"use strict";

	/* Total */
	var times = {};
	var tmp;
	
	times.totalDays = Math.floor(duration / 86400);
	times.totalHours = Math.floor(duration / 3600);
	times.totalMinutes = Math.floor(duration / 60);
	
	/* Combined.
	 * Remains after remove days. */
	tmp = duration % 86400;
	times.remainHours = Math.floor( tmp / 3600 );
	/* Remains after remove hours */
	tmp = tmp % 3600;
	times.remainMinutes = Math.floor( tmp / 60 );
	times.remainSeconds = Math.floor( tmp % 60 );
	
	return times;
};

/**
* @brief Permet de convertir une date courte au format US vers FR et vice versa. Par exemple, "31/12/2007" devient "2007/12/31".
* @warning Cette fonction ne test pas la date en elle même. Une chaîne quelconque comme "abc/yyz/123" sera également inversée.
* @param $string Une chaîne de caractères représentant la date de départ, par exemple "31/12/2007".
* @param $srcDelimitor Une chaîne de caractères représentant la séparation employée dans la date source. '-' par défaut.
* @param $dstDelimitor Une chaîne de caractères représentant la séparation employée dans la date finale. Par défaut, $srcDelimitor sera utilisé.
* @return Une chaîne de caractères ou null si la date de départ n'est pas valide.
*/
LTK.reverseDate = function (string, srcDelimitor, dstDelimitor)
{
	"use strict";

	if ( typeof srcDelimitor === 'undefined' )
	{
		srcDelimitor = "-";
	}

	if ( srcDelimitor && string )
	{
		string = string.trim().split(srcDelimitor);
	
		if ( string.length == 3 )
		{
			if ( typeof dstDelimitor === 'undefined' )
			{
				dstDelimitor = srcDelimitor;
			}
			
			return string[2] + dstDelimitor + string[1] + dstDelimitor + string[0];
		}
	}
	
	return '';
};

/**
* @brief Met en majuscule la première lettre de chaque mot d'une chaîne de caractères.
* @param $string Une chaîne de caractères à traiter.
* @param $onlyFirstChar Un booléen qui permet de s'assurer que seul le premier caractère d'un mot est en majuscule. Par défaut, false.
* @param $avoidWords Une tableau de chaînes de caractères contenant les mots à ne pas mettre en majuscule.
* @return Une chaîne de caractères.
*/
/*LTK.stringUcfirst = function (string, onlyFirstChar, avoidWords)
{
	if ( $onlyFirstChar )
		$string = strtolower($string);

	$words = explode(' ', $string);

	$separators = array('\'', '-');
	
	foreach ( $words as $key => $word )
	{
		// Check for sub separators
		$hasSeparators = false;

		foreach ( $separators as $separator )
		{
			if ( strstr($word, $separator) !== false )
			{
				$hasSeparators = true;

				$subWords = explode($separator, $word);

				foreach ( $subWords as $subKey => $subWord )
				{
					// Check if the sub word can be processed
					if ( in_array($subWord, $avoidWords) )
						continue;

					$subWords[$subKey] = ucfirst($subWord);
				}

				// Recompose the word for the new separator
				$word = implode($separator, $subWords);	
			}
		}

		// If sub separators has been detected, the word is processed
		if ( $hasSeparators )
		{
			$words[$key] = $word;

			continue;
		}

		// Check if the word can be processed
		if ( in_array($word, $avoidWords) )
			continue;

		$words[$key] = ucfirst($word);
	}
	
	return implode(' ', $words);

	return string;
};*/

/**
* @brief Génère un mot de passe aléatoire en suivant une chaîne de caractères.
* @param $count Un entier pour la longueur du mot de passe désiré.
* @param $charslist Une chaîne de caractères dans laquelle seront utilisé les caractères pour générer le mot de passe. Par défaut à null et utilisera des chiffres et les lettres de l'alphabet en minuscule.
*/
LTK.generatePassword = function (count, charslist)
{
	"use strict";

	var buffer = '';

	if ( count > 0 )
	{
		var endkey;
		var i;

		if ( charslist === undefined || charslist.length === 0 )
		{
			charslist = '0123456789abcdefghijklmnopqrstuvwxyz';
		}

		endkey = charslist.length - 1;

		for ( i = 0; i < count; i += 1 )
		{
			randkey = LTK.random(0, endkey);

			buffer += charslist[randkey];
		}
	}

	return buffer;
};

/**
* @brief Retourne l'age en fonction de la date du jour.
* @return Un entier ou false dans le cas d'une date invalide.
*/
LTK.getAge = function (year, month, day, atTime)
{
	"use strict";

	/* The birthdate must be valid. */
	if ( PHPEquiv.checkdate(month, day, year) === false )
	{
		console.log("The input date is not valid !");
		
		return false;
	}

	/* Get the target day, month and year. */
	var date = Number.isInteger(atTime) ? new Date(atTime * 1000) : new Date();
	
	/* Check if we are not trying to compare a date in future. */
	if ( new Date(year, month, day, 0, 0, 0, 0) > date )
	{
		console.log("The input date is in future !");
		
		return false;
	}
	
	/* Compare these with the user birthdate. */
	var baseYear = date.getFullYear() - year;
	var currentMonth = date.getMonth();
	var currentDay = date.getDate();

	/* Check if we passed the month and the day in the year. */
	if ( currentMonth < month )
	{
		return baseYear - 1;
	}
	
	if ( currentMonth === month && currentDay < day )
	{
		return baseYear - 1;
	}
	
	return baseYear;
};

LTK.alarm = function ()
{
	"use strict";

	var colorValue = 255;
	var fadeIn = true;
	var bodyObject = document.body;
	var timerID = 0;

	if ( bodyObject === undefined )
	{
		console.log("Document body is not found !");

		return false;
	}

	bodyObject.style.background = "url(55)";
	
	timerID = setInterval(function () {
		if ( fadeIn )
		{
			colorValue += 5;
		
			if ( colorValue >= 255 )
			{
				colorValue = 255;
				fadeIn = false;
			}
		}
		else
		{
			colorValue -= 5;
		
			if ( colorValue <= 0 )
			{
				colorValue = 0;
				fadeIn = true;
			}
		}
	
		bodyObject.style.backgroundColor = "rgb(255, "+colorValue+", "+colorValue+")";
	}, 10);

	return true;
};

LTK.checkInputLimit = function (inputField, counterID, maxLength)
{
	"use strict";

	var counter = document.getElementById(counterID);
	var currentLength = inputField.value.length;
	
	if ( (maxLength - currentLength) < 0 )
	{
		alert('Vous ne pouvez pas écrire plus de ' + maxLength + ' caractères !');
		
		if ( counter )
		{
			counter.innerHTML = 0;
		}
		
		inputField.value = inputField.value.substring(0, maxLength);
	}
	else
	{
		if ( counter )
		{
			counter.innerHTML = maxLength - currentLength;
		}
	}
};

LTK.inputValueToLowerCase = function (inputField)
{
	"use strict";

	if ( inputField.value )
	{
		inputField.value = inputField.value.toLowerCase();
	}
};

LTK.inputValueToUpperCase = function (inputField)
{
	"use strict";

	if ( inputField.value )
	{
		inputField.value = inputField.value.toUpperCase();
	}
};

LTK.animateCount = function (objectID, from, to, speed, step, smooth)
{
	"use strict";

	var timerID = 0;

	if ( !objectID || from == to )
	{
		return false;
	}
	
	if ( speed === undefined || speed === 0 )
	{
		speed = 100;
	}
	
	if ( step === undefined || step === 0 )
	{
		step = 1;
	}
	
	/* Increment */
	if ( from < to )
	{
		if ( smooth )
		{
			timerID = setInterval(function () {
				var ratio = from / to;
				var smoothStep = step * (1.0 - ratio);
				
				from += smoothStep;
				
				var roundValue = Math.ceil(from);
				document.getElementById(objectID).innerHTML = roundValue;
				
				if ( roundValue >= to )
				{
					clearInterval(timerID);
				}
			}, speed);
		}
		/* linear */
		else 
		{
			timerID = setInterval(function () {
				from += step;
				document.getElementById(objectID).innerHTML = from;
				
				if ( from >= to )
				{
					clearInterval(timerID);
				}
			}, speed);
		}
	}
	/* Decrement */
	else
	{
		if ( smooth )
		{
			var base = from;
			
			timerID = setInterval(function () {
				var ratio = from / base;
				var smoothStep = step * ratio;
				
				from -= smoothStep;
				var roundValue = Math.floor(from);
				document.getElementById(objectID).innerHTML = roundValue;
				
				if ( roundValue <= to )
				{
					clearInterval(timer);
				}
			}, speed);
		}
		/* linear */
		else
		{
			timerID = setInterval(function () {
				from -= step;
				document.getElementById(objectID).innerHTML = from;
				
				if ( from <= to )
				{
					clearInterval(timer);
				}
			}, speed);
		}
	}

	return true;
};

LTK.drag = function (objectID, e)
{
	"use strict";

	var mouseX;
	var mouseY;
	var x;
	var y;
	var dx;
	var dy;
	var object = document.getElementById(objectID);
	
	if ( object === undefined )
	{
		return false;
	}
	
	// Initial mouse position
	if ( IE6 || IE7 )
	{
		mouseX = event.clientX;
		mouseY = event.clientY;
	}
	else
	{
		mouseX = e.clientX;
		mouseY = e.clientY;
	}
	
	// Initial element position
	if ( object.style.left && object.style.top )
	{
		x = parseInt(object.style.left);
		y = parseInt(object.style.top);
	}
	else
	{
		x = 0;
		y = 0;
	}
	
	// Delta element-mouse
	dx = mouseX - x;
	dy = mouseY - y;
	
	// Dropping
	document.onmouseup = function ()
	{
		// clear method
		document.onmouseup = null;
		document.onmousemove = null;
	};
		
	// Dragging
	if ( IE6 || IE7 )
	{
		document.onmousemove = function ()
		{
			object.style.left = (event.clientX - dx) + 'px';
			object.style.top = (event.clientY - dy) + 'px';
		};
	}
	else
	{
		document.onmousemove = function (e)
		{
			object.style.left = (e.clientX - dx) + 'px';
			object.style.top = (e.clientY - dy) + 'px';
		};
	}
};

/**
* @brief Retourne un objet de type HTTP Request
* @detail Sample of use :
*var xhr = LTK.createXHRObject();
*	
*if ( xhr )
*{
*	xhr.onreadystatechange = function ()
*	{
*		if ( xhr.readyState == 4 && xhr.status == 200 )
*		{
*			document.getElementById('...').innerHTML = xhr.responseText;
*		}
*	}
*	
*	xhr.open('POST', 'http://...', true);
*	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
*	xhr.send('variableA=value&variableB=value');
*}
* @return Object or null.
*/
LTK.createXHRObject = function ()
{
	"use strict";

	if ( window.XMLHttpRequest )
	{
		return new XMLHttpRequest();
	}
	
	if ( window.ActiveXObject )
	{
		var names = [
			"Msxml2.XMLHTTP.6.0",
			"Msxml2.XMLHTTP.3.0",
			"Msxml2.XMLHTTP",
			"Microsoft.XMLHTTP"
		];

		Object.keys(names).forEach(function (key) {
			try
			{
				return new ActiveXObject(names[key]);
			}
			catch ( error )
			{
				console.log(error);
			}
		});
	}

	return null;
};

LTK.getValue = function (name, type, defaultValue)
{
	"use strict";

	var rawData = document.location.search;

	if ( rawData )
	{
		var chunks = rawData.replace('?', '').split('&');
		var i;
		var variable;

		for ( i = 0; i < chunks.length; i += 1 )
		{
			variable = chunks[i].split('=');

			if ( variable.length === 2 )
			{
				if ( variable[0] == name )
				{
					switch ( type )
					{
						case 'uint' :
							tmp = parseInt(variable[1], 10);

							return tmp > 0 ? tmp : 0;

						case 'int' :
							return parseInt(variable[1], 10);

						case 'bool' :
							return ( tmp ? true : false );

						case 'float' :
						case 'double' :
							return parseFloat(variable[1]);

						default :
							return variable[1];
					}
				}
			}
		}
	}

	if ( defaultValue )
	{
		return defaultValue;
	}
	else
	{
		switch ( type )
		{
			case 'uint' :
			case 'int' :
				return 0;

			case 'float' :
			case 'double' :
				return 0.0;

			case 'bool' :
				return false;

			default :
				return '';
		}
	}
};

/* ========================php.js================================ */

var PHPEquiv = {};

PHPEquiv.checkdate = function (m, d, y)
{
	"use strict";

	var date = new Date(y, m, 0);

	return ( m > 0 ) && ( m < 13 ) && ( y > 0 ) && ( y < 32768 ) && ( d > 0 ) && ( d <= date.getDate() );
};

PHPEquiv.htmlspecialchars = function (string, quote_style, charset, double_encode)
{
	"use strict";

	//       discuss at: http://phpjs.org/functions/htmlspecialchars/
	//      original by: Mirek Slugen
	//      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//      bugfixed by: Nathan
	//      bugfixed by: Arno
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//         input by: Ratheous
	//         input by: Mailfaker (http://www.weedem.fr/)
	//         input by: felix
	// reimplemented by: Brett Zamir (http://brett-zamir.me)
	//             note: charset argument not supported
	//        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
	//        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
	//        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
	//        returns 2: 'ab"c&#039;d'
	//        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
	//        returns 3: 'my &quot;&entity;&quot; is still here'

	var optTemp = 0;
	var noquotes = false;
	var i = 0;

	var OPTS = {
		ENT_NOQUOTES: 0,
		ENT_HTML_QUOTE_SINGLE: 1,
		ENT_HTML_QUOTE_DOUBLE: 2,
		ENT_COMPAT: 2,
		ENT_QUOTES: 3,
		ENT_IGNORE: 4
	};
	
	if ( typeof quote_style === 'undefined' || quote_style === null )
	{
		quote_style = 2;
	}

	string = string.toString();

	// Put this first to avoid double-encoding
	if ( double_encode !== false )
	{
		string = string.replace(/&/g, '&amp;');
	}

	string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

	if ( quote_style === 0 )
	{
		noquotes = true;
	}

	// Allow for a single string or an array of string flags
	if ( typeof quote_style !== 'number' )
	{ 
		quote_style = [].concat(quote_style);

		for ( i = 0; i < quote_style.length; i += 1 )
		{
			// Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
			if ( OPTS[quote_style[i]] === 0 )
			{
				noquotes = true;
			}
			else if ( OPTS[quote_style[i]] )
			{
				optTemp = optTemp | OPTS[quote_style[i]];
			}
		}

		quote_style = optTemp;
	}

	if ( quote_style & OPTS.ENT_HTML_QUOTE_SINGLE )
	{
		string = string.replace(/'/g, '&#039;');
	}

	if ( !noquotes )
	{
		string = string.replace(/"/g, '&quot;');
	}

	return string;
};

PHPEquiv.strstr = function (haystack, needle, bool)
{
	"use strict";

	//  discuss at: http://phpjs.org/functions/strstr/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// bugfixed by: Onno Marsman
	// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//   example 1: strstr('Kevin van Zonneveld', 'van');
	//   returns 1: 'van Zonneveld'
	//   example 2: strstr('Kevin van Zonneveld', 'van', true);
	//   returns 2: 'Kevin '
	//   example 3: strstr('name@example.com', '@');
	//   returns 3: '@example.com'
	//   example 4: strstr('name@example.com', '@', true);
	//   returns 4: 'name'

	var pos = 0;

	haystack += '';

	pos = haystack.indexOf(needle);

	if ( pos == -1 )
	{
		return false;
	}
	else if ( bool )
	{
		return haystack.substr(0, pos);
	}
	else
	{
		return haystack.slice(pos);
	}
};

PHPEquiv.get_html_translation_table = function (table, quote_style)
{
	"use strict";

	//  discuss at: http://phpjs.org/functions/get_html_translation_table/
	// original by: Philip Peterson
	//  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// bugfixed by: noname
	// bugfixed by: Alex
	// bugfixed by: Marco
	// bugfixed by: madipta
	// bugfixed by: Brett Zamir (http://brett-zamir.me)
	// bugfixed by: T.Wild
	// improved by: KELAN
	// improved by: Brett Zamir (http://brett-zamir.me)
	//    input by: Frank Forte
	//    input by: Ratheous
	//        note: It has been decided that we're not going to add global
	//        note: dependencies to php.js, meaning the constants are not
	//        note: real constants, but strings instead. Integers are also supported if someone
	//        note: chooses to create the constants themselves.
	//   example 1: get_html_translation_table('HTML_SPECIALCHARS');
	//   returns 1: {'"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;'}

	var entities = {};
	var hash_map = {};
	var decimal;
	var constMappingTable = {};
	var constMappingQuoteStyle = {};
	var useTable = {};
	var useQuoteStyle = {};

	// Translate arguments
	constMappingTable[0] = 'HTML_SPECIALCHARS';
	constMappingTable[1] = 'HTML_ENTITIES';
	constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
	constMappingQuoteStyle[2] = 'ENT_COMPAT';
	constMappingQuoteStyle[3] = 'ENT_QUOTES';

	useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
	useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() :
	'ENT_COMPAT';

	if ( useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES' )
	{
		throw new Error('Table: ' + useTable + ' not supported');
		// return false;
	}

	entities['38'] = '&amp;';

	if ( useTable === 'HTML_ENTITIES' )
	{
		entities['160'] = '&nbsp;';
		entities['161'] = '&iexcl;';
		entities['162'] = '&cent;';
		entities['163'] = '&pound;';
		entities['164'] = '&curren;';
		entities['165'] = '&yen;';
		entities['166'] = '&brvbar;';
		entities['167'] = '&sect;';
		entities['168'] = '&uml;';
		entities['169'] = '&copy;';
		entities['170'] = '&ordf;';
		entities['171'] = '&laquo;';
		entities['172'] = '&not;';
		entities['173'] = '&shy;';
		entities['174'] = '&reg;';
		entities['175'] = '&macr;';
		entities['176'] = '&deg;';
		entities['177'] = '&plusmn;';
		entities['178'] = '&sup2;';
		entities['179'] = '&sup3;';
		entities['180'] = '&acute;';
		entities['181'] = '&micro;';
		entities['182'] = '&para;';
		entities['183'] = '&middot;';
		entities['184'] = '&cedil;';
		entities['185'] = '&sup1;';
		entities['186'] = '&ordm;';
		entities['187'] = '&raquo;';
		entities['188'] = '&frac14;';
		entities['189'] = '&frac12;';
		entities['190'] = '&frac34;';
		entities['191'] = '&iquest;';
		entities['192'] = '&Agrave;';
		entities['193'] = '&Aacute;';
		entities['194'] = '&Acirc;';
		entities['195'] = '&Atilde;';
		entities['196'] = '&Auml;';
		entities['197'] = '&Aring;';
		entities['198'] = '&AElig;';
		entities['199'] = '&Ccedil;';
		entities['200'] = '&Egrave;';
		entities['201'] = '&Eacute;';
		entities['202'] = '&Ecirc;';
		entities['203'] = '&Euml;';
		entities['204'] = '&Igrave;';
		entities['205'] = '&Iacute;';
		entities['206'] = '&Icirc;';
		entities['207'] = '&Iuml;';
		entities['208'] = '&ETH;';
		entities['209'] = '&Ntilde;';
		entities['210'] = '&Ograve;';
		entities['211'] = '&Oacute;';
		entities['212'] = '&Ocirc;';
		entities['213'] = '&Otilde;';
		entities['214'] = '&Ouml;';
		entities['215'] = '&times;';
		entities['216'] = '&Oslash;';
		entities['217'] = '&Ugrave;';
		entities['218'] = '&Uacute;';
		entities['219'] = '&Ucirc;';
		entities['220'] = '&Uuml;';
		entities['221'] = '&Yacute;';
		entities['222'] = '&THORN;';
		entities['223'] = '&szlig;';
		entities['224'] = '&agrave;';
		entities['225'] = '&aacute;';
		entities['226'] = '&acirc;';
		entities['227'] = '&atilde;';
		entities['228'] = '&auml;';
		entities['229'] = '&aring;';
		entities['230'] = '&aelig;';
		entities['231'] = '&ccedil;';
		entities['232'] = '&egrave;';
		entities['233'] = '&eacute;';
		entities['234'] = '&ecirc;';
		entities['235'] = '&euml;';
		entities['236'] = '&igrave;';
		entities['237'] = '&iacute;';
		entities['238'] = '&icirc;';
		entities['239'] = '&iuml;';
		entities['240'] = '&eth;';
		entities['241'] = '&ntilde;';
		entities['242'] = '&ograve;';
		entities['243'] = '&oacute;';
		entities['244'] = '&ocirc;';
		entities['245'] = '&otilde;';
		entities['246'] = '&ouml;';
		entities['247'] = '&divide;';
		entities['248'] = '&oslash;';
		entities['249'] = '&ugrave;';
		entities['250'] = '&uacute;';
		entities['251'] = '&ucirc;';
		entities['252'] = '&uuml;';
		entities['253'] = '&yacute;';
		entities['254'] = '&thorn;';
		entities['255'] = '&yuml;';
	}

	if ( useQuoteStyle !== 'ENT_NOQUOTES' )
	{
		entities['34'] = '&quot;';
	}

	if ( useQuoteStyle === 'ENT_QUOTES' )
	{
		entities['39'] = '&#39;';
	}

	entities['60'] = '&lt;';
	entities['62'] = '&gt;';

	// ascii decimals to real symbols
	for ( decimal in entities )
	{
		if ( entities.hasOwnProperty(decimal) )
		{
			hash_map[String.fromCharCode(decimal)] = entities[decimal];
		}
	}

	return hash_map;
};

PHPEquiv.htmlentities = function (string, quote_style, charset, double_encode)
{
	"use strict";

	//  discuss at: http://phpjs.org/functions/htmlentities/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: nobbler
	// improved by: Jack
	// improved by: Rafał Kukawski (http://blog.kukawski.pl)
	// improved by: Dj (http://phpjs.org/functions/htmlentities:425#comment_134018)
	// bugfixed by: Onno Marsman
	// bugfixed by: Brett Zamir (http://brett-zamir.me)
	//    input by: Ratheous
	//  depends on: get_html_translation_table
	//   example 1: htmlentities('Kevin & van Zonneveld');
	//   returns 1: 'Kevin &amp; van Zonneveld'
	//   example 2: htmlentities("foo'bar","ENT_QUOTES");
	//   returns 2: 'foo&#039;bar'

	var hash_map = PHPEquiv.get_html_translation_table('HTML_ENTITIES', quote_style);
	var symbol = '';

	string = string == null ? '' : string + '';

	if ( !hash_map )
	{
		return false;
	}

	if ( quote_style && quote_style === 'ENT_QUOTES' )
	{
		hash_map["'"] = '&#039;';
	}

	if ( !! double_encode || double_encode == null )
	{
		for ( symbol in hash_map )
		{
			if ( hash_map.hasOwnProperty(symbol) )
			{
				string = string.split(symbol).join(hash_map[symbol]);
			}
		}
	}
	else
	{
		string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function (ignore, text, entity) {
			for ( symbol in hash_map )
			{
				if ( hash_map.hasOwnProperty(symbol) )
				{
					text = text.split(symbol).join(hash_map[symbol]);
				}
			}

			return text + entity;
		});
	}

	return string;
};

PHPEquiv.html_entity_decode = function (string, quote_style)
{
	"use strict";

	//  discuss at: http://phpjs.org/functions/html_entity_decode/
	// original by: john (http://www.jd-tech.net)
	//    input by: ger
	//    input by: Ratheous
	//    input by: Nick Kolosov (http://sammy.ru)
	// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: marc andreu
	//  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// bugfixed by: Onno Marsman
	// bugfixed by: Brett Zamir (http://brett-zamir.me)
	// bugfixed by: Fox
	//  depends on: get_html_translation_table
	//   example 1: html_entity_decode('Kevin &amp; van Zonneveld');
	//   returns 1: 'Kevin & van Zonneveld'
	//   example 2: html_entity_decode('&amp;lt;');
	//   returns 2: '&lt;'

	var hash_map = PHPEquiv.get_html_translation_table('HTML_ENTITIES', quote_style);
	var symbol = '';
	var entity = '';
	var tmp_str = string.toString();

	if ( hash_map === false )
	{
		return false;
	}

	// fix &amp; problem
	// http://phpjs.org/functions/get_html_translation_table:416#comment_97660
	delete(hash_map['&']);
	hash_map['&'] = '&amp;';

	for ( symbol in hash_map )
	{
		entity = hash_map[symbol];
		tmp_str = tmp_str.split(entity).join(symbol);
	}

	tmp_str = tmp_str.split('&#039;').join("'");

	return tmp_str;
};

PHPEquiv.htmlspecialchars = function (string, quote_style, charset, double_encode)
{
	"use strict";

	//       discuss at: http://phpjs.org/functions/htmlspecialchars/
	//      original by: Mirek Slugen
	//      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//      bugfixed by: Nathan
	//      bugfixed by: Arno
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//         input by: Ratheous
	//         input by: Mailfaker (http://www.weedem.fr/)
	//         input by: felix
	// reimplemented by: Brett Zamir (http://brett-zamir.me)
	//             note: charset argument not supported
	//        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
	//        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
	//        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
	//        returns 2: 'ab"c&#039;d'
	//        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
	//        returns 3: 'my &quot;&entity;&quot; is still here'

	var optTemp = 0,
		i = 0,
		noquotes = false;

	if ( typeof quote_style === 'undefined' || quote_style === null )
	{
		quote_style = 2;
	}
	
	string = string.toString();

	// Put this first to avoid double-encoding
	if ( double_encode !== false )
	{
		string = string.replace(/&/g, '&amp;');
	}

	string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

	var OPTS = {
		ENT_NOQUOTES: 0,
		ENT_HTML_QUOTE_SINGLE: 1,
		ENT_HTML_QUOTE_DOUBLE: 2,
		ENT_COMPAT: 2,
		ENT_QUOTES: 3,
		ENT_IGNORE: 4
	};

	if ( quote_style === 0 )
	{
		noquotes = true;
	}

	// Allow for a single string or an array of string flags
	if ( typeof quote_style !== 'number' )
	{ 
		quote_style = [].concat(quote_style);
		for ( i = 0; i < quote_style.length; i += 1 )
		{
			// Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
			if ( OPTS[quote_style[i]] === 0 )
			{
				noquotes = true;
			}
			else if ( OPTS[quote_style[i]] )
			{
				optTemp = optTemp | OPTS[quote_style[i]];
			}
		}

		quote_style = optTemp;
	}

	if ( quote_style & OPTS.ENT_HTML_QUOTE_SINGLE )
	{
		string = string.replace(/'/g, '&#039;');
	}
	
	if ( !noquotes )
	{
		string = string.replace(/"/g, '&quot;');
	}

	return string;
};

PHPEquiv.htmlspecialchars_decode = function (string, quote_style)
{
	"use strict";

	//       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
	//      original by: Mirek Slugen
	//      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//      bugfixed by: Mateusz "loonquawl" Zalega
	//      bugfixed by: Onno Marsman
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//         input by: ReverseSyntax
	//         input by: Slawomir Kaniecki
	//         input by: Scott Cariss
	//         input by: Francois
	//         input by: Ratheous
	//         input by: Mailfaker (http://www.weedem.fr/)
	//       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// reimplemented by: Brett Zamir (http://brett-zamir.me)
	//        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
	//        returns 1: '<p>this -> &quot;</p>'
	//        example 2: htmlspecialchars_decode("&amp;quot;");
	//        returns 2: '&quot;'

	var optTemp = 0,
		i = 0,
		noquotes = false;

	if ( typeof quote_style === 'undefined')
	{
		quote_style = 2;
	}

	string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');

	var OPTS = {
		ENT_NOQUOTES: 0,
		ENT_HTML_QUOTE_SINGLE: 1,
		ENT_HTML_QUOTE_DOUBLE: 2,
		ENT_COMPAT: 2,
		ENT_QUOTES: 3,
		ENT_IGNORE: 4
	};

	if ( quote_style === 0 )
	{
		noquotes = true;
	}

	// Allow for a single string or an array of string flags
	if ( typeof quote_style !== 'number' )
	{ 
		quote_style = [].concat(quote_style);

		for ( i = 0; i < quote_style.length; i += 1 )
		{
			// Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
			if ( OPTS[quote_style[i]] === 0 )
			{
				noquotes = true;
			}
			else if ( OPTS[quote_style[i]] )
			{
				optTemp = optTemp | OPTS[quote_style[i]];
			}
		}

		quote_style = optTemp;
	}

	if ( quote_style & OPTS.ENT_HTML_QUOTE_SINGLE )
	{
		string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
		// string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
	}
	
	if ( !noquotes )
	{
		string = string.replace(/&quot;/g, '"');
	}

	// Put this in last place to avoid escape being double-decoded
	return string.replace(/&amp;/g, '&');
};

PHPEquiv.utf8_encode = function (string)
{
	"use strict";

	var utftext = '';
	var i;
	var c;
	var len;

	string = string.replace(/\r\n/g,"\n");
	len = string.length;
	
	for ( i = 0; i < len; i += 1 )
	{
		c = string.charCodeAt(i);

		if ( c < 128 )
		{
			utftext += String.fromCharCode(c);
		}
		else if((c > 127) && (c < 2048))
		{
			utftext += String.fromCharCode((c >> 6) | 192);
			utftext += String.fromCharCode((c & 63) | 128);
		}
		else
		{
			utftext += String.fromCharCode((c >> 12) | 224);
			utftext += String.fromCharCode(((c >> 6) & 63) | 128);
			utftext += String.fromCharCode((c & 63) | 128);
		}
	}

	return utftext;
};

PHPEquiv.md5 = function (string)
{
	function RotateLeft (lValue, iShiftBits)
	{
		return ( lValue << iShiftBits ) | ( lValue >>> ( 32 - iShiftBits ) );
	}
 
	function AddUnsigned (lX, lY)
	{
		var lX8 = ( lX & 0x80000000 );
		var lY8 = ( lY & 0x80000000 );
		var lX4 = ( lX & 0x40000000 );
		var lY4 = ( lY & 0x40000000 );
		var lResult = ( lX & 0x3FFFFFFF ) + ( lY & 0x3FFFFFFF );

		if ( lX4 & lY4 )
		{
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}

		if ( lX4 | lY4 )
		{
			if ( lResult & 0x40000000 )
			{
				return ( lResult ^ 0xC0000000 ^ lX8 ^ lY8 );
			}
			else
			{
				return ( lResult ^ 0x40000000 ^ lX8 ^ lY8 );
			}
		}
		else
		{
			return ( lResult ^ lX8 ^ lY8 );
		}
	}
 
	function F (x, y, z)
	{
		return (x & y) | ((~x) & z);
	}

	function G (x, y, z)
	{
		return (x & z) | (y & (~z));
	}
	
	function H (x, y, z)
	{
		return (x ^ y ^ z);
	}
	
	function I (x, y, z)
	{
		return (y ^ (x | (~z)));
	}
 
	function FF (a, b, c, d, x, s, ac)
	{
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));

		return AddUnsigned(RotateLeft(a, s), b);
	}
 
	function GG (a, b, c, d, x, s, ac)
	{
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));

		return AddUnsigned(RotateLeft(a, s), b);
	}
 
	function HH (a, b, c, d, x, s, ac)
	{
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));

		return AddUnsigned(RotateLeft(a, s), b);
	}
 
	function II (a, b, c, d, x, s, ac)
	{
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));

		return AddUnsigned(RotateLeft(a, s), b);
	}
 
	function ConvertToWordArray (string)
	{
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1 = lMessageLength + 8;
		var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
		var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
		var lWordArray = Array(lNumberOfWords - 1);
		var lBytePosition = 0;
		var lByteCount = 0;

		while ( lByteCount < lMessageLength )
		{
			lWordCount = (lByteCount - (lByteCount % 4)) / 4;
			lBytePosition = (lByteCount % 4) * 8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
			lByteCount++;
		}

		lWordCount = (lByteCount - (lByteCount % 4)) / 4;
		lBytePosition = (lByteCount % 4) * 8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
		lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
		lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
		
		return lWordArray;
	}
 
	function WordToHex (lValue)
	{
		var WordToHexValue = "";
		var WordToHexValue_temp = "";
		var lByte;
		var lCount;

		for ( lCount = 0; lCount <= 3; lCount += 1 )
		{
			lByte = (lValue >>> ( lCount * 8 ) ) & 255;
			WordToHexValue_temp = '0' + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
		}

		return WordToHexValue;
	}
  
	var x = [];
	var xLen = 0;
	var k;
	var AA;
	var BB;
	var CC;
	var DD;
	var a;
	var b;
	var c;
	var d;
	var S11 = 7;
	var S12 = 12;
	var S13 = 17;
	var S14 = 22;
	var S21 = 5;
	var S22 = 9;
	var S23 = 14;
	var S24 = 20;
	var S31 = 4;
	var S32 = 11;
	var S33 = 16;
	var S34 = 23;
	var S41 = 6;
	var S42 = 10;
	var S43 = 15;
	var S44 = 21;
 
	string = PHPEquiv.utf8_encode(string);
 
	x = ConvertToWordArray(string);
	xLen = x.length;
 
	a = 0x67452301; 
	b = 0xEFCDAB89; 
	c = 0x98BADCFE; 
	d = 0x10325476;
 
	for ( k = 0; k < xLen; k += 16 )
	{
		AA = a;
		BB = b;
		CC = c;
		DD = d;
		
		a = FF(a, b, c, d, x[k+0], S11, 0xD76AA478);
		d = FF(d, a, b, c, x[k+1], S12, 0xE8C7B756);
		c = FF(c, d, a, b, x[k+2], S13, 0x242070DB);
		b = FF(b, c, d, a, x[k+3], S14, 0xC1BDCEEE);
		a = FF(a, b, c, d, x[k+4], S11, 0xF57C0FAF);
		d = FF(d,a,b,c,x[k+5], S12, 0x4787C62A);
		c = FF(c,d,a,b,x[k+6], S13, 0xA8304613);
		b = FF(b,c,d,a,x[k+7], S14, 0xFD469501);
		a=FF(a,b,c,d,x[k+8], S11, 0x698098D8);
		d=FF(d,a,b,c,x[k+9], S12, 0x8B44F7AF);
		c=FF(c,d,a,b,x[k+10], S13, 0xFFFF5BB1);
		b=FF(b,c,d,a,x[k+11], S14, 0x895CD7BE);
		a=FF(a,b,c,d,x[k+12], S11, 0x6B901122);
		d=FF(d,a,b,c,x[k+13], S12, 0xFD987193);
		c=FF(c,d,a,b,x[k+14], S13, 0xA679438E);
		b=FF(b,c,d,a,x[k+15], S14, 0x49B40821);
		a=GG(a,b,c,d,x[k+1], S21, 0xF61E2562);
		d=GG(d,a,b,c,x[k+6], S22, 0xC040B340);
		c=GG(c,d,a,b,x[k+11], S23, 0x265E5A51);
		b=GG(b,c,d,a,x[k+0], S24, 0xE9B6C7AA);
		a=GG(a,b,c,d,x[k+5], S21, 0xD62F105D);
		d=GG(d,a,b,c,x[k+10], S22, 0x2441453);
		c=GG(c,d,a,b,x[k+15], S23, 0xD8A1E681);
		b=GG(b,c,d,a,x[k+4], S24, 0xE7D3FBC8);
		a=GG(a,b,c,d,x[k+9], S21, 0x21E1CDE6);
		d=GG(d,a,b,c,x[k+14],S22, 0xC33707D6);
		c=GG(c,d,a,b,x[k+3], S23, 0xF4D50D87);
		b=GG(b,c,d,a,x[k+8], S24, 0x455A14ED);
		a=GG(a,b,c,d,x[k+13], S21, 0xA9E3E905);
		d=GG(d,a,b,c,x[k+2], S22, 0xFCEFA3F8);
		c=GG(c,d,a,b,x[k+7], S23, 0x676F02D9);
		b=GG(b,c,d,a,x[k+12], S24, 0x8D2A4C8A);
		a=HH(a,b,c,d,x[k+5], S31, 0xFFFA3942);
		d=HH(d,a,b,c,x[k+8], S32, 0x8771F681);
		c=HH(c,d,a,b,x[k+11], S33, 0x6D9D6122);
		b=HH(b,c,d,a,x[k+14], S34, 0xFDE5380C);
		a=HH(a,b,c,d,x[k+1], S31, 0xA4BEEA44);
		d=HH(d,a,b,c,x[k+4], S32, 0x4BDECFA9);
		c=HH(c,d,a,b,x[k+7], S33, 0xF6BB4B60);
		b=HH(b,c,d,a,x[k+10], S34, 0xBEBFBC70);
		a=HH(a,b,c,d,x[k+13], S31, 0x289B7EC6);
		d=HH(d,a,b,c,x[k+0], S32, 0xEAA127FA);
		c=HH(c,d,a,b,x[k+3], S33, 0xD4EF3085);
		b=HH(b,c,d,a,x[k+6], S34, 0x4881D05);
		a=HH(a,b,c,d,x[k+9], S31, 0xD9D4D039);
		d=HH(d,a,b,c,x[k+12], S32, 0xE6DB99E5);
		c=HH(c,d,a,b,x[k+15], S33, 0x1FA27CF8);
		b=HH(b,c,d,a,x[k+2], S34, 0xC4AC5665);
		a=II(a,b,c,d,x[k+0], S41, 0xF4292244);
		d=II(d,a,b,c,x[k+7], S42, 0x432AFF97);
		c=II(c,d,a,b,x[k+14],S43, 0xAB9423A7);
		b=II(b,c,d,a,x[k+5], S44, 0xFC93A039);
		a=II(a,b,c,d,x[k+12],S41, 0x655B59C3);
		d=II(d,a,b,c,x[k+3], S42, 0x8F0CCC92);
		c=II(c,d,a,b,x[k+10],S43, 0xFFEFF47D);
		b=II(b,c,d,a,x[k+1], S44, 0x85845DD1);
		a=II(a,b,c,d,x[k+8], S41, 0x6FA87E4F);
		d=II(d,a,b,c,x[k+15],S42, 0xFE2CE6E0);
		c=II(c,d,a,b,x[k+6], S43, 0xA3014314);
		b=II(b,c,d,a,x[k+13],S44, 0x4E0811A1);
		a=II(a,b,c,d,x[k+4], S41, 0xF7537E82);
		d=II(d,a,b,c,x[k+11],S42, 0xBD3AF235);
		c=II(c,d,a,b,x[k+2], S43, 0x2AD7D2BB);
		b=II(b,c,d,a,x[k+9], S44, 0xEB86D391);
		a = AddUnsigned(a, AA);
		b = AddUnsigned(b, BB);
		c = AddUnsigned(c, CC);
		d = AddUnsigned(d, DD);
	}
 
	return ( WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d) ).toLowerCase();
};
