<?php

	/*
	* autoload.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	$GLOBALS['ltk_top'] = microtime(true);
	$GLOBALS['ltk_debug'] = true;
	$GLOBALS['ltk_root_path'] = __DIR__;
	if ( substr($GLOBALS['ltk_root_path'], -1) !== DIRECTORY_SEPARATOR )
		$GLOBALS['ltk_root_path'] .= DIRECTORY_SEPARATOR;

	spl_autoload_register(function (string $name) {
		$filename = str_replace('LTK\\', '', $name).'.php';

		$filepath = $GLOBALS['ltk_root_path'] . 'src' . DIRECTORY_SEPARATOR . $filename;

		if ( file_exists($filepath) )
			include($filepath);
	});
	