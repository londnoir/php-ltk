<?php

	/*
	* Libraries/php/LTK/Time.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe contient les outils pour manipuler le temps.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class Time
	{
		/** @internal */
		private function __construct () {}
		
		/**
		 * Retourne un timestamp unix dans le futur en fonction du nombre de jour désiré.
		 *
		 * @param int $dayCount Le nombre de jour après que l'on veuille le timestamp.
		 * @param bool $atMorning Permet de clamper le timestamp sur la premier de la journée à savoir 00:00.
		 * @return int
		 */
		static public function futurTimestamp (int $dayCount, bool $atMorning = false): int
		{
			if ( $atMorning )
				return mktime(0, 0, 0, date('n'), intval(date('j')) + $dayCount);

			return time() + ($dayCount * 86400);
		}

		/**
		 * Retourne l'age en fonction de la date du jour.
		 *
		 * @param int $year Un entier pour l'année de naissance.
		 * @param int $month Un entier pour le mois de naissance.
		 * @param int $day Un entier pour le jour de naissance.
		 * @param int $atTime Un entier (Timestamp) pour la date par rapport à laquelle sera calculé l'age. Par défaut, maintenant.
		 * @return int
		 */
		static public function age (int $year, int $month, int $day, int $atTime = 0): int
		{
			/* The birthdate must be valid. */
			if ( !checkdate($month, $day, $year) )
			{
				trigger_error('The input date is not valid !', E_USER_WARNING);
				
				return -1;
			}
			
			/* Get the target day, month and year. */
			$time = $atTime ?: time();
			
			/* Check if we are not trying to compare a date in future. */
			if ( mktime(0, 0, 0, $month, $day, $year) > $time )
			{
				trigger_error('The input date is in future !', E_USER_WARNING);
				
				return -1;
			}
						
			/* Compare these with the user birthdate. */
			$baseYear = intval(date('Y', $time)) - $year;
			$currentMonth = intval(date('m', $time));
			$currentDay = intval(date('d', $time));

			/* Check if we passed the month and the day in the year. */
			if ( $currentMonth < $month )
				return $baseYear - 1;
			
			if ( $currentMonth == $month && $currentDay < $day )
				return $baseYear - 1;
			
			return $baseYear;
		}

		/**
		 * Cette fonction prend un nombre de secondes et retourne l'équivalant en heures, minutes, jours, ... Totals ou combinés.
		 *
		 * @param int $duration Le temps en secondes à détailler.
		 * @return array Un tableau d'entier du type :
		 * array(
		 *	'total_days' => 0,
		 *	'total_hours' => 0,
		 *	'total_minutes' => 0,
		 *	'remain_hours' => 0,
	 	 *	'remain_minutes' => 0,
	 	 *	'remain_seconds' => 0
		 * )
		 */
		static public function detailTime (int $duration): array
		{
			$times = [
				'total_days' => intval($duration / 86400),
				'total_hours' => intval($duration / 3600),
				'total_minutes' => intval($duration / 60)
			];
			
			$tmp = $duration % 86400; // Remains after remove days
			$times['remain_hours'] = intval($tmp / 3600);
			$tmp = $tmp % 3600; // Remains after remove hours
			$times['remain_minutes'] = intval($tmp / 60);
			$times['remain_seconds'] = intval($tmp % 60);
			
			return $times;
		}
		
		/**
		 * Permet de convertir une date courte au format US vers FR et vice versa. Par exemple, "31/12/2007" devient "2007/12/31".
		 * Cette fonction ne test pas la date en elle même. Une chaîne quelconque comme "abc/yyz/123" sera également inversée.
		 *
		 * @param string $string La date de départ, par exemple "31/12/2007".
		 * @param string $srcDelimitor La séparation employée dans la date source. '-' par défaut.
		 * @param string $dstDelimitor La séparation employée dans la date finale. Par défaut, $srcDelimitor sera utilisé.
		 * @return string
		 */
		static public function reverseDate (string $string, string $srcDelimitor = '-', string $dstDelimitor = ''): string
		{
			if ( $srcDelimitor && $string )
			{
				$string = explode($srcDelimitor, trim($string));
			
				if ( count($string) === 3 )
				{
					if ( empty($dstDelimitor) )
						$dstDelimitor = $srcDelimitor;
				
					return $string[2].$dstDelimitor.$string[1].$dstDelimitor.$string[0];
				}
			}
			
			return '';
		}
	}	
