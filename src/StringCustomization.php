<?php

	/*
	* Libraries/php/LTK/StringCustomization.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	* Classe permettant de personnaliser des messages de masses dans une boucle.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class StringCustomization
	{
		private array $rules = [];
		private array $replacements = [];

		/** 
		 * Ajoute une règle de remplacement.
		 *
		 * @param string $key Une chaîne de caractères pour la clé à remplacer dans le texte final.
		 * @param string $dataKey Une chaîne de caractères pour la clé du tableau ou se trouve la donnée à remplacer.
		 * @param string $title Une chaîne de caractères optionnelle pour décrire le remplacement.
		 */
		public function addRule (string $key, string $dataKey, string $title = '')
		{
			$this->rules[] = [
				'key' => $key, 
				'data_key' => $dataKey, 
				'title' => $title
			];
		}

		/** 
		 * Retourne la liste des règles de remplacement.
		 *
		 * @return array
		 */
		public function rules (): array
		{
			return $this->rules;
		}

		/** 
		 * Crée un tableau de remplacement avec les valeurs réelles de remplacement sur base des règles.
		 *
		 * @param array $data Un tableau associatif contenant les données réelles à substituer aux règles. Exemple un profil d'utilisateur.
		 */
		public function setReplacementsData (array $data)
		{
			/* NOTE: On vire les anciennes données. */
			$this->replacements = [];

			foreach ( $this->rules as $rule )
			{
				if ( isset($data[$rule['data_key']]) === false )
				{
					trigger_error('"'.$rule['data_key'].'" key doesn\'t appears in data associative array !', E_USER_NOTICE);

					continue;
				}

				$this->replacements[$rule['key']] = $data[$rule['data_key']];
			}
		}

		/** 
		 * Permet avec une tableau de personnaliser une chaîne de caractères.
		 *
		 * @param string $string La chaîne de caractères à traiter.
		 * @return string
		 */
		public function customize (string $string): string
		{
			foreach ( $this->replacements as $key => $replace )
				$string = str_replace($key, $replace, $string);

			return $string;
		}
	};
