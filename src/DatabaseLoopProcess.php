<?php

	/*
	* Libraries/php/LTK/DatabaseLoopProcess.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	use PDO;

	/**
	* Permet d'effectuer une tâche sur les lignes d'une large table.
	* Il faut simplement indiquer la requête SQL de base et passer une fonction pour la tâche à effectuer.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class DatabaseLoopProcess
	{
		private PDO $database;
		private bool $noLimitOffset;
		private string $selectClause = '*';
		private string $baseQuery = '';
		private int $limit;
		
		/**
		* Retourne le nombre de ligne total.
		*
		* @return int.
		*/
		private function getTotalCount (): int
		{
			$statement = 'SELECT COUNT(1) '.$this->baseQuery.';';

			if ( ($PDOStatement = $this->database->query($statement)) !== false )
			{
				return intval($PDOStatement->fetchColumn());
			}
			else
			{
				$error = $this->database->errorInfo();

				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

				return 0;
			}
		}

		/**
		* Le constructeur.
		*
		* @param PDO $database Une instance de la classe PDO connectée pour accèder à la base de données.
		* @param bool $updateWillModifySelection Permet d'indiquer au processus qu'une modification des données (UPDATE) induit une reduction de la sélection (SELECT) pendant la boucle.
		* @param integer $limit Permet de choisir le nombre d'enregistrements chargé par palier. Par défaut, la valeur est à 1000.
		*/
		public function __construct (PDO $database, bool $updateWillModifySelection, int $limit = 1000)
		{
			$this->database = $database;
			$this->noLimitOffset = $updateWillModifySelection;
			$this->limit = $limit;
		}

		/**
		* Permet de choisir les champs qui seront utilisés lors de la requête. Par défaut, la requête fera "SELECT *".
		*
		* @param array $data Un tableau contenant les champs à sélectionner. Exemple: ['`id`', '`joinedtable`.`category_title`', 'title'].
		*/
		public function setSelectedFields (array $data)
		{
			$this->selectClause = implode(', ', $data);
		}

		/**
		* Permet de définir la requête de base.
		* NOTE: Ne pas inclure la clause SELECT ou LIMIT.
		*
		* @param string $query, La base de la requête, elle commence depuis la clause FROM et peut se terminer jusqu'à la clause ORDER.
		*/
		public function setQueryBase (string $query)
		{
			$this->baseQuery = $query;
		}

		/**
		* Exécute la boucle de traîtement.
		*
		* @param callable $function Une fonction dans la signature est "mixed function (array $data, int $element, int $elementCount, int $step, int $steppes)" 
		* - 1: Le tableau représentant les données de l'élément en cours.
		* - 2: Le numéro de l'élément traité dans la boucle.
		* - 3: Le nombre total d'éléments.
		* - 4: Le numéro de boucle en cours.
		* - 5: Le nombre total de boucle.
		* NOTE: Le processus s'arrête si la fonction utilisateur retourne false.
		* @param array $output Une référence vers un tableau pour stocker chaque retour de la fonction utilisateur.
		* @return bool|int
		*/
		public function execute (callable $function, array &$output): bool|int
		{
			$elementCount = $this->getTotalCount();

			if ( $elementCount <= 0 )
				return 0;

			$steppes = ceil($elementCount / $this->limit);

			for ( $step = 0; $step < $steppes; $step++ )
			{
				$statement = 
					'SELECT '.$this->selectClause.' '.
					$this->baseQuery.' '.
					( $this->noLimitOffset ? 'LIMIT '.$this->limit : 'LIMIT '.( $step * $this->limit ).', '.$this->limit ).
					';';

				/* On balance willy à la plage. */
				if ( ($PDOStatement = $this->database->query($statement)) === false )
				{
					$error = $this->database->errorInfo();

					trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

					break;
				}

				/* On récupère willy dans l'eau. */
				if ( $PDOStatement->rowCount() )
				{
					foreach ( $PDOStatement->fetchAll(\PDO::FETCH_ASSOC) as $element => $data )
					{
						$return = $function($data, $element, $elementCount, $step, $steppes);

						if ( $return === false )
						{
							trigger_error(__METHOD__.'(), loop ended by user function !', E_USER_WARNING);

							return false;
						}

						$output[] = $return;
					}
				}
				else
				{
					trigger_error(__METHOD__.'(), no result with query ('.($step + 1).'/'.$steppes.') !', E_USER_WARNING);

					return false;
				}
			}

			return $elementCount;
		}
	}
