<?php

	/*
	* Libraries/php/LTK/CSRF.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;
	
	use Exception;

	/**
	 * Implémentation de la protection CSRF pour les formulaires d'envoi de données.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class CSRF
	{
		const TokenName = 'ltk-csrf-token';
		const TokenHeaderName = 'X-CSRF-TOKEN';

		const Match = 0;
		const NotInitialized = 1;
		const NotFound = 2;
		const Mismatch = 3;

		/**
		 * Constructeur privé
		 *
		 * @internal
		 */
		private function __construct ()
		{

		}

		/**
		 * Démarre une session PHP.
		 *
		 * @internal
		 */
		static private function startSession ()
		{
			if ( session_status() == PHP_SESSION_NONE )
				session_start();
		}

		/**
		 * Vérifie dans les en-têtes HTTP si le token ne s'y trouve pas.
		 *
		 * @return string
		 */
		static public function getFromHeader (): string
		{
			$headerName = 'HTTP_'.self::TokenHeaderName;

			if ( !isset($_SERVER[$headerName]) || empty($_SERVER[$headerName]) )
				return '';

			return $_SERVER[$headerName];
		}

		/**
		 * Génère un token CSRF en session PHP.
		 *
		 * @return string
		 * @throws Exception
		 */
		static public function generate (): string
		{
			self::startSession();

			$_SESSION[self::TokenName] = bin2hex(random_bytes(64));

			return $_SESSION[self::TokenName];
		}

		/**
		 * Cherche un token valide dans les en-têtes HTTP, puis GET et POST et le compare avec celui de la session.
		 *
		 * @return int
		 */
		static public function validate (): int
		{
			self::startSession();

			if ( empty($_SESSION[self::TokenName]) )
				return self::NotInitialized;

			$userToken = self::getFromHeader();

			if ( empty($userToken) )
				$userToken = Tools::getValue(self::TokenName);

			if ( empty($userToken) )
				return self::NotFound;

			if ( $userToken !== $_SESSION[self::TokenName] )
				return self::Mismatch;

			return self::Match;
		}
	};
