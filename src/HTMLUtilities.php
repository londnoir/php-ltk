<?php

	/*
	* Libraries/php/LTK/HTMLUtilities.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe contient les outils pour manipuler du code HTML.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class HTMLUtilities
	{
		/** @internal */
		private function __construct () {}
		
		/**
		 * Extrait un tag d'un contenu HTML.
		 *
		 * @param string $HTMLBuffer Une chaîne de caractères pour le contenu HTML
		 * @param string $HTMLTag Une chaîne de caractères contenat le tag à extraire. Exemple, 'input'.
		 * @param string $subHTMLBuffer Une chaîne de caractères passée par référence pour récuperer la chaîne extraite.
		 * @param int $offset Un entier qui permet de définir le point de départ dans le contenu HTML. Par défaut, 0.
		 * @return bool
		 */
		static public function extractHTMLTag (string $HTMLBuffer, string $HTMLTag, string &$subHTMLBuffer, int $offset = 0): bool
		{
			switch ( $HTMLTag )
			{
				/* ie, <audio ... /> */
				case 'audio' :
				case 'video' :
				case 'img' :
				case 'br' :
				case 'hr' :
				case 'param' :
				case 'input' :
					$begin = '<'.$HTMLTag;
					$end = '>'; /* NOTE: sometimes devs forget '/' at the end of the tag. */
					break;
				
				/* ie, <a>...</a> */
				default :
					$begin = '<'.$HTMLTag;
					$end = '</'.$HTMLTag.'>';
					break;
			}
			
			/* Avoid to deal with upper case tag. */
			$lowerCaseHTMLBuffer = strtolower($HTMLBuffer);
			
			if ( ($startOffset = strpos($lowerCaseHTMLBuffer, $begin, $offset)) !== false )
			{
				if ( ($endOffset = strpos($lowerCaseHTMLBuffer, $end, $startOffset)) !== false )
				{
					$len = strlen($end);
					
					/* Send sub buffer to the reference. */
					$subHTMLBuffer = substr($HTMLBuffer, $startOffset, ($endOffset - $startOffset) + $len);
					
					/* Return the end offset to perform a while(). */
					return $endOffset + $len;
				}
			}
			
			return false;
		}
		
		/**
		 * Extrait le nom du premier tag d'un contenu HTML.
		 *
		 * @param string $HTMLBuffer Une chaîne de caractères pour le contenu HTML.
		 * @return string
		 */
		static public function getFirstTagName (string $HTMLBuffer): string
		{
			if ( ($startOffset = strpos($HTMLBuffer, '<')) !== false )
			{
				$endTagOffset = strpos($HTMLBuffer, '>', $startOffset);
				$endWhiteCharOffset = strpos($HTMLBuffer, ' ', $startOffset);
				
				if ( $endTagOffset !== false && $endWhiteCharOffset !== false )
					$endOffset = min($endTagOffset, $endWhiteCharOffset);
				elseif ( $endTagOffset !== false )
					$endOffset = $endTagOffset;
				elseif ( $endWhiteCharOffset !== false)
					$endOffset = $endTagOffset;
				else
					return '';
				
				/* Note we don't want the first char '<'. */
				$startOffset++;
				
				return substr($HTMLBuffer, $startOffset, $endOffset - $startOffset);
			}
			
			return '';
		}
		
		/**
		 * Extrait le contenu d'une balise de type <xxx>...</xxx>.
		 *
		 * @param string $HTMLBuffer Une chaîne de caractères pour le contenu HTML
		 * @return string
		 */
		static public function extractInnerHTMLTag (string $HTMLBuffer): string
		{
			if ( ($startOffset = strpos($HTMLBuffer, '>')) !== false && ($endOffset = strpos($HTMLBuffer, '<')) !== false )
			{
				$startOffset++;
				$endOffset--;
				
				return substr($HTMLBuffer, $startOffset, $endOffset - $startOffset);
			}
			
			return '';
		}
		
		/**
		 * Retourne un tableau associatif des attributes d'un tag HTML.
		 *
		 * @param string $HTMLBuffer Une chaîne de caractères pour le contenu HTML
		 * @return array
		 */
		static public function extractHTMLTagAttributes (string $HTMLBuffer): array
		{
			$attributes = array();
			
			if ( preg_match_all('#([a-zA-Z]+)="([^"]+)"#', $HTMLBuffer, $matches) )
			{
				foreach ( $matches[1] as $i => $match )
				{
					$attributeName = strtolower($match);
					
					$attributes[$attributeName] = trim($matches[2][$i]);
				}
			}
			
			return $attributes;
		}

		/**
		 * Crée un bouton de type input avec un formulaire html valide. Les boutons générés se situent dans une div utilisant la classe CSS "ltk_html_buttons".
		 *
		 * @param string $url une chaîne de caractères contenant l'URL ciblée. Si la méthod est "get", il faut impérativement utiliser le paramètre $options pour passer les variables.
		 * @param string $title une chaîne de caractères contenant le titre affiché sur le bouton.
		 * @param string $divID une chaîne de caractères contenant l'ID de la balise DIV englobant le formulaire. Par défaut null.
		 * @param array $variables un tableaux clés/valeurs permettant d'ajouter des variables. Par defaut vide.
		 * @param string $method une chaîne de caractères contenant la méthode employée pour générer l'action, soit "get" ou "post". Par défaut, "get".
		 * @param string $confirm une chaîne de caractères contenant une notification javascript pour confirmer l'action. Par défaut null.
		 * @return string
		 */
		static public function getHTMLButton (string $url, string $title, string $divID = '', array $variables = [], string $method = 'get', string $confirm = ''): string
		{
			if ( empty($url) || empty($title) )
				return '';

			/* Check method */
			switch ( strtolower($method) )
			{
				case 'get' :
				case 'post' :
					break;

				default :
					$method = 'get';
					break;
			}

			$HTMLBuffer =
				'<div'.( $divID ? ' id="'.$divID.'"' : null ).' class="ltk_html_buttons">'.
					'<form'.( $divID ? ' id="'.$divID.'_form"' : null ).' action="'.$url.'" method="'.$method.'">'.
						'<input'.( $divID ? ' id="'.$divID.'_submit"' : null ).' type="submit" value="'.$title.'"'.( $confirm ? ' onclick="javascript: return confirm(\''.self::HTMLStringToJS($confirm).'\');"' : null ).' />';
			
			if ( $variables )
			{
				foreach ( $variables as $key => $value )
					$HTMLBuffer .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
			}
			
			$HTMLBuffer .=
					'</form>'.
				'</div>';

			return $HTMLBuffer;
		}

		/**
		 * Crée un bouton de type input exécutant un code javascript.
		 *
		 * @param string $code une chaîne de caractères contenant le code à exécuter.
		 * @param string $label une chaîne de caractères contenant le titre affiché sur le bouton.
		 * @param string $id une chaîne de caractères contenant l'ID de la balise DIV englobant le formulaire. Par défaut ''.
		 * @param string $classname une chaîne de caractères contenant la classe CSS à utiliser sur le button. Par défaut ''.
		 * @return string
		 */
		static public function getJavascriptHTMLButton (string $code, string $label, string $id = '', string $classname = ''): string
		{
			if ( empty($code) || empty($label) )
				return '';

			return 
				'<div id="'.$id.'" class="'.$classname.'">'."\n".
				"\t".'<button onclick="'.$code.'">'.$label.'</button>'."\n".
				'</div>'."\n";
		}

		/**
		 * Convertit une chaîne de caractères HTML pour l'afficher dans une chaîne Javascript.
		 *
		 * @param string $string une chaîne de caractères contenant le code HTML.
		 * @param bool $doubleQuotes un booléen permettant de spécifier dans quel type de chaine JS sera utilisé le résultat. Entre guillemets par défaut.
		 * @return string
		 */
		static public function HTMLStringToJS (string $string, bool $doubleQuotes = true): string
		{
			$string = str_replace(array("\r", "\n", "\t"), null, $string);
			
			if ( $doubleQuotes )
			{
				$string = str_replace('"', '\"', $string);
				$string = str_replace('</script', '</scr"+"ipt', $string);
				$string = str_replace('<script', '<scr"+"ipt', $string);
			}
			else
			{
				$string = str_replace('\'', '\\\'', $string);
				$string = str_replace('</script', '</scr\'+\'ipt', $string);
				$string = str_replace('<script', '<scr\'+\'ipt', $string);
			}
			
			return $string;
		}
	}
