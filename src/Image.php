<?php

	/*
	* Libraries/php/LTK/Image.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	/**
	 * Facilite la conversion des fichiers images et les opérations de redimensionnements.
	 * Cette classe utilise GD pour redimensionner avec la possibilité de conserver la transparence,
	 * le format de l'image ou rajouter des bordures.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
   	class Image
	{
		/** L'indice de la composante rouge dans un tableau représentant une couleur. */
		const R = 0;
		/** L'indice de la composante verte dans un tableau représentant une couleur. */
		const G = 1;
		/** L'indice de la composante bleue dans un tableau représentant une couleur. */
		const B = 2;
		/** L'indice de la composante de transparence (alpha) dans un tableau représentant une couleur. */
		const A = 3;
		
		/** Image en mode paysage. */
		const LandScapeMode = 100;
		/** Image en mode portrait. */
		const PortraitMode = 101;
		/** Image carrée. */
		const SquareMode = 102;
		
		/** Sortie en compression Targa. */
		const TGA = 200;
		/** Sortie en compression Jpeg. */
		const JPG = 201;
		/** Sortie en compression GIF. */
		const GIF = 202;
		/** Sortie en compression PNG. */
		const PNG = 203;
		
		private bool $isReady = false;

		private string $inputFilepath = '';
		private int $inputWidth = 0;
		private int $inputHeight = 0;
		private string $inputFormat = '';
		private mixed $GDInput = null;
		
		private array $backgroundColor = [0.0, 0.0, 0.0, 1.0];
		
		private int $borderSize = 0;
		private array $borderColor = [0.0, 0.0, 0.0, 1.0];

		private float $quality = 0.9;
		private int $outputWidth = 0;
		private int $outputHeight = 0;
		private int $outputFormat = self::JPG;
		private mixed $GDOutput = null;
		
		/**
		 * Le constructeur.
		 *
		 * @param string $filepath Une chaîne de caractères pointant le fichier de l'image sur le serveur.
		 */
		public function __construct (string $filepath)
		{
			if ( file_exists($filepath) )
			{
				$isImage = true;
				$infos = getimagesize($filepath);
				
				// GD input canvas
				switch ( $infos['mime'] )
				{
					// TGA file
					case 'application/tga' :
					case 'application/x-tga' :
					case 'application/x-targa' :
					case 'image/tga' :
					case 'image/x-tga' :
					case 'image/targa' :
					case 'image/x-targa' :
						$this->inputFormat = self::TGA;
						$this->GDInput = imagecreatefromtga($filepath);
						break;
						
					// Jpeg file
					case 'image/jpeg' :
					case 'image/jpg' :
					case 'image/jp_' :
					case 'application/jpg' :
					case 'application/x-jpg' :
					case 'image/pjpeg' :
					case 'image/pipeg' :
					case 'image/vnd.swiftview-jpeg' :
						$this->inputFormat = self::JPG;
						$this->GDInput = imagecreatefromjpeg($filepath);
						break;
						
					// GIF file
					case 'image/gif' :
					case 'image/gi_' :
						$this->inputFormat = self::GIF;
						$this->GDInput = imagecreatefromgif($filepath);
						break;
						
					// PNG file
					case 'image/png' :
					case 'application/png' :
					case 'application/x-png' :
						$this->inputFormat = self::PNG;
						$this->GDInput = imagecreatefrompng($filepath);
						break;
						
					default :
						trigger_error(__METHOD__.'(), file "'.$filepath.'" is not an image or format is not handled ! Mime-type detected : '.$infos['mime'], E_USER_NOTICE);
						
						$isImage = false;
						break;
				}
				
				if ( $isImage )
				{
					$this->inputFilepath = $filepath;
					$this->inputWidth = $infos[0];
					$this->inputHeight = $infos[1];
					
					$this->isReady = true;
				}
			}
			else
			{
				trigger_error(__METHOD__.'(), file "'.$filepath.'" not exists !', E_USER_NOTICE);
			}
		}
		
		/**
		 * Le destructeur.
		 */
		public function __destruct ()
		{
			if ( $this->GDInput )
				imageDestroy($this->GDInput);

			if ( $this->GDOutput )
				imageDestroy($this->GDOutput);
		}
		
		/**
		 * Indique la largeur de l'image.
		 *
		 * @return int
		 */
		public function width (): int
		{
			if ( !$this->isReady )
				return false;
			
			return $this->inputWidth;
		}
		
		/**
		 * Indique la hauteur de l'image.
		 *
		 * @return int
		 */
		public function height (): int
		{
			if ( !$this->isReady )
				return false;
			
			return $this->inputHeight;
		}
		
		/**
		 * Indique le format d'image (ratio) de l'image.
		 *
		 * @return float
		 */
		public function aspectRatio (): float
		{
			if ( !$this->isReady )
				return false;
			
			return $this->inputWidth / max(1, $this->inputHeight);
		}
		
		/**
		 * Indique l'orientation de l'image avec les constantes de la classe suivantes : Image::LandScapeMode, Image::SquareMode or Image::PortraitMode.
		 *
		 * @return int Une constante de classe ou false si aucune image n'est chargée.
		 */
		public function pictureMode (): int
		{
			if ( !$this->isReady )
				return 0;
			
			if ( $this->inputWidth > $this->inputHeight )
				return self::LandScapeMode;
			elseif ( $this->inputWidth == $this->inputHeight )
				return self::SquareMode;
			else
				return self::PortraitMode;
		}
		
		/**
		 * Indique le format de l'image avec les constantes de la classe suivantes : Image::TGA, Image::JPG, Image::GIF or Image::PNG.
		 *
		 * @return int Une constante de classe ou false si aucune image n'est chargée.
		 */
		public function inputFormat (): int
		{
			if ( !$this->isReady )
				return 0;

			return $this->inputFormat;
		}
		
		/**
		 * Donne l'extension de l'image.
		 * Cette méthode utilise le type mime du fichier et non son extension.
		 *
		 * @param string $filepath Le fichier à analyser.
		 * @return string
		 */
		static public function getFileExtension (string $filepath): string
		{
			if ( file_exists($filepath) )
			{
				$infos = getimagesize($filepath);
				
				switch ( $infos['mime'] )
				{
					case 'application/tga' :
					case 'application/x-tga' :
					case 'application/x-targa' :
					case 'image/tga' :
					case 'image/x-tga' :
					case 'image/targa' :
					case 'image/x-targa' :
						return 'tga';

					case 'image/jpeg' :
					case 'image/jpg' :
					case 'image/jp_' :
					case 'application/jpg' :
					case 'application/x-jpg' :
					case 'image/pjpeg' :
					case 'image/pipeg' :
					case 'image/vnd.swiftview-jpeg' :
						return 'jpg';

					case 'image/gif' :
					case 'image/gi_' :
						return 'gif';

					case 'image/png' :
					case 'application/png' :
					case 'application/x-png' :
						return 'png';
				}
			}
			
			return '';
		}
		
		/**
		 * Sauvegarde l'image dans un fichier.
		 *
		 * @internal
		 * @param string $filepath Une chaîne de caractères pour le chemin sur le serveur où sera enregistré le fichier.
		 * @return bool.
		 */
		private function save (string $filepath): bool
		{
			if ( file_exists($filepath) )
			{
				if ( !is_writable($filepath) )
				{
					trigger_error(__METHOD__.'(), file "'.$filepath.'" is not writable !', E_USER_WARNING);
					
					return false;
				}
			}
			else
			{
				$dir = dirname($filepath);
				
				if ( is_dir($dir) )
				{
					if ( !is_writable($dir) )
					{
						trigger_error(__METHOD__.'(), directory "'.$dir.'" is not writable !', E_USER_WARNING);
					
						return false;
					}
				}
				else
				{
					if ( !Path::build($dir) )
					{
						trigger_error(__METHOD__.'(), unable to create "'.$dir.'" dir !', E_USER_WARNING);
						
						return false;
					}
				}
			}
			
			switch ( $this->outputFormat )
			{
				case self::JPG :
					// Compression : 0 -> 100
					$quality = round( $this->quality * 100 );
					
					if ( !imagejpeg($this->GDOutput, $filepath, $quality) )
					{
						trigger_error(__METHOD__.'(), unable to save image with jpeg format !', E_USER_WARNING);
						
						return false;
					}
					break;
					
				case self::PNG :
					// Compression : 9 -> 0
					$quality = round( 9 - ($this->quality * 9) );
					
					if ( !imagepng($this->GDOutput, $filepath, $quality) )
					{
						trigger_error(__METHOD__.'(), unable to save image with png format !', E_USER_WARNING);
						
						return false;
					}
					break;
					
				default :
					trigger_error(__METHOD__.'(), trying to save image without a suitable output format ('.$this->outputFormat.') !', E_USER_WARNING);
					break;
			}
			
			return true;
		}
		
		/**
		 * Vérifie les composantes d'une couleur exprimée dans une tableau de nombre à virgule flottante.
		 *
		 * @internal
		 * @param array $color Un tableau de 4 scalaires représenant les composantes rouge, vert, blue et alpha.
		 * @return array Un tableau de 4 scalaires.
		 */
		private function validateDecimalColor (array $color): array
		{
			$validColor = [
				self::R => 0.0,
				self::G => 0.0,
				self::B => 0.0,
				self::A => 1.0
			];
			
			/* Count the min number of component */
			$components = min(4, count($color));
			
			for ( $i = 0; $i < $components; $i ++ )
			{
				/* force float as var type */
				$float = floatval($color[$i]);
				
				/* clamp to scalar */
				$validColor[$i] = min(1.0, abs($float));
			}
			
			return $validColor;
		}
		
		/**
		 * Convertit un nombre scalaire en un nombre entier de 256 valeurs pour les composantes de couleur.
		 *
		 * @internal
		 * @param float $value Un nombre scalaire.
		 * @return int.
		 */
		private function rgbConv (float $value): int
		{
			return round($value * 255);
		}
		
		/**
		 * Convertit un nombre scalaire en un nombre entier de 256 valeurs pour le canal alpha.
		 *
		 * @internal
		 * @param float $value Un nombre scalaire.
		 * @return int.
		 */
		private function alphaConv (float $value): int
		{
			return 127 - round($value * 127);
		}
		
		/**
		 * Ajoute une bordure à l'image.
		 *
		 * @param int $size Un nombre entier pour la largeur de la bordure, exprimée en pixel.
		 * @param array $color Un tableau de 4 scalaires représentant une couleur. Noir par défaut.
		 * @return self.
		 */
		public function setBorder (int $size, array $color = []): self
		{
			if ( $size > 0 )
				$this->borderSize = $size;
			
			if ( !empty($color) )
				$this->borderColor = $this->validateDecimalColor($color);
			
			return $this;
		}
		
		/**
		 * Ajoute un fond de couleur unie. Utile pour le traitement des images contenant un canal alpha vers un format sans transparence.
		 *
		 * @param array $color Un tableau de 4 scalaires représentant une couleur.
		 * @return self.
		 */
		public function setBackgroundColor (array $color): self
		{
			$this->backgroundColor = $this->validateDecimalColor($color);
			
			return $this;
		}
		
		/**
		 * Définit le format de l'image de sortie.
		 *
		 * @param int $format Une constante de la classe qui peut-être Image::GIF, Image::TGA, Image::PNG ou Image::JPG.
		 * @return self.
		 */
		public function setOutputFormat (int $format): self
		{
			$this->outputFormat = $format;
			
			return $this;
		}
		
		/**
		 * Définit la qualité de compression de l'image de sortie.
		 * Ce paramètre est ignoré dans les formats sans compression.
		 *
		 * @param float $quality Un nombre scalaire où 1.0 est la meilleure qualité possible.
		 * @return self.
		 */
		public function setQuality (float $quality): self
		{
			if ( $quality < 0.0 )
				$this->quality = 0.0;
			elseif ( $quality > 1.0 )
				$this->quality = 1.0;
			else
				$this->quality = $quality;
			
			return $this;
		}
		
		/**
		 * Indique si une image est chargée.
		 *
		 * @return bool
		 */
		public function isImage (): bool
		{
			return $this->isReady;
		}
		
		/**
		 * Internal funtion to apply the background color.
		 *
		 * @internal
		 */
		private function applyBackground ()
		{
			if ( $this->backgroundColor[self::A] > 0.0 )
			{
				/* No opacity */
				if ( $this->backgroundColor[self::A] == 1.0 )
				{
					$bgColor = imageColorAllocate(
						$this->GDOutput, 
						$this->rgbConv($this->backgroundColor[self::R]), 
						$this->rgbConv($this->backgroundColor[self::G]), 
						$this->rgbConv($this->backgroundColor[self::B])
					);
				}
				else
				{
					imagesavealpha($this->GDOutput, true);
					
					$bgColor = imagecolorallocatealpha(
						$this->GDOutput, 
						$this->rgbConv($this->backgroundColor[self::R]), 
						$this->rgbConv($this->backgroundColor[self::G]), 
						$this->rgbConv($this->backgroundColor[self::B]), 
						$this->alphaConv($this->backgroundColor[self::A])
					);
				}
				
				imagefill($this->GDOutput, 0, 0, $bgColor);
			}
		}
		
		/**
		 * Internal funtion to apply the border.
		 *
		 * @internal
		 */
		private function applyBorder ()
		{
			if ( $this->borderSize > 0 && $this->borderColor[self::A] > 0.0 )
			{
				/* No opacity */
				if ( $this->borderColor[self::A] == 1.0 )
				{
					$bgColor = imageColorAllocate(
						$this->GDOutput, 
						$this->rgbConv($this->borderColor[self::R]), 
						$this->rgbConv($this->borderColor[self::G]), 
						$this->rgbConv($this->borderColor[self::B])
					);
				}
				else
				{
					imagesavealpha($this->GDOutput, true);
					
					$bgColor = imagecolorallocatealpha(
						$this->GDOutput, 
						$this->rgbConv($this->borderColor[self::R]), 
						$this->rgbConv($this->borderColor[self::G]), 
						$this->rgbConv($this->borderColor[self::B]), 
						$this->alphaConv($this->borderColor[self::A])
					);
				}
				
				/* border-top */
				imagefilledrectangle($this->GDOutput, 
					0, 
					0, 
					$this->outputWidth, 
					$this->borderSize - 1, 
					$bgColor);
				/* border-bottom */
				imagefilledrectangle($this->GDOutput, 
					0, 
					$this->outputHeight, 
					$this->outputWidth, 
					$this->outputHeight - 
					$this->borderSize, 
					$bgColor);
				/* border-left */
				imagefilledrectangle(
					$this->GDOutput, 
					0, 
					$this->borderSize, 
					$this->borderSize - 1, 
					$this->outputHeight - ($this->borderSize + 1), 
					$bgColor);
				/* border-right */
				imagefilledrectangle(
					$this->GDOutput, 
					$this->outputWidth, 
					$this->borderSize, 
					$this->outputWidth - $this->borderSize, 
					$this->outputHeight - ($this->borderSize + 1), 
					$bgColor);
			}
		}
		
		/**
		 * Convertit le format d'une image et sauvegarde le fichier.
		 *
		 * @param string $filepath Le chemin du fichier de sortie sur le serveur.
		 * @return int 0 : Aucune erreur.\n
		 * 1 : Pas d'image.\n
		 * 2 : L'échantillonnage GD a échoué.\n
		 * 3 : Erreur d'écriture avec le fichier.\n
		 */
		public function convert (string $filepath): int
		{
			if ( !$this->isReady )
			{
				trigger_error(__METHOD__.'(), image was not ready !', E_USER_WARNING);
				
				return 1;
			}
			
			/* Création de la ressource GD de sortie */
			$this->outputWidth = $this->inputWidth;
			$this->outputHeight = $this->inputHeight;
			$this->GDOutput = imagecreatetruecolor($this->outputWidth, $this->outputHeight);
			
			/* Set the background color */
			$this->applyBackground();
			
			$returnCode = 0;
			
			/* Resampling input vers output */
			if ( imagecopy(
				$this->GDOutput, /* Ressource GD de sortie */
				$this->GDInput, /* Ressources GD d'entrée */
				0, 0, /* XY sur la sortie */
				0, 0, /* XY sur l'entrée */
				$this->inputWidth, $this->inputHeight /* longueur et largeur depuis XY sur l'entrée */
			) )
			{
				$this->applyBorder();
				
				if ( !$this->save($filepath) )
					$returnCode = 3;
			}
			else
			{
				trigger_error(__METHOD__.'(), unable to resamble GD surface !', E_USER_WARNING);
				
				$returnCode = 2;
			}
			
			return $returnCode;
		}
		
		/**
		 * Redimensionne une image et sauvegarde le fichier.
		 *
		 * @param int $outputWidth La largeur de l'image de sortie.
		 * @param int $outputHeight La hauteur de l'image de sortie.
		 * @param string $filepath Le chemin du fichier de sortie sur le serveur.
		 * @param bool $keepAR Indique s'il faut conserver ou non l'aspect de l'image. Activé par défaut.
		 * @param bool $keepFullImage Indique que l'on préfère découper dans l'image que d'afficher des bordures. Désactivé par défaut.
		 * @return int 0 : Aucune erreur.\n
		 * 1 : Pas d'image.\n
		 * 2 : L'échantillonnage GD a échoué.\n
		 * 3 : Erreur d'écriture avec le fichier.\n
		 * 4 : La dimension de sortie est nulle.\n
		 */
		public function resize (int $outputWidth, int $outputHeight, string $filepath, bool $keepAR = true, bool $keepFullImage = false): int
		{
			if ( !$this->isReady )
			{
				trigger_error(__METHOD__.'(), image was not ready !', E_USER_WARNING);
				
				return 1;
			}
			
			/* Création de la ressource GD de sortie */
			$this->outputWidth = intval($outputWidth);
			$this->outputHeight = intval($outputHeight);
			
			if ( $this->outputWidth < 1 || $this->outputHeight < 1 )
			{
				trigger_error(__METHOD__.'(), image output size invalid !', E_USER_WARNING);
				
				return 4;
			}
			
			$this->GDOutput = imagecreatetruecolor($this->outputWidth, $this->outputHeight);
			
			/* Setting the background */
			$this->applyBackground();
			
			/* Coordinates in the source image from where 
			starting the copy into the output. */
			$xShift = 0;
			$yShift = 0;
			
			/* Dimensions of the image part 
			to copy into the output. */
			$clampWidth = $this->inputWidth;
			$clampHeight = $this->inputHeight;
			
			if ( $keepAR )
			{
				/* NOTE : landscape > 1.0, portrait < 1.0, square = 1.0 */
				$inputRatio = $this->inputWidth / $this->inputHeight;
				$outputRatio = $this->outputWidth / $this->outputHeight;
			
				/* Si les ratios sont différent ont doit découper les bords de l'image d'entrée */
				if ( $inputRatio != $outputRatio )
				{
					if ( $inputRatio >= 1.0 ) // From Landscape ...
					{
						if ( $outputRatio >= 1.0 ) // ... to Landscape
						{
							if ( $inputRatio > $outputRatio )
								$ratio = $keepFullImage ? $this->inputWidth / $this->outputWidth : $this->inputHeight / $this->outputHeight;
							else
								$ratio = $keepFullImage ? $this->inputHeight / $this->outputHeight : $this->inputWidth / $this->outputWidth;
						}
						else // ... to Portrait
						{
							$ratio = $keepFullImage ? $this->inputWidth / $this->outputWidth : $this->inputHeight / $this->outputHeight;
						}
					}
					else // From Portrait ...
					{
						if ( $outputRatio >= 1.0 ) // ... to Landscape
						{
							$ratio = $keepFullImage ? $this->inputHeight / $this->outputHeight : $this->inputWidth / $this->outputWidth;
						}
						else // ... to Portrait
						{
							if ( $inputRatio > $outputRatio )
								$ratio = $keepFullImage ? $this->inputWidth / $this->outputWidth : $this->inputHeight / $this->outputHeight;
							else
								$ratio = $keepFullImage ? $this->inputHeight / $this->outputHeight : $this->inputWidth / $this->outputWidth;
						}
					}
					
					$clampWidth = round($this->outputWidth * $ratio);
					$clampHeight = round($this->outputHeight * $ratio);
				
					$xShift = round(($this->inputWidth - $clampWidth) / 2);
					$yShift = round(($this->inputHeight - $clampHeight) / 2);
				}
			}
			
			$returnCode = 0;
			
			/* Resampling input vers output */
			if ( imagecopyresampled(
				$this->GDOutput, // Output GD resource
				$this->GDInput, // Input GD resource
				0, 0, // Outputting to XY
				$xShift, $yShift, // Copying from XY
				$this->outputWidth, $this->outputHeight, // Width and height limits on the output
				$clampWidth, $clampHeight // Width and height of copied data
			) )
			{
				$this->applyBorder();
				
				if ( !$this->save($filepath) )
					$returnCode = 3;
			}
			else
			{
				trigger_error(__METHOD__.'(), unable to resample GD surface !', E_USER_WARNING);
				
				$returnCode = 2;
			}
			
			return $returnCode;
		}
		
		/**
		 * Redimensionne une image et sauvegarde le fichier. Version paramétrique.
		 *
		 * @param int $x Le point de départ en X sur l'image source.
		 * @param int $y Le point de départ en Y sur l'image source.
		 * @param int $w La largeur converservée depuis le point de départ sur l'image source.
		 * @param int $h La hauteur converservée depuis le point de départ sur l'image source.
		 * @param int $outputWidth La largeur de l'image de sortie.
	 	 * @param int $outputHeight La hauteur de l'image de sortie.
		 * @param string $filepath Le chemin du fichier de sortie sur le serveur.
		 * @return int 0 : Aucune erreur.\n
		 * 1 : Pas d'image.\n
		 * 2 : L'échantillonnage GD a échoué.\n
		 * 3 : Erreur d'écriture avec le fichier.\n
		 */
		public function parametricResize (int $x, int $y, int $w, int $h, int $outputWidth, int $outputHeight, string $filepath): int
		{
			if ( !$this->isReady )
			{
				trigger_error(__METHOD__.'(), image was not ready !', E_USER_WARNING);
				
				return 1;
			}
			
			/* Check X start offset */
			$x = intval($x);
			if ( $x < 0 )
				$x = 0;
			
			/* Check X length */
			$w = intval($w);
			if ( ($x + $w) > $this->inputWidth )
				$w -= (($x + $w) - $this->inputWidth);
			
			/* Check Y start offset */
			$y = intval($y);
			if ( $y < 0 )
				$y = 0;
			
			/* Check Y length */
			$h = intval($h);
			if ( ($y + $h) > $this->inputHeight )
				$h -= (($y + $h) - $this->inputHeight);
			
			/* Création de la ressource GD de sortie */
			$this->outputWidth = intval($outputWidth);
			$this->outputHeight = intval($outputHeight);
			$this->GDOutput = imagecreatetruecolor($this->outputWidth, $this->outputHeight);
			
			/* Setting the background */
			$this->applyBackground();
			
			$returnCode = 0;
			
			/* Resampling input vers output */
			if ( imagecopyresampled(
				$this->GDOutput, // Output GD resource
				$this->GDInput, // Input GD resource
				0, 0, // Outputting to XY
				intval($x), intval($y), // Copying from XY
				$this->outputWidth, $this->outputHeight, // Width and height limits on the output
				intval($w), intval($h) // Width and height of copied data
			) )
			{
				$this->applyBorder();
				
				if ( !$this->save($filepath) )
					$returnCode = 3;
			}
			else
			{
				trigger_error(__METHOD__.'(), unable to resample GD surface !', E_USER_WARNING);
				
				$returnCode = 2;
			}
			
			return $returnCode;
		}
		
		/**
		 * Fix for imagecopymerge wich skip alpha channel.
		 *
		 * @internal
		 */
		private function imagecopymerge_alpha ($dst_im, $src_im, int $dst_x, int $dst_y, int $src_x, int $src_y, int $src_w, int $src_h, int $pct): bool
		{
			// creating a cut resource
			$cut = imagecreatetruecolor($src_w, $src_h);
			
			// copying relevant section from background to the cut resource
			imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
			
			// copying relevant section from watermark to the cut resource
			imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
			
			// insert cut resource to destination image
			return imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
		} 
		
		/**
		 * Colle une image sur une autre.
		 *
		 * @param string $filePath Le chemin sur le serveur de l'image à coller.
		 * @param int $x Le point de départ en X sur l'image source.
 		 * @param int $y Le point de départ en Y sur l'image source.
	 	 * @param int $alpha Un nombre de 0 à 100 pour indiquer la transparence de l'image source. Par défaut, le paramètre est à 100.
		 * @return int 0 : Aucune erreur.\n
		 * 1 : Aucune image de base.\n
		 * 2 : Problème d'ouverture de l'image source.\n
		 * 3 : Paramètres sans effet.\n
		 * 4 : Erreur lors de la copie.\n
		 */
		public function blit (string $filePath, int $x = 0, int $y = 0, int $alpha = 100): int
		{
			if ( !$this->isReady )
			{
				trigger_error(__METHOD__.'(), image was not ready !', E_USER_WARNING);
				
				return 1;
			}
			
			/* Check alpha. */
			if ( $alpha < 1 )
				return 3;
			elseif ( $alpha > 100 )
				$alpha = 100;
			
			$copy = new Image($filePath);
		
			if ( !$copy->isImage() )
				return 2;
			
			/* Copy the full image. */
			$startX = 0;
			$startY = 0;
			$endX = $copy->inputWidth;
			$endY = $copy->inputHeight;
			
			/* Check for overflow on X axis. */
			$offsetX = $x + $copy->inputWidth;

			if ( $offsetX > 0 && $x < $this->inputWidth )
			{
				/* check start. */
				if ( $x < 0 )
				{
					$delta = abs($x);
					
					$startX = $copy->inputWidth - $delta;
					$endX -= $delta;
					$x = 0;
				}
				
				/* check end */
				if ( $offsetX > $this->inputWidth )
					$endX = $copy->inputWidth - ( $offsetX - $this->inputWidth );
			}
			else
			{
				/* Outside on X */
				return 3;
			}
			
			/* Check for overflow on Y axis */
			$offsetY = $y + $copy->inputHeight;

			if ( $offsetY > 0 && $y < $this->inputHeight )
			{
				/* check start */
				if ( $y < 0 )
				{
					$delta = abs($y);
					
					$startY = $copy->inputWidth - $delta;
					$endY -= $delta;
					$y = 0;
				}
				
				/* check end */
				if ( $offsetY > $this->inputHeight )
					$endY = $copy->inputHeight - ( $offsetY - $this->inputHeight );
			}
			else
			{
				/* Outside on Y */
				return 3;
			}
			
			if ( $alpha == 100 )
			{
				if ( !imagecopy($this->GDInput, $copy->GDInput, $x, $y, $startX, $startY, $endX, $endY) )
					return 4;
			}
			else
			{
				//if ( !imagecopymerge($this->GDInput, $copy->GDInput, $x, $y, $startX, $startY, $endX, $endY, $alpha) )
				if ( !self::imagecopymerge_alpha($this->GDInput, $copy->GDInput, $x, $y, $startX, $startY, $endX, $endY, $alpha) )
					return 4;
			}
			
			return 0;
		}
	}
	
