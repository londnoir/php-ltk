<?php

	/*
	* Libraries/php/LTK/Headers.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	/**
	* Classe pour écrire proprement les en-têtes d'un email ou d'une requête HTTP.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class Headers
	{
		/** LF (Line Feed) pour les séparations de lignes. */
		const UnixLike = "\n";
		/** CR+LF (Carriage Return + Line Feed) pour les séparations de lignes. */
		const RFC2822 = "\r\n";

		const Accept = 'Accept';
		const AcceptCharset = 'Accept-Charset';
		const AcceptEncoding = 'Accept-Encoding';
		const AcceptLanguage = 'Accept-Language';
		const AcceptDatetime = 'Accept-Datetime';
		const AcceptRanges = 'Accept-Ranges';
		const AccessControlAllowOrigin = 'Access-Control-Allow-Origin';
		const Age = 'Age';
		const Allow = 'Allow';
		const ArchivedAt = 'Archived-At';
		const Authorization = 'Authorization';
		const AutoSubmitted = 'Auto-Submitted';
		const Bcc = 'Bcc';
		const CacheControl = 'Cache-Control';
		const Connection = 'Connection';
		const Cookie = 'Cookie';
		const Cc = 'Cc';
		const ContentDisposition = 'Content-Disposition';
		const ContentEncoding = 'Content-Encoding';
		const ContentLanguage = 'Content-Language';
		const ContentLength = 'Content-Length';
		const ContentLocation = 'Content-Location';
		const ContentMD5 = 'Content-MD5';
		const ContentRange = 'Content-Range';
		const ContentSecurityPolicy = 'Content-Security-Policy';
		const ContentTransferEncoding = 'Content-Transfer-Encoding';
		const ContentType = 'Content-Type';
		const Date = 'Date';
		const DNT = 'DNT';
		const ETag = 'ETag';
		const Expect = 'Expect';
		const Expires = 'Expires';
		const From = 'From';
		const FrontEndHttps = 'Front-End-Https';
		const Host = 'Host';
		const IfMatch = 'If-Match';
		const IfModifiedSince = 'If-Modified-Since';
		const IfNoneMatch = 'If-None-Match';
		const IfRange = 'If-Range';
		const IfUnmodifiedSince = 'If-Unmodified-Since';
		const InReplyTo = 'In-Reply-To';
		const LastModified = 'Last-Modified';
		const Link = 'Link';
		const Location = 'Location';
		const MaxForwards = 'Max-Forwards';
		const MessageID = 'Message-ID';
		const MIMEVersion = 'MIME-Version';
		const Origin = 'Origin';
		const P3P = 'P3P';
		const Pragma = 'Pragma';
		const Precedence = 'Precedence';
		const ProxyAuthenticate = 'Proxy-Authenticate';
		const ProxyAuthorization = 'Proxy-Authorization';
		const ProxyConnection = 'Proxy-Connection';
		const Range = 'Range';
		const Received = 'Received';
		const ReceivedSPF = 'Received-SPF';
		const References = 'References';
		const Referer = 'Referer';
		const Refresh = 'Refresh';
		const ReplyTo = 'Reply-To';
		const RetryAfter = 'Retry-After';
		const ReturnPath = 'Return-Path';
		const Sender = 'Sender';
		const Server = 'Server';
		const SetCookie = 'Set-Cookie';
		const Status = 'Status';
		const StrictTransportSecurity = 'Strict-Transport-Security';
		const Subject = 'Subject';
		const TE = 'TE';
		const To = 'To';
		const Trailer = 'Trailer';
		const TransferEncoding = 'Transfer-Encoding';
		const Upgrade = 'Upgrade';
		const UserAgent = 'User-Agent';
		const Vary = 'Vary';
		const VBRInfo = 'VBR-Info';
		const Via = 'Via';
		const Warning = 'Warning';
		const WWWAuthenticate = 'WWW-Authenticate';
		const XATTDeviceId = 'X-ATT-DeviceId';
		const XContentSecurityPolicy = 'X-Content-Security-Policy';
		const XContentTypeOptions = 'X-Content-Type-Options';
		const XForwardedFor = 'X-Forwarded-For';
		const XForwardedProto = 'X-Forwarded-Proto';
		const XFrameOptions = 'X-Frame-Options';
		const XPoweredBy = 'X-Powered-By';
		const XRequestedWith = 'X-Requested-With';
		const XUACompatible = 'X-UA-Compatible';
		const XWapProfile = 'X-Wap-Profile';
		const XWebKitCSP = 'X-WebKit-CSP';
		const XXSSProtection = 'X-XSS-Protection';	

		private string $eol = self::UnixLike;
		private array $headers = [];

		/**
		 * Le constructeur.
		 *
		 * @param string $buffer Une chaîne de caractères contenant des en-têtes à parser. Par défaut vide.
		 */
		public function __construct (string $buffer = '')
		{
			if ( empty($buffer) )
				return;
			
			$this->parse($buffer);
		}

		/** 
		 * Permet de changer le style de retour de ligne.
		 *
		 * @param string $type Une constance de classe. Elle peut être Headers::UnixLike pour utiliser seulement des retours chariot (CR) \n
		 * ou Headers::RFC2822 pour utiliser les retours chariot suivit d'un passage à la ligne (CRLF).
		 * @return self
		 */
		public function setLineBreak (string $type): self
		{
			switch ( $type )
			{
				case self::UnixLike :
				case self::RFC2822 :
					$this->eol = $type;
					break;

				default :
					trigger_error(__METHOD__.', bad type !');
					break;
			}

			return $this;
		}

		/** 
		 * Permet de parser des en-têtes au format texte.
		 *
		 * @param string $buffer Une chaîne de caractères contenant les en-têtes à parser.
		 * @return bool
		 */
		public function parse (string $buffer): bool
		{
			if ( $lines = explode("\n", $buffer) )
			{
				foreach ( $lines as $line )
				{
					if ( empty($line) )
						continue;

					$chunks = explode(':', $line, 2);

					if ( count($chunks) != 2 )
					{
						trigger_error(__METHOD__.', "'.$line.'" don\'t seems to be a valid header !');

						continue;
					}

					$key = trim($chunks[0]);
					$value = trim($chunks[1]);

					$this->headers[$key] = $value;
				}

				return true;
			}
			else
			{
				trigger_error(__METHOD__.', no headers !');

				return false;
			}
		}

		/** 
		 * Ajoute un champ dans les en-têtes en cours.
		 *
		 * @param string $key Une chaîne de caractères contenant le nom du champ à ajouter.
		 * @param string $value Une chaîne de caractères contenant la valeur du champ à ajouter.
		 * @return self
		 */
		public function add (string $key, string $value): self
		{
			$this->headers[$key] = $value;

			return $this;
		}

		/** 
		 * Permet de lire le contenu d'un champ dans les en-têtes en cours.
		 *
		 * @param string $key Une chaîne de caractères contenant le nom du champ à lire.
		 * @return string
		 */
		public function get (string $key): string
		{
			return $this->headers[$key] ?? '';
		}

		/** 
		 * Permet de récupèrer les en-têtes au format texte.
		 *
		 * @return string
		 */
		public function getText (): string
		{
			$buffer = null;

			foreach ( $this->headers as $key => $value )
				$buffer .= $key.': '.$value.$this->eol;
			$buffer .= $this->eol;

			return $buffer;
		}

		/** Affiche directement les en-têtes au format texte. */
		public function printText ()
		{
			echo $this->getText();
		}
	}
