<?php

	/*
	* Libraries/php/LTK/Storage.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;
	
	/**
	 * Classe de gestion de larges banques de fichiers.
	 * Cette classe permet de répartir des fichiers selon un numéro d'enregistrement sur le disque dur et de les récuperer facilement au moment souhaité.\n
	 * Exemple : [BASE][TYPE][SECTION][FICHIER] soit [http://www.monsite.com/stockage/][images/][1000/39/][39.jpg]
	 *
	 * Cet objet est configurable avec Settings et les clés suivantes :\n
	 * - 'storage_mode', le mode de stockage peut être Storage::ContinuousMode qui triera les dossier par segment numéroté (1000, 2000, ...)
	 * ou Storage::SparseMode qui dispersera les dossiers selon un hash MD5.\n
	 * - 'storage_depth', dans le cas de Storage::ContinuousMode, la taille d'un segment (valeur possible : de 100 à 1000000). Dans le cas de Storage::SparseMode, le nombre de caractères du hashage MD5 retenu (valeur possible : de 3 à 8).  \n
	 * - 'storage_path', le chemin sur le serveur du dossier de base pour le stockage.\n
	 * - 'storage_url', l'URL du dossier de base pour le stockage.
	 * - 'storage_use_subdir', ajoute un répertoire dans la segmentation en cours portant l'id de l'élément ou non.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class Storage
	{
		/** Les fichiers sont éparpillés selon un MD5. */
		const SparseMode = 0;
		/** Les fichiers sont stockés dans des dossiers numérotés. */
		const ContinuousMode = 1;

		const ModeKey = 'storage_mode';
		const DepthKey = 'storage_depth';
		const PathKey = 'storage_path';
		const URLKey = 'storage_url';
		const UseSubdirKey = 'storage_use_subdir';
				
		private ?AbstractSettings $settings;
		private string $storagePath;
		private string $storageURL;
		private int $mode;
		private int $depth;
		private bool $useSubDir;
		private string $objectPath;
		private string $objectURL;

		/**
		 * Le constructeur.
		 *
		 * @param AbstractSettings|null $settings Une interface de settings. Si aucune n'est passée, la classe va lire automatique dans StaticSettings.
		 */
		public function __construct (AbstractSettings $settings = null)
		{
			$this->settings = $settings ?: StaticSettings::instance();

			/* Configure storage location. */
			$this->storagePath = $this->settings->get(self::PathKey, Path::getSitePath('Data/Storage/'));
			$this->storageURL = $this->settings->get(self::URLKey, Path::getSiteURL('Data/Storage/'));
			
			switch ( $this->mode = intval($this->settings->get(self::ModeKey, self::SparseMode)) )
			{
				case self::SparseMode :
					$value = $this->settings->get(self::DepthKey, 3);
					
					if ( $value < 3 )
						$this->depth = 3;
					elseif ( $value  > 8 )
						$this->depth = 8;
					else
						$this->depth = $value;
					break;
					
				case self::ContinuousMode :
					$value = $this->settings->get(self::DepthKey, 1000);
					
					if ( $value < 100 )
						$this->depth = 1;
					elseif ( $value  > 1000000 )
						$this->depth = 1000000;
					else
						$this->depth = $value;
					break;
					
				default :
					trigger_error(__METHOD__.'(), "storage_mode" wrong parameter !', E_USER_WARNING);
					break;
			}

			$this->useSubDir = $this->settings->get(self::UseSubdirKey, true);
		}

		/**
		 * Prépare le chemin d'un fichier à sauvegarder ou récupérer.
		 *
		 * @param int $id L'ID de l'élément à stocker.
		 * @param string $dirname La section (type de données) à laquelle l'élément à stocker appartient.
		 */
		public function setObject (int $id, string $dirname = '')
		{
			$folder = match ($this->mode) {
				self::SparseMode => substr(md5($id), 0, $this->depth) . DIRECTORY_SEPARATOR . ($this->useSubDir ? $id . DIRECTORY_SEPARATOR : null),
				self::ContinuousMode => (ceil($id / $this->depth) * $this->depth) . DIRECTORY_SEPARATOR . ($this->useSubDir ? $id . DIRECTORY_SEPARATOR : null),
			};
			
			$this->objectPath = $this->storagePath.( $dirname ? $dirname.DIRECTORY_SEPARATOR : null ).$folder;
			$this->objectURL = $this->storageURL.( $dirname ? $dirname.DIRECTORY_SEPARATOR : null ).$folder;
		}
		
		/**
		 * Donne le chemin du dossier sur le serveur consacré à l'élément à stocker.
		 *
		 * @param string $append Permet d'accoler un nom du fichier au dossier. Optionnel.
		 * @return string
		 */
		public function path (string $append = ''): string
		{
			return $this->objectPath.$append;
		}
		
		/**
		 * Donne L'URL du dossier consacré à l'élément à stocker.
		 *
		 * @param string $append Permet d'accoler un nom du fichier au dossier. Optionnel.
		 * @return string
		 */
		public function URL (string $append = ''): string
		{
			return $this->objectURL.$append;
		}
	}
	
