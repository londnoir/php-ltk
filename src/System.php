<?php

	/*
	* Libraries/php/LTK/System.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe contient les outils de type système.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class System
	{
		/** @internal */
		private function __construct () {}

		/**
		 * Indique si le script s'exécute en mode CLI.
		 *
		 * @return bool
		 */
		static public function isCLI (): bool
		{
			switch ( php_sapi_name() )
			{
				case 'cli' :
				case 'cgi-fcgi' :
					return true;
			}

			return false;
		}

		/**
		 * Execute une commande sur le système.
		 *
		 * @param string $command La commande à executer.
		 * @param string &$out Affiche la sortie de la commande. Optionnel
		 * @return bool Le code d'erreur du programme. (0 = true)
		 */
		static public function run (string $command, string &$out = ''): bool
		{
			$descriptorSpec = [
			   0 => ['pipe', 'r'], /* stdin */
			   1 => ['pipe', 'w'], /* stdout */
			   2 => ['pipe', 'w']  /* stderr */
			];
			
			$process = proc_open($command, $descriptorSpec, $pipes);
			
			if ( is_resource($process) )
			{
				/* stdin */
				fclose($pipes[0]);
				
				/* stdout */
				$info = stream_get_contents($pipes[1]);
				fclose($pipes[1]);
				
				/* stderr */
				$errors = stream_get_contents($pipes[2]);
				fclose($pipes[2]);
				
				$return = proc_close($process);
				
				if ( $return == 0 )
				{
					$out = $info;
					
					return true;
				}
				else
				{
					$out = $errors;
					
					return false;
				}
			}
			else
			{
				trigger_error(__METHOD__.'(), process resource creation failed.', E_USER_NOTICE);
				
				return false;
			}
		}

		/**
		* Execute une commande simple sur le système et retourne la dernière ligne. 
		* Pratique pour connaître un retour simple d'une commande système.
		*
		* @param string $command La commande à exécuter.
		* @return string|bool
		*/
		static public function runSimple (string $command): string|bool
		{
			$output = [];
			$code = 0;

			$lastLine = exec($command, $output, $code);

			return $code > 0 ? false : $lastLine;
		}
	}
