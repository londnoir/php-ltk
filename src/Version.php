<?php

	/*
	* Libraries/php/LTK/Version.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Permet de faire des tests de comparaison de numéro de version.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class Version
	{
		private int $major;
		private int $minor;
		private int $revision;
	
		/** 
		 * Le constructeur.
		 *
		 * @param int $major Un entier pour le numéro majeur de la version. Par défaut, 0.
		 * @param int $minor Un entier pour le numéro mineur de la version. Par défaut, 0.
		 * @param int $revision Un entier pour le numéro de révision. Par défaut, 0.
		 */
		public function __construct (int $major = 0, int $minor = 0, int $revision = 0)
		{
			$this->major = $major;
			$this->minor = $minor;
			$this->revision = $revision;
		}

		/** 
		 * Constructeur static utilisant une chaîne de caractères.
		 *
		 * @param string $string Un chaîne de caractères contenant le numéro de version à tester.
		 * @param string $separator Un chaîne de caractères pour définir le séparateur. Par défaut, '.'.
		 */
		static public function fromString (string $string, string $separator = '.'): object
		{
			$version = new Version();

			$tmp = explode($separator, $string);
		
			if ( count($tmp) === 3 )
			{
				$version->major = intval($tmp[0]);
				$version->minor = intval($tmp[1]);
				$version->revision = intval($tmp[2]);
			}
			else
			{
				trigger_error(__METHOD__.'(), "'.$string.'" is not a valid version value !', E_USER_ERROR);
			}

			return $version;
		}

		/** 
		 * Change le numéro de version majeur.
		 *
		 * @param int $number Numéro.
		 */
		public function setMajor (int $number)
		{
			$this->major = intval($number);
		}

		/** 
		 * Change le numéro de version mineur.
		 *
		 * @param int $number Numéro.
		 */
		public function setMinor (int $number)
		{
			$this->minor = intval($number);
		}

		/** 
		 * Change le numéro de révision.
		 *
		 * @param int $number Numéro.
		 */
		public function setRevision (int $number)
		{
			$this->revision = intval($number);
		}

		/** 
		 * Retourne le numéro de version majeur.
		 *
		 * @return int
		 */
		public function major (): int
		{
			return $this->major;
		}

		/** 
		 * Retourne le numéro de version mineur.
		 *
		 * @return int
		 */
		public function minor (): int
		{
			return $this->minor;
		}

		/** 
		 * Retourne le numéro de révision.
		 *
		 * @return int
		 */
		public function revision (): int
		{
			return $this->revision;
		}

		/** 
		 * Retourne une chaîne contenant le numéro de version
		 *
		 * @param string $separator Défini le séparateur. Par défaut, '.'.
		 * @return string
		 */
		public function version (string $separator = '.'): string
		{
			return $this->major.$separator.$this->minor.$separator.$this->revision;
		}
		
		/** 
		* Vérifie que le numéro de version est plus grand que celui passé en paramètre.
		*
		* @param Version $against Un object de type Version à comparer.
		* @return bool
		*/
		public function isGreaterThan (Version $against): bool
		{
			if ( $this->major > $against->major )
				return true;
		
			if ( $this->major == $against->major && $this->minor > $against->minor )
				return true;
			
			if ( $this->major == $against->major && $this->minor == $against->minor && $this->revision > $against->revision )
				return true;
		
			return false;
		}
		
		/** 
		 * Vérifie que le numéro de version est plus grand ou égal que celui passé en paramètre.
		 *
		 * @param Version $against Un object de type Version à comparer.
		 * @return bool
		 */
		public function isGreaterOrEqualThan (Version $against): bool
		{
			if ( $this->major > $against->major )
				return true;
		
			if ( $this->major == $against->major && $this->minor > $against->minor )
				return true;
			
			if ( $this->major == $against->major && $this->minor == $against->minor && $this->revision >= $against->revision )
				return true;
		
			return false;
		}
		
		/** 
		 * Vérifie que le numéro de version est plus petit que celui passé en paramètre.
		 *
		 * @param Version $against Un object de type Version à comparer.
		 * @return bool
		 */
		public function isLesserThan (Version $against): bool
		{
			if ( $this->major < $against->major )
				return true;
		
			if ( $this->major == $against->major && $this->minor < $against->minor )
				return true;
			
			if ( $this->major == $against->major && $this->minor == $against->minor && $this->revision < $against->revision )
				return true;
		
			return false;
		}
			
		/** 
		 * Vérifie que le numéro de version est plus petit ou égal que celui passé en paramètre.
		 *
		 * @param Version $against Un object de type Version à comparer.
		 * @return bool
		 */
		public function isLesserOrEqualThan (Version $against): bool
		{
			if ( $this->major < $against->major )
				return true;
		
			if ( $this->major == $against->major && $this->minor < $against->minor )
				return true;
			
			if ( $this->major == $against->major && $this->minor == $against->minor && $this->revision <= $against->revision )
				return true;
		
			return false;
		}
				
		/** 
		 * Vérifie que le numéro de version est équivalent à celui passé en paramètre.
		 *
		 * @param Version $against Un object de type Version à comparer.
		 * @return bool
		 */
		public function isEqualThan (Version $against): bool
		{
			return ( $this->major == $against->major && $this->minor == $against->minor && $this->revision == $against->revision );
		}
	}
	