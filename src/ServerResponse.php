<?php

	/*
	* Libraries/php/LTK/ServerResponse.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	/**
	* Permet de lire les réponses JSON standard générées par la class ServerAnswer.
	* Cet objet utilise une structure JSON standard pour communiquer entre des applications. 
	* Celui-ci s'occupe de lire les réponses.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/	
	class ServerResponse extends AbstractServerMessage
	{
		private string $rawResponse;

		/**
		 * Le constructeur.
		 *
		 * @param string $rawJSON Le flux JSON à parser.
		 */
		public function __construct (string $rawJSON = '')
		{
			if ( !empty($rawJSON) )
				$this->fromJSON($rawJSON);
		}

		/**
		 * Permet de lire un flux JSON généré par ServerAnswer.
		 *
		 * @param string $rawJSON Le flux JSON à parser.
		 * @return bool
		 */
		public function fromJSON (string $rawJSON): bool
		{	
			if ( empty($this->rawResponse) )
			{
				$this->code = self::Invalid;

				return false;
			}

			$this->rawResponse = $rawJSON;

			if ( !($json = json_decode($this->rawResponse, true)) )
			{
				$this->code = self::Invalid;

				return false;
			}

			/* Code must be there ! */
			if ( !isset($json['code']) )
			{
				$this->code = self::Invalid;

				return false;
			}

			$this->code = intval($json['code']);
			$this->message = $json['message'] ?? '';
			$this->data = $json['data'] ?? '';

			return true;
		}

		/**
		 * Retourne la réponse JSON brute.
		 *
		 * @return string
		 */
		public function rawResponse (): string
		{
			return $this->rawResponse;
		}

		/**
		 * Retourne le code de la réponse. 0 (ServerMessage::Success) signifiant qu'il n'y a pas eu de problème.
		 *
		 * @return int
		 */
		public function code (): int
		{
			return $this->code;
		}

		/**
		 * Retourne le message associé au code.
		 *
		 * @return string
		 */
		public function message (): string
		{
			return $this->message;
		}

		/**
		 * Retourne une variable personnalisée du flux JSON.
		 *
		 * @param string $key Une chaîne de caractère pour la clé de la variable personnalisée.
		 * @param mixed $defaultValue Une valeur quelconque à retourner si la variable n'est pas trouvée.
		 * @return mixed
		 */
		public function data (string $key, mixed $defaultValue = null): mixed
		{
			if ( array_key_exists($key, $this->data) )
				return $this->data[$key];

			return $defaultValue;
		}
	}
