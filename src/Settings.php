<?php

	/*
	* Libraries/php/LTK/Settings.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	* Settings class implementation using volative data.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	final class Settings extends AbstractSettings
	{
		/**
		 * Constructeur par défaut.
		 *
		 * @param array $data Un tableau associatif dont les clés seront les noms des variables, les valeurs seront les valeurs des variables. Vide par défaut.
		 */
		public function __construct (array $data = [])
		{
			if ( !empty($data) )
				$this->setArray($data);
		}

		/**
		 * Permet de construire d'instancier un objet Settings depuis un tableau de données.
		 * Simple raccourcit d'instanciation et de l'utilisateur de la méthode Settings::setArray().
		 *
		 * @param array $data Un tableau associatif dont les clés seront les noms des variables, les valeurs seront les valeurs des variables.
		 * @return Settings.
		 */
		static public function build (array $data): Settings
		{
			$settings = new Settings();
			$settings->setArray($data);

			return $settings;
		}
	}
