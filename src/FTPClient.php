<?php

	/*
	* Libraries/php/LTK/FTPClient.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

    namespace LTK;

	/**
	 * Classe de gestion de connexion FTP.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class FTPClient
	{
		private mixed $handle;
		private string $host;
		private int $port;
		private int $timeout;
		private string $username;
		private string $password;
		private bool $passiveMode;
		private bool $status = false;

		/**
		 * Le constructeur. Se connecte directement sur le serveur FTP.
		 * Pour connaître le status, il suffit d'appeler FTP::checkConnectivity().
		 *
		 * @param string $host Le host du serveur FTP.
		 * @param string $username Le nom d'utilisateur pour la connexion.
		 * @param string $password Le mot de passe.
		 * @param bool $passiveMode Indique s'il faut activer le mode passif.
		 * @param int $port Permet de stipuler un autre port que le 21.
		 * @param int $timeout Permet d'indiquer après combien de temps on laisse tomber une tentative de connexion.
		 */
		public function __construct (string $host, string $username = '', string $password = '', bool $passiveMode = false, int $port = 21, int $timeout = 90)
		{
			$this->host = $host;
			$this->port = $port;
			$this->timeout = $timeout;
			$this->username = $username;
			$this->password = $password;
			$this->passiveMode = $passiveMode;

			if ( $this->handle = ftp_connect($this->host, $this->port, $this->timeout) )
			{
				if ( empty($this->username) )
					return;

				if ( ftp_login($this->handle, $this->username, $this->password) )
				{
					$this->status = true;

					$this->passiveMode = ftp_pasv($this->handle, $this->passiveMode);
				}
			}
		}

		/**
		 * Le destructeur. Se déconnecte du serveur FTP.
		 */
		public function __destruct ()
		{
			ftp_close($this->handle);
		}

		/**
		 * Retourne la main sur la resource FTP soit le résultat de la commande ftp_connect().
		 *
		 * @return mixed
		 */
		public function handle (): mixed
		{
			return $this->handle;
		}

		/**
		 * Permet de savoir si la connexion au serveur FTP est établie. 
		 *
		 * @return bool
		 */
		public function checkConnectivity (): bool
		{
			return is_resource($this->handle) && $this->status;
		}

		/**
		 * Permet de récupèrer une liste de fichier dans le dossier indiqué.
		 *
		 * @note Ne fonctionne pas sur tout les serveurs.
		 * @param string $remoteDirectory Le chemin sur le serveur.
		 * @param array &$output Une référence sur un tableau qui sera alimenté par chaque fichier trouvé.
		 * @param bool $details Utilise la fonction ftp_mlsd() plutot que ftp_nlist() pour obtenir plus de détails sur les fichiers.
		 * @return bool
		 */
		public function listFiles (string $remoteDirectory, array &$output, bool $details = false): bool
		{
			$files = $details ? ftp_mlsd($this->handle, $remoteDirectory) : ftp_nlist($this->handle, $remoteDirectory);

			if ( $files === false )
			{
				trigger_error(__METHOD__.'(), unable to list files of remote directory "'.$remoteDirectory.'" !', E_USER_WARNING);

				return false;
			}

			$output = $files;

			return true;
		}

		/**
		 * Permet de récupèrer les entrées (fichiers et répertoires) depuis un certain niveau. Indiquer '.' pour la racine.
		 *
		 * @param string $remoteDirectory Le chemin sur le serveur.
		 * @param array &$output Une référence sur un tableau qui sera alimenté par chaque entré trouvée.
		 * @return bool
		 */
		public function listEntries (string $remoteDirectory, array &$output): bool
		{
			$entries = ftp_rawlist($this->handle, $remoteDirectory);

			if ( $entries === false )
			{
				trigger_error(__METHOD__.'(), unable to read remote directory "'.$remoteDirectory.'" !', E_USER_WARNING);

				return false;
			}

			$output = $entries;

			return true;
		}

		/**
		 * Recupère un fichier distant vers un fichier local. Cette méthode s'occupe de vérifier les droits et la création du fichier en local.
		 *
		 * @param string $remoteFilepath Le chemin vers le fichier sur le serveur.
		 * @param string $localFilepath Le chemin local ou copier le fichier.
		 * @param bool $overwrite Si le fichier local existe déjà, permet d'indiquer si on peut l'écraser ou non.
		 * @return bool
		 */
		public function copyToFile (string $remoteFilepath, string $localFilepath, bool $overwrite = false): bool
		{
			$dirname = dirname($localFilepath);

			if ( !Path::build($dirname) )
			{
				trigger_error(__METHOD__.'(), unable to create directory "'.$dirname.'".', E_USER_WARNING);

				return false;
			}

			/* NOTE: We want to check the overwrite rule in case of file presence. */
			if ( file_exists($localFilepath) )
			{
				if ( $overwrite && !is_writable($localFilepath) )
				{
					trigger_error(__METHOD__.'(), file "'.$localFilepath.'" already exists'.( $overwrite ? ' and not overwritable' : '' ).' ! Copy cancelled.', E_USER_WARNING);

					return false;
				}
			}

			return ftp_get($this->handle, $localFilepath, $remoteFilepath, FTP_BINARY, 0);
		}

		/**
		 * Recupère un fichier distant et écrit le contenu dans une resource de type fichier.
		 *
		 * @param string $remoteFilepath Le chemin vers le fichier sur le serveur.
		 * @param mixed $fileHandle La resource d'un fichier donné par fopen().
		 * @return bool
		 */
		public function copyToResource (string $remoteFilepath, mixed $fileHandle): bool
		{
			return ftp_fget($this->handle, $fileHandle, $remoteFilepath, FTP_BINARY, 0);
		}

		/**
		 * Retourne le contenu text du fichier distant directement dans une variable.
		 *
		 * @param string $remoteFilepath Le chemin vers le fichier sur le serveur.
		 * @return string
		 */
		public function getFileContent (string $remoteFilepath): string
		{
			$buffer = '';

			if ( $tempResource = $this->temporaryFile() )
			{
				if ( ftp_fget($this->handle, $tempResource, $remoteFilepath, FTP_BINARY, 0) )
				{
					rewind($tempResource);

					while ( !feof($tempResource) )
						$buffer .= fread($tempResource, 8192);
				}
				else
				{
					trigger_error(__METHOD__.'(), unable to read remote file "'.$remoteFilepath.'" !', E_USER_WARNING);
				}

				fclose($tempResource);
			}

			return $buffer;
		}

		/**
		 * Envoi un fichier local vers un fichier distant.
		 *
		 * @param string $localFilepath Le chemin vers le fichier local.
		 * @param string $remoteFilepath Le chemin vers le fichier sur le serveur.
		 * @return bool
		 */

		public function put (string $localFilepath, string $remoteFilepath): bool
		{
			return ftp_put($this->handle, $remoteFilepath, $localFilepath, FTP_BINARY, 0);
		}

		/**
		 * Envoi le contenu d'une variable dans un fichier distant.
		 * Cette méthode ne crée aucun fichier sur le disque.
		 *
		 * @param string $buffer Le contenu.
		 * @param string $remoteFilepath Le chemin vers le fichier sur le serveur.
		 * @return bool
		 */
		public function putContent (string $buffer, string $remoteFilepath): bool
		{
			if ( !($tempResource = $this->temporaryFile()) )
				return false;

			if ( fwrite($tempResource, $buffer) === false )
			{
				trigger_error(__METHOD__.'(), unable to write in temporary file !', E_USER_WARNING);

				return false;
			}

			rewind($tempResource);

			$result = ftp_fput($this->handle, $remoteFilepath, $tempResource, FTP_BINARY, 0);

			fclose($tempResource);

			return $result;
		}

		/**
		 * Crée un tampon en mémoire pour manipuler un fichier temporaire.
		 *
		 * @return mixed
		 */
		private function temporaryFile (): mixed
		{
			$resource = fopen('php://memory', 'w+');

			if ( !$resource )
			{
				trigger_error(__METHOD__.'(), unable to create a temporary file resource in memory !', E_USER_WARNING);

				return false;
			}

			return $resource;
		}
	}

