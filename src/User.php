<?php

	/*
	* Libraries/php/LTK/User.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	use PDO;

	/**
	 * Permet de gérer un compte utilisateur.
	 * Cet objet permet de créer des comptes utilisateurs avec des droits.
	 * Il permet de gérer les connexions des utilisateurs, la modification des comptes et la sécurité de l'application.
	 * Un profil standard est associé avec le compte, un système permet de rajouter des champs personnalisé à l'objet User.
	 *
 	 * @author LondNoir <londnoir@gmail.com>
	 */
	class User
	{
		/** La taille minimum du mot de passe doit être de 4 caractères. */
		const PasswordStrengthLow = 4;
		/** La taille minimum du mot de passe doit être de 8 caractères. */
		const PasswordStrengthMedium = 8;
		/** La taille minimum du mot de passe doit être de 16 caractères. */
		const PasswordStrengthHigh = 16;
		
		/** Une connexion ou une déconnexion a réussi. */
		const Success = 0;
		/** Une connexion à échoué. En règle général, il s'agit d'un problème au niveau de la base de données. */
		const Failed = 1;
		/** Pas de connexion à la base de données, aucune connexion d'utilisateur ne peut avoir lieu. */
		const NoDatabase = 2;
		/** Le nom du compte est invalide et ne peut pas être utilisé pour créer un utilisateur ou se connecter. */
		const InvalidUsername = 3;
		/** Le mot de passe est invalide et ne peut pas être utilisé pour créer un utilisateur ou se connecter. */
		const InvalidPassword = 4;
		/** L'adresse email est invalide et ne peut pas être utilisé pour créer un utilisateur ou se connecter. */
		const InvalidEmail = 5;
		/** Le compte n'existe pas dans la base de données. */
		const UnknowAccount = 6;
		/** Le mot passe ne correspond pas à celui du compte trouvé. */
		const BadPassword = 7;
		/** Le compte est suspendu par l'administration, la connexion est donc annulée. */
		const AccountSuspended = 8;
		/** Le compte est déjà déconnecté. Aucun compte en cours. */
		const AlreadyDisconnected = 9;
		/** Le nom de compte est déjà utilisé. */
		const UsernameAlreadyUsed = 10;
		/** L'adresse email est déjà utilisée. */
		const EmailAlreadyUsed = 11;
		/** Le mot de passe est invalide pour le compte. */
		const InvalidNewPassword = 12;
		/** Le mot de passe de confirmation ne correspond pas. */
		const MalformedNewPassword = 13;
		/** Le compte n'est pas validé. */
		const AccountNotValidated = 14;

		const TableNameKey = 'user_table_name';
		const DefaultTableName = 'ltk_users';
		/** Permet ou non de multiple connexions sur le compte. Désactivé par défaut. */
		const EnableMultipleConnexionKey = 'user_enable_multiple_connexion';
		/** Charge automatiquement le profil ou non. Désactivé par défaut. */
		const EnableProfileKey = 'user_enable_profile';
		/** Un tableau associatif permettant de rajouter des champs au profil.\n
		 * Exemple : \n
		 * array(\n
		 *  'var_a' => array(User::Int, 0),\n
		 *  'var_b' => array(User::String, "default value")\n
		 * )\n. */
		const ExtraFieldsKey = 'user_extra_fields';
		/** Définit le niveau de sécurité du mot de passe. User::PasswordStrengthMedium par défaut. */
		const PasswordSecurityKey = 'user_password_security';
		/** Spécifie l'URL de connection de l'utilisateur. */
		const LoginURLKey = 'user_login_url';
		/** Active le mode debug de l'objet. */
		const DebugKey = 'user_debug';
		/** Le nombre minimum de caractères pour le nom du compte. Par défaut 4. */
		const NameMinCharKey = 'user_name_minchar';
		/** Le nombre maximum de caractères pour le nom du compte. Par défaut 16. */
		const NameMaxCharKey = 'user_name_maxchar';
		/** Le temps en secondes minimum de la durée de vie d'une clé sur le serveur. Par défaut 1 journée. */
		const AuthkeyMinLifeTimeKey = 'user_authkey_minlifetime';
		/** Permet d'étendre X secondes la durée de vie de la clé d'authentification toutes les X secondes, si l'utilisateur utilise sa session. Activé par défaut.\n
		 *  X est égal à la configuration 'user_update_granularity'. */
		const UpdateAuthkeyLifeTimeKey = 'user_update_authkey_lifetime';
		/** Permet de définir en secondes l'interval entre les mises à jour de connexion. Par défaut 1 Heure. */
		const UpdateGranularityKey = 'user_update_granularity';
		/** Permet de valider automatiquement un compte à la création ou non. Par défaut true. */
		const AutoValidateAccountKey = 'user_auto_validate_account';

		/** Un champ de type chaîne de caractères. */
		const String = 0;
		/** Un champ de type nombre entier. */
		const Int = 1;
		/** Un champ de type booléen. */
		const Bool = 2;
		/** Un champ de type nombre à virgule flottante. */
		const Float = 3;
		/** Un champ de type nombre à virgule flottante (double). */
		const Double = 4;

		/** Constante du niveau correspondant au super administrateur (255). */
		const SuperAdmininistrator = 255;
		/** Constante du niveau correspondant au administrateur (254). */
		const Administrator = 254;
		/** Constante du niveau correspondant au modérateur (127). */
		const Moderator = 127;
		/** Constante du niveau correspondant au membre simple (1). */
		const Member = 1;
		/** Constante du niveau correspondant au visiteur (0). */
		const Visitor = 0;
		
		/* Required objects. */
		private AbstractSettings $settings;
		private Database $database;

		/* Authentication informations */
		private string $username = 'visiteur';
		private ?string $authkey = null;
		private int $authkeyLife = 0;
		
		/* Account information. */
		private int $id = 0;
		private int $creationDate = 0;
		private int $creationIP = 0;
		private string $creationUserAgent = '';
		private int $modificationDate = 0;
		private int $lastConnexion = 0;
		private int $lastIP = 0;
		private string $lastUserAgent = '';
		private int $loginCount = 0;
		private int $logoutCount = 0;
		private string $email = '';
		private int $level = 0;
		private bool $isSuspended = false;
		/* Profile data array. This will be only loaded if one profile field is requested. */
		private array $profileData = [
			'p_username' => '',
			'p_firstname' => '',
			'p_lastname' => '',
			'p_sexe' => 2,
			'p_birthdate' => '0000-00-00',
			'p_address' => '',
			'p_address_number' => '',
			'p_box_number' => '',
			'p_city' => '',
			'p_postal_code' => '',
			'p_phone_number' => '',
			'p_mobile_phone_number' => '',
			'p_fax_number' => '',
			'p_country' => '',
			'p_lang' => ''
		];
		/* Allow extra fields to be grabbed from db (Optionnal).
		 * Note: This store the extra fields in db. */
		private array $extraFields = [];

		/**
		 * Constructs an user.
		 *
		 * @param AbstractSettings $settings Un object de type settings.
		 * @param Database $database Un connexion à la base de données.
		 * @param bool $searchUser Un booléen qui permet de rechercher ou non un utilisateur sur la machine, grâce au cookie.
		 */
		public function __construct (AbstractSettings $settings, Database $database, bool $searchUser = false)
		{
			$this->settings = $settings ?: StaticSettings::instance();
			$this->database = $database;

			/* Check database connexion. */
			if ( !$this->checkObject() )
				return;
			
			$value = $this->settings->get(self::ExtraFieldsKey);
			
			if ( $value && is_array($value) )
			{
				foreach ( $value as $field => $fieldsInfos )
				{
					if ( !is_array($fieldsInfos) )
					{
						$this->extraFields[$field] = array(self::String, null);
						
						continue;
					}
					
					switch ( $type = intval($fieldsInfos[0]) )
					{
						case self::Int :
						case self::Bool :
							$default = isset($fieldsInfos[1]) ? intval($fieldsInfos[1]) : 0;
							break;
							
						case self::Float :
							$default = isset($fieldsInfos[1]) ? floatval($fieldsInfos[1]) : 0.0;
							break;
							
						case self::Double :
							$default = isset($fieldsInfos[1]) ? doubleval($fieldsInfos[1]) : 0.0;
							break;
							
						case self::String :
						default :
							$type = self::String;
							$default = $fieldsInfos[1] ?? null;
							break;
					}
					
					$this->extraFields[$field] = [$type, $default];

					/* Initialize extra data. */
					$this->profileData[$field] = $default;
				}
			}
						
			/* If requested, we load the current user by searching the authentication key. */
			if ( $searchUser && $authkey = $this->getClientAuthkey() )
			{
				$this->authkey = $authkey;
				
				if ( $userData = $this->database->getRow('SELECT '.$this->selectFields().' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `authkey` = \''.$this->authkey.'\' LIMIT 1;') )
				{
					$this->setUserData($userData);
					
					/* Check server 'authkey' life. */
					if ( $this->authkeyLife < time() )
					{
						$this->destroyClientAuthkey();
						$this->destroyServerAuthkey();
					}
					else
					{
						$this->updateConnexion();
					}
				}
				else
				{
					if ( $this->settings->get(self::DebugKey) )
						trigger_error(__METHOD__.'(), the authkey '.$this->authkey.' is not in database !');
					
					/* The authentication key is not found in the database.
					 * Note : the 'authkey' var will be erased ! */
					$this->destroyClientAuthkey();
				}
			}
		}

		/**
		 * Checks if the database is usable.
		 *
		 * @return bool
		 */
		private function checkObject (): bool
		{
			if ( is_null($this->database) )
				return false;
			
			if ( $this->database->isConnected() === false )
				return false;
			
			return true;
		}
		
		/**
		 * Gets an user with the ID.
		 *
		 * @param AbstractSettings $settings A reference to a settings object.
		 * @param Database $database A reference to a valid database object.
		 * @param int $userId The user ID.
		 * @return User|null
		 */
		public static function getById (AbstractSettings $settings, Database $database, int $userId): ?User
		{
			$user = new User($settings, $database, false);
						
			$userData = $database->getRow('SELECT '.$user->selectFields().' FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `id` = '.intval($userId).' LIMIT 1;');
			
			if ( $userData && $user->setUserData($userData) )
				return $user;
			else
				return null;
		}
		
		/**
		 * Gets an user with the authentication key.
		 *
		 * @param AbstractSettings $settings A reference to a settings object.
		 * @param Database $database A reference to a valid database object.
		 * @param string $authkey A valid authentication key.
		 * @return User|null
		 */
		public static function getByAuthkey (AbstractSettings $settings, Database $database, string $authkey): ?User
		{
			$user = new User($settings, $database, false);

			if ( $user->checkAuthkeySyntax($authkey) === false )
				return null;
			
			$userData = $database->getRow('SELECT '.$user->selectFields().' FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `authkey` = \''.$authkey.'\' LIMIT 1;');
			
			if ( $userData && $user->setUserData($userData) )
				return $user;
			else
				return null;
		}

		/**
		 * Gets an user with the account name.
		 *
		 * @param AbstractSettings $settings A reference to a settings object.
		 * @param Database $database A reference to a valid database object.
		 * @param string $username A valid user name.
		 * @return User|null
		 */
		public static function getByName (AbstractSettings $settings, Database $database, string $username): ?User
		{
			$user = new User($settings, $database, false);

			if ( $user->checkUsernameSyntax($username) === false )
				return null;
			
			$userData = $database->getRow('SELECT '.$user->selectFields().' FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `username` = \''.$username.'\' LIMIT 1;');
			
			if ( $userData && $user->setUserData($userData) )
				return $user;
			else
				return null;
		}

		/**
		 * Gets an user with the email address.
		 *
		 * @param AbstractSettings $settings A reference to a settings object.
		 * @param Database $database A reference to a valid database object.
		 * @param string $email A valid email address.
		 * @return User|null
		 */
		public static function getByEmail (AbstractSettings $settings, Database $database, string $email): ?User
		{
			if ( !EmailAddress::checkSyntax($email) )
				return null;

			$user = new User($settings, $database, false);
			
			$userData = $database->getRow('SELECT '.$user->selectFields().' FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `email` = \''.$email.'\' LIMIT 1;');
			
			if ( $userData && $user->setUserData($userData) )
				return $user;
			else
				return null;
		}
		
		/**
		 * Update a user connexion lifetime.
		 * 
		 * @return bool
		 */
		private function updateConnexion (): bool
		{
			/* NOTE: Update last connexion time if useful. */
			if ( $this->lastConnexion > (time() - $this->settings->get(self::UpdateGranularityKey, 3600)) )
				return true;

			/* Set the new timestamp for the User object. */
			$this->lastConnexion = time();

			/* Save it to the database. */
			$query =
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
				'`last_connexion` = '.$this->lastConnexion.', '.
				'`last_ip` = '.self::getCurrentIP().', '.
				'`last_useragent` = \''.addslashes($_SERVER['HTTP_USER_AGENT']).'\' ';

			if ( $this->settings->get(self::UpdateAuthkeyLifeTimeKey, true) )
			{
				$time = $this->authkeyLife + $this->settings->get(self::UpdateGranularityKey, 3600);

				/* Update cookie. */
				$this->saveClientAuthkey($time);

				/* Update in database. */
				$query .= ', `authkey_life` = '.$time.' ';
			}

			$query .=
				'WHERE `id` = '.$this->id.' '.
				'LIMIT 1;';

			return $this->database->execute($query) === false;
		}
		
		/**
		 * Updates user data.
		 *
		 * @param array $rawUserData An array of user data.
		 * @return bool
		 */
		private function setUserData (array $rawUserData): bool
		{
			if ( !$rawUserData )
			{
				trigger_error(__METHOD__.'(), empty user data !', E_USER_WARNING);
				
				return false;
			}
			
			$this->id = intval($rawUserData['id']);
			$this->creationDate = intval($rawUserData['c_date']);
			$this->creationIP = intval($rawUserData['c_ip']);
			$this->creationUserAgent = $rawUserData['c_useragent'];
			$this->modicationDate = intval($rawUserData['m_date']);
			$this->lastConnexion = intval($rawUserData['last_connexion']);
			$this->lastIP = intval($rawUserData['last_ip']);
			$this->lastUserAgent = $rawUserData['last_useragent'];
			$this->loginCount = intval($rawUserData['login_count']);
			$this->logoutCount = intval($rawUserData['logout_count']);
			$this->username = $rawUserData['username'];
			$this->authkeyLife = intval($rawUserData['authkey_life']);
			$this->email = $rawUserData['email'];
			$this->level = intval($rawUserData['level']);
			$this->isSuspended = (bool)$rawUserData['suspended'];
			
			/* Check profile data if requested. */
			if ( $this->settings->get(self::EnableProfileKey) )
			{
				$this->profileData['p_username'] = $rawUserData['p_username'];
				$this->profileData['p_firstname'] = $rawUserData['p_firstname'];
				$this->profileData['p_lastname'] = $rawUserData['p_lastname'];
				$this->profileData['p_sexe'] = intval($rawUserData['p_sexe']);
				$this->profileData['p_birthdate'] = $rawUserData['p_birthdate'];
				$this->profileData['p_address'] = $rawUserData['p_address'];
				$this->profileData['p_address_number'] = $rawUserData['p_address_number'];
				$this->profileData['p_box_number'] = $rawUserData['p_box_number'];
				$this->profileData['p_city'] = $rawUserData['p_city'];
				$this->profileData['p_postal_code'] = $rawUserData['p_postal_code'];
				$this->profileData['p_phone_number'] = $rawUserData['p_phone_number'];
				$this->profileData['p_mobile_phone_number'] = $rawUserData['p_mobile_phone_number'];
				$this->profileData['p_fax_number'] = $rawUserData['p_fax_number'];
				$this->profileData['p_country'] = $rawUserData['p_country'];
				$this->profileData['p_lang'] = $rawUserData['p_lang'];
			
				if ( $this->extraFields )
				{
					foreach ( $this->extraFields as $field => $fieldInfos )
					{
						switch ( $fieldInfos[0] )
						{
							case User::Int :
								$this->profileData[$field] = intval($rawUserData[$field]);
								break;
							
							case User::Bool :
								$this->profileData[$field] = (bool)$rawUserData[$field];
								break;
							
							case User::Float :
								$this->profileData[$field] = floatval($rawUserData[$field]);
								break;
							
							case User::Double :
								$this->profileData[$field] = doubleval($rawUserData[$field]);
								break;
							
							case User::String :
							default :
								$this->profileData[$field] = $rawUserData[$field];
								break;
						}
					}
				}
			}
			
			return true;
		}
		
		/**
		 * Generates an authentication key.
		 *
		 * @param string $string A string to include some custom data in the final key.
		 * @return string
		 */
		public static function generateAuthkey (string $string): string
		{
			$newKey = null;
			
			/* Only lower case. */
			$string = strtolower($string);
			
			/* First part of the authentication key.
			 * NOTE: Username too short, we complete it with random chars. */
			if ( strlen($string) < 32 )
			{
				$rndPattern = md5( $string . rand(0, time()) );
				$charNeeded = 32 - strlen($string);
			
				$newKey .= substr($rndPattern, 0, $charNeeded).$string;
			}
			/* Username too long, we cut it. */
			elseif ( strlen($string) > 32 )
			{
				$newKey .= substr($string, 0, 32);
			}
			/* Same length, just a copy. */
			else 
			{
				$newKey .= $string;
			}
		
			/* Second part of the authentication key. */
			$rndPattern = md5( $string . rand(0, time()) );
			$newKey .= substr($rndPattern, 0, 32);
			
			if ( strlen($newKey) == 64 )
				return $newKey;
			else
				return '';
		}

		/**
		 * Saves the authentication key on the server side.
		 *
		 * @param int $time
		 * @return bool
		 */
		private function saveServerAuthkey (int $time): bool
		{
			if ( $this->checkObject() )
			{
				if ( $time <= 0 )
					$time = time () + $this->settings->get(self::AuthkeyMinLifeTimeKey, 86400);
				
				$query = 
					'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
					'`authkey` = \''.$this->authkey.'\', '.
					'`authkey_life` = '.$time.', '.
					'`last_connexion` = UNIX_TIMESTAMP(), '.
					'`last_ip` = '.self::getCurrentIP().', '.
					'`last_useragent` = \''.addslashes($_SERVER['HTTP_USER_AGENT']).'\', '.
					'`login_count` = `login_count` + 1 '.
					'WHERE `id` = '.$this->id.' LIMIT 1 ;';
				
				if ( $this->database->execute($query) !== false )
					return true;
			}
			
			return false;
		}

		/**
		 * Destroys the authentication key on the server side.
		 *
		 * @return bool
		 */
		private function destroyServerAuthkey (): bool
		{
			if ( $this->checkObject() )
			{
				$query = 
					'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` '.
					'SET `logout_count` = `logout_count` + 1, `authkey` = NULL, `authkey_life` = 0 '.
					'WHERE `id` = '.$this->id.' LIMIT 1;';
				
				if ( $this->database->execute($query) !== false )
					return true;
			}
			
			return false;
		}
		
		/**
		 * Saves the authentication key on the client site (cookie).
		 *
		 * @param int $time Set a validity delay for the key.
		 * @return bool
		 */
		private function saveClientAuthkey (int $time): bool
		{
			return setcookie('authkey', $this->authkey, intval($time), '/');
		}
		
		/**
		 * Destroys the authentication key on the client site (cookie).
		 *
		 * @return bool
		 */
		private function destroyClientAuthkey (): bool
		{
			$this->authkey = null;
			
			return setcookie('authkey', null, 0, '/');
		}
		
		/**
		 * Get the autentication key from the client side.
		 *
		 * @return string
		 */
		private function getClientAuthkey (): string
		{
			$tmpKey = null;
			$fromCookie = false;
			
			if ( isset($_COOKIE['authkey']) && !empty($_COOKIE['authkey']) )
			{
				$tmpKey = $_COOKIE['authkey'];
				$fromCookie = true;
			}
			elseif ( isset($_POST['authkey']) && !empty($_POST['authkey']) )
			{
				$tmpKey = $_COOKIE['authkey'];
			}
			elseif ( isset($_GET['authkey']) && !empty($_GET['authkey']) )
			{
				$tmpKey = $_COOKIE['authkey'];
			}
			
			if ( $tmpKey )
			{
				/* Check key validity. */
				if ( $this->checkAuthkeySyntax($tmpKey) )
					return $tmpKey;

				/* If key is not valid and comme from cookie, we kill the cookie. */
				if ( $fromCookie )
					$this->destroyClientAuthkey();
			}
			
			return '';
		}

		/**
		 * Checks authentication key syntax validity.
		 *
		 * @param string $authkey The account name.
		 * @return bool
		 */
		public static function checkAuthkeySyntax (string $authkey): bool
		{
			if ( empty($authkey) )
				return false;

			return preg_match('#^([a-z0-9-_]{64,64})$#', $authkey);
		}
		
		/**
		 * Checks account name syntax validity.
		 *
		 * @param string $username The account name.
		 * @return bool
		 */
		public function checkUsernameSyntax (string $username): bool
		{
			if ( empty($username) )
				return false;

			$minChar = $this->settings->get(self::NameMinCharKey, 4);
			$maxChar = $this->settings->get(self::NameMaxCharKey, 16);
			
			return preg_match('#^([a-z0-9-_]{'.$minChar.','.$maxChar.'})$#', $username);
		}
		
		/**
		* Vérifie la syntaxe d'un mot de passe.
		* @param string $password Une chaîne de caractères contenant le mot de passe à vérifier.
		* @return bool Un booléen ou vrai signifie valide.
		*/
		public function checkPasswordSyntax (string $password): bool
		{
			if ( empty($password) )
				return false;

			switch ( $securityLevel = $this->settings->get(self::PasswordSecurityKey, self::PasswordStrengthMedium) )
			{
				case self::PasswordStrengthLow :
				case self::PasswordStrengthMedium :
				case self::PasswordStrengthHigh :
					break;
			
				default :
					trigger_error(__METHOD__.'(), "user_password_security" wrong parameter !', E_USER_WARNING);

					$securityLevel = self::PasswordStrengthMedium;

					break;
			}

			return preg_match('#^([a-zA-Z0-9-_]{'.$securityLevel.',32})$#', $password);
		}
		
		/**
		 * Checks the current user level to execute the next part of the current script.
		 *
		 * @param int $requiredLevel An int from 0 to 255 to set the minimum required level.
		 * @param string $redirectOnFail The URL where the user will be redirected if he is not authorized. Default LoginURL.
		 * @param string $from An extra variable to add where the user were before being redirected. Default current location.
		 */
		public function checkpoint (int $requiredLevel, string $redirectOnFail = '', string $from = '')
		{
			/* Only if the user do not have the required level. */
			if ( $this->level < $requiredLevel )
			{
				if ( $redirectOnFail )
					$link = $redirectOnFail;
				else
					$link = $this->settings->get(self::LoginURLKey, 'http://'.$_SERVER['HTTP_HOST'].'/');
				
				/* If connected, go to the $goto URL or home page. */
				if ( !$this->isConnected() )
				{
					$params = array(
						'from' => base64_encode($from ? $from : Path::getAbsoluteURL())
					);
					
					$link = Path::buildURL($link, $params, false);
				}
				
				header('Location: '.$link);
				echo '<script type="text/javascript">document.location.href = "'.$link.'";</script>'."\n";
				exit(0);
			}
		}
		
		/**
		 * Creates a new user.
		 *
		 * @param AbstractSettings $settings A reference to a settings object.
		 * @param Database $database A reference to a valid database object.
		 * @param string $username The user account name.
		 * @param string $email The user mail address.
		 * @param string $password The user clear password.
		 * @param array & $errors A reference to an array where possible errors will be set as: \n
		 * User::UsernameAlreadyUsed\n
		 * User::InvalidUsername\n
		 * User::EmailAlreadyUsed\n
		 * User::InvalidEmail\n
		 * User::InvalidPassword
		 * @return User|null
	  	 */
		public static function createNewUser (AbstractSettings $settings, Database $database, string $username, string $email, string $password, array &$errors): ?User
		{
			if ( !$database->isConnected() )
			{
				trigger_error(__METHOD__.'(), database is not connected !', E_USER_WARNING);
				
				return null;
			}

			$user = new User($settings, $database, false);
			$errors = [];
			
			/* Check the username for syntax and availability. */
			if ( $user->checkUsernameSyntax($username) )
			{
				$statement =
					'SELECT COUNT(1) '.
					'FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` '.
					'WHERE `username` = "'.$username.'";';

				if ( $database->getResult($statement, 'int') )
					$errors[] = self::UsernameAlreadyUsed;
			}
			else
			{
				$errors[] = self::InvalidUsername;
			}
			
			/* Check the email address for syntax and availability. */
			if ( EmailAddress::checkSyntax($email) )
			{
				$statement =
					'SELECT COUNT(1) '.
					'FROM `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` '.
					'WHERE `email` = \''.$email.'\';';

				if ( $database->getResult($statement, 'int') )
					$errors[] = self::EmailAlreadyUsed;
			}
			else
			{
				$errors[] = self::InvalidEmail;
			}
			
			/* Check the password for syntax. */
			if ( !$user->checkPasswordSyntax($password) )
				$errors[] = self::InvalidPassword;

			$values = [
				'c_date' => time(),
				'c_ip' => self::getCurrentIP(),
				'c_useragent' => $_SERVER['HTTP_USER_AGENT'],
				'last_ip' => self::getCurrentIP(),
				'last_useragent' => $_SERVER['HTTP_USER_AGENT'],
				'username' => $username,
				'password' => md5($password),
				'authkey' => self::generateAuthkey($username), // FIXME : there is no authkey !
				'level' => $settings->get(self::AutoValidateAccountKey, true) ? 1 : 0,
				'email' => strtolower($email),
				'p_username' => $username
			];
				
			$statement = Database::writeInsertQuery($settings->get(self::TableNameKey, self::DefaultTableName), $values);

			if ( $database->execute($statement) === false )
				$errors[] = self::Failed;

			if ( !empty($errors) )
				return null;

			$user->id = intval($database->lastInsertId());
			$user->creationDate = intval($values['c_date']);
			$user->username = $values['username'];
			$user->email = $values['email'];
			$user->level = $values['level'];
			$user->isSuspended = false;

			return $user;
		}
		
		/**
		 * Simplifies a string to be used as an account name.
		 *
		 * @param string $string The string to modify.
		 * @return string
		 */
		public function sanitizeUsername (string $string): string
		{
			$string = str_replace([
				'²','³','°','à','â','ä','å','æ','Â','Ä','À','Á','Ã','Å','Æ','ß','ç','Ç','Ð',
				'é','è','ê','ë','Ê','Ë','È','É','€','î','ï','Î','Ï','Ì','Í','ì','í','£','ñ',
				'Ñ','ð','õ','ô','ö','ø','Ô','Ö','Ø','Õ','Ò','$','§','û','ù','ú','ü','µ','Û',
				'Ü','Ù','Ú','¥','Ý','ý','ÿ','/','|','&',' ',"'",'"','`'
			], [
				'2','3','0','a','a','a','a','a','a','a','a','a','a','a','a','b','c','c','d',
				'e','e','e','e','e','e','e','e','e','i','i','i','i','i','i','i','i','l','n',
				'n','o','o','o','o','o','o','o','o','o','o','s','s','u','u','u','u','u','u',
				'u','u','u','y','y','y','y','-','-','-','-','-','-','-'
			], $string);
			$string = trim($string, " \t\n\r\0\x0B-");
			$string = preg_replace('/[^a-zA-Z0-9-_]/', '', $string);
			$string = strtolower($string);
			
			if ( $this->checkUsernameSyntax($string) )
				return $string;
			
			return '';
		}

		/**
		 * @param string $authkey
		 * @param int $dayLife
		 * @return bool
		 */
		private function activateAuthkey (string $authkey, int $dayLife): bool
		{
			/* Set the connexion life */
			$time = ( $dayLife > 0 ) ? time() + ($dayLife  * 86400) : 0;

			/* Create or reuse the authentication key. */
			if ( $this->settings->get(self::EnableMultipleConnexionKey) )
			{
				if ( $authkey == 'disconnected' || !self::checkAuthkeySyntax($authkey) )
					$this->authkey = self::generateAuthkey($this->username);
				else
					$this->authkey = $authkey; 
			}
			else
			{
				$this->authkey = self::generateAuthkey($this->username);
			}
			
			/* Saving the key on the server (database). */
			if ( $this->saveServerAuthkey($time) === false )
			{
				trigger_error(__METHOD__.'(), the authentication key is not saved in database !', E_USER_WARNING);
				
				return false;
			}
			
			/* Saving key on the client side (cookie). */
			if ( $this->saveClientAuthkey($time) === false )
			{
				trigger_error(__METHOD__.'(), the authentication key is not saved in cookie !', E_USER_WARNING);
				
				return false;
			}
			
			return true;
		}
		
		/**
		 * Performs an user login.
		 *
		 * @param string $username The account name.
		 * @param string $password The clear password to check the connexion.
		 * @param int $dayLife The number of day where the connexion still alive. Default 0, at the end of the browser execution.
		 * @return int
		 */
		public function login (string $username, string $password, int $dayLife = 0): int
		{
			if ( !$this->checkObject() )
				return self::NoDatabase;
			
			if ( !$this->checkPasswordSyntax($password) )
				return self::InvalidPassword;
			
			/* Using email as login */
			if ( strstr($username, '@') )
			{
				if ( !EmailAddress::checkSyntax($username) )
					return self::InvalidUsername;
				
				$query = 'SELECT '.$this->selectFields().' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `email` = \''.strtolower($username).'\' LIMIT 1;';
			}
			else
			{
				/* Prevent from weird chars typed at registration time. Example: "Cr@zy" -> "crazy" */
				$username = $this->sanitizeUsername($username);
				
				if ( empty($username) )
					return self::InvalidUsername;
				
				$query = 'SELECT '.$this->selectFields().' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `username` = \''.$username.'\' LIMIT 1;';
			}
			
			/* Search the account. */
			if ( !($userData = $this->database->getRow($query)) )
				return self::UnknowAccount;
			
			/* Check the given password with this account. */
			if ( md5($password) !== $userData['password'] )
				return self::BadPassword;
				
			/* Check account validity. */
			if ( $userData['level'] === 0 )
				return self::AccountNotValidated;
			
			/* Check account suspension. */
			if ( $userData['suspended'] )
				return self::AccountSuspended;
			
			/* Load the account into memory. */
			$this->setUserData($userData);

			/* Activate the key. */
			return $this->activateAuthkey($userData['authkey'] ?: '', $dayLife) ? self::Success : self::Failed;
		}
		
		/**
		 * Performs an user auto-login with his ID.
		 *
		 * @param int $userID The user ID.
		 * @param int $dayLife The number of day where the connexion still alive. Default 0, at the end of the browser execution.
		 * @return int
		 */
		public function autoLogin (int $userID, int $dayLife = 0): int
		{
			if ( !$this->checkObject() )
				return self::NoDatabase;
			
			if ( $userID <= 0 )
				return self::UnknowAccount;
			
			$query = 'SELECT '.$this->selectFields().' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `id` = '.intval($userID).' LIMIT 1;';
			
			/* Search the account. */
			if ( !($userData = $this->database->getRow($query)) )
				return self::UnknowAccount;
			
			/* Check account validity. */
			if ( $userData['level'] === 0 )
				return self::AccountNotValidated;
			
			/* Check account suspension. */
			if ( $userData['suspended'] )
				return self::AccountSuspended;
			
			/* Loads the account into memory. */
			$this->setUserData($userData);

			/* Activate the key. */
			return $this->activateAuthkey($userData['authkey'], $dayLife) ? self::Success : self::Failed;
		}
		
		/**
		 * Performs an user logout.
		 *
		 * @return int
		 */
		public function logout (): int
		{
			$error = false;
			
			if ( empty($this->authkey) )
				return self::AlreadyDisconnected;
			
			if ( !$this->destroyClientAuthkey() ) 
				$error = true;
			
			if ( !$this->destroyServerAuthkey() )
				$error = true;
			
			return $error ? self::Failed : self::Success;
		}
		
		/**
		 * Changes the user password.
		 *
		 * @param string $oldPassword The old password.
		 * @param string $newPassword The new password.
		 * @param string $confirmPassword The new password confirmation.
		 * @param bool $firstParameterHashed Allow the first parameter to be the hashed value instead of the clear password. Default false.
		 * @return int
		 */
		public function changePassword (string $oldPassword, string $newPassword, string $confirmPassword, bool $firstParameterHashed = false): int
		{
			if ( !$firstParameterHashed )
			{
				if ( !$this->checkPasswordSyntax($oldPassword) )
					return self::InvalidPassword;
				
				$oldPassword = md5($oldPassword);
			}
			
			$query = 
				'SELECT 1 '.
				'FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` '.
				'WHERE `id` = '.$this->id.' '.
				'AND `password` = \''.$oldPassword.'\' LIMIT 1 ;';
			
			if ( $this->database->getResult($query, 'int') < 1 )
				return self::BadPassword;
			
			if ( !$this->checkPasswordSyntax($newPassword) )
				return self::InvalidNewPassword;
			
			if ( $newPassword !== $confirmPassword )
				return self::MalformedNewPassword;
			
			$query = 
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
				'`password` = \''.md5($newPassword).'\' '.
				'WHERE `id` = '.$this->id.' '.
				'AND `password` = \''.$oldPassword.'\' LIMIT 1;';
			
			if ( $this->database->execute($query) !== false )
				return self::Success;
			
			return self::Failed;
		}
		
		/**
		 * Checks if the user is connected.
		 *
		 * @return bool
		 */
		public function isConnected (): bool
		{
			return ( $this->authkey != null );
		}

		/**
		 * Checks if the user is suspended.
		 *
		 * @return bool
		 */
		public function isSuspended (): bool
		{
			return $this->isSuspended;
		}

		/**
		 * Suspends the user account.
		 *
		 * @return bool
		 */
		public function suspend (): bool
		{
			$query = 
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
				'`suspended` = 1 '.
				'WHERE `id` = '.$this->id.' '.
				'LIMIT 1;';

			return ( $this->database->execute($query) !== false );
		}

		/**
		 * Release the user account from a previous suspension.
		 *
		 * @return bool
		 */
		public function release (): bool
		{
			$query = 
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
				'`suspended` = 0 '.
				'WHERE `id` = '.$this->id.' '.
				'LIMIT 1;';

			return ( $this->database->execute($query) !== false );
		}
		
		/**
		 * Returns the user Id.
		 *
		 * @return int
		 */
		public function getId (): int
		{
			return $this->id;
		}

		/**
		 * Returns the account creation date (UNIX timestamp).
		 *
		 * @return int
		 */
		public function getCreationDate (): int
		{
			return $this->creationDate;
		}

		/**
		 * Returns the account creation IP address.
		 *
		 * @return int
		 */
		public function getCreationIP (): int
		{
			return $this->creationIP;
		}

		/**
		 * Returns the account creation IP address.
		 *
		 * @return string
		 */
		public function getCreationIPString (): string
		{
			return long2ip($this->creationIP);
		}

		/**
		 * Returns the account creation user-agent.
		 *
		 * @return string
		 */
		public function getCreationUserAgent (): string
		{
			return $this->creationUserAgent;
		}

		/**
		 * Returns the account last modification date (UNIX timestamp).
		 *
		 * @return int
		 */
		public function getLatestModificationDate (): int
		{
			return $this->modificationDate;
		}

		/**
		 * Returns the account last connexion date (UNIX timestamp).
		 *
		 * @return int
		 */
		public function getLastConnexion (): int
		{
			return $this->lastConnexion;
		}

		/**
		 * Returns the last used IP address with this account.
		 *
		 * @return int
		 */
		public function getLastIP (): int
		{
			return $this->lastIP;
		}

		/**
		 * Returns the last used IP address with this account.
		 *
		 * @return string
		 */
		public function getLastIPString (): string
		{
			return long2ip($this->lastIP);
		}

		/**
		 * Returns the last user-agent used with this account.
		 *
		 * @return string
		 */
		public function getLastUserAgent (): string
		{
			return $this->lastUserAgent;
		}

		/**
		 * Returns the number of successful login.
		 *
		 * @return int
		 */
		public function getLoginCount (): int
		{
			return $this->loginCount;
		}

		/**
		 * Returns the number of successful logout.
		 *
		 * @return int
		 */
		public function getLogoutCount (): int
		{
			return $this->logoutCount;
		}
		
		/**
		 * Returns the authentication key of the account.
		 *
		 * @return string
		 */
		public function getAuthkey (): string
		{
			return $this->authkey;
		}

		/**
		 * Returns the name of the account.
		 *
		 * @return string
		 */
		public function getUsername (): string
		{
			return $this->username;
		}

		/**
		 * Returns the email address of the account.
		 *
		 * @return string
		 */
		public function getEmail (): string
		{
			return $this->email;
		}

		/**
		 * Changes the user level.
		 *
		 * @param int $level A int from 0 to 255 to set user level.
		 * @return bool
		 */
		public function setLevel (int $level)
		{
			if ( $level < 0 )
				$level = 0;
			elseif ( $level > 255 )
				$level = 255;

			$query = 
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` SET '.
				'`m_date` = UNIX_TIMESTAMP(), '.
				'`level` = '.$level.' '.
				'WHERE `id` = '.$this->id.' LIMIT 1 ;';

			return ( $this->database->execute($query) !== false );
		}
		
		/**
		 * Returns the user level.
		 *
		 * @return int
		 */
		public function getLevel (): int
		{
			return $this->level;
		}
		
		/**
		 * Completes the user profile data.
		 *
		 * @param array $data The profile data.
		 * @return bool
		 */
		public function feedProfile (array $data): bool
		{
			$changes = [];
			
			/* NOTE : profile data is not critical to the account, so we
			 * check only the field and not the value passed. */
			foreach ( $data as $field => $value )
			{
				switch ( $field )
				{
					case 'p_username' :
					case 'p_firstname' :
					case 'p_lastname' :
					case 'p_address' :
					case 'p_address_number' :
					case 'p_box_number' :
					case 'p_city' :
					case 'p_postal_code' :
					case 'p_phone_number' :
					case 'p_mobile_phone_number' :
					case 'p_fax_number' :
					case 'p_birthdate' :
					case 'p_lang' :
						$changes[] = '`'.$field.'` = \''.addslashes($value).'\' ';

						$this->profileData[$field] = $value;
						break;
						
					case 'p_country' :
						if ( empty($value) )
						{
							$this->profileData[$field] = '__';
							$changes[] = '`'.$field.'` = \'__\' ';
						}
						else
						{
							if ( strlen($value) == 2 )
							{
								$changes[] = '`'.$field.'` = \''.$value.'\' ';

								$this->profileData[$field] = $value;
							}
							else
							{
								trigger_error(__METHOD__.'(), "'.$value.'" is not a valid country !', E_USER_WARNING);
							}
						}
						break;
						
					case 'p_sexe' :
						$checkedValue = min(2, intval($value));
					
						$changes[] = '`'.$field.'` = '.$checkedValue.' ';

						$this->profileData[$field] = $checkedValue;
						break;
						
					default:
						if ( isset( $this->extraFields[$field] ) )
						{
							switch ( $this->extraFields[$field][0] )
							{
								case User::Int :
									$checkedValue = intval($value);
									$changes[] = '`'.$field.'` = '.$checkedValue.' ';
									break;
									
								case User::Bool :
									$checkedValue = $value ? 1 : 0;
									$changes[] = '`'.$field.'` = '.$checkedValue.' ';
									break;
									
								case User::Float :
									$checkedValue = floatval($value);
									$changes[] = '`'.$field.'` = '.$checkedValue.' ';
									break;
							
								case User::Double :
									$checkedValue = doubleval($value);
									$changes[] = '`'.$field.'` = '.$checkedValue.' ';
									break;
							
								case User::String :
								default :
									$checkedValue = $value;
									$changes[] = '`'.$field.'` = \''.addslashes($checkedValue).'\' ';
									break;
							}

							$this->profileData[$field] = $checkedValue;
						}
						else
						{
							trigger_error(__METHOD__.'(), "'.$field.'" is not a valid key !', E_USER_WARNING);
						}
						break;
				}
			}

			if ( $changes )
			{
				$query =
					'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` '.
					'SET '.implode(',', $changes).' '.
					'WHERE `id` = '.$this->id.' '.
					'LIMIT 1;';
				
				if ( $this->database->execute($query) === false )
				{
					trigger_error(__METHOD__.'(), faild to save profile. Query is "'.$query.'".', E_USER_WARNING);
					
					return false;
				}
			}
			
			return true;
		}
		
		/**
		 * Returns the user profile.
		 *
		 * @return array
		 */
		public function getProfile (): array
		{
			if ( $this->settings->get(self::EnableProfileKey) )
				return $this->profileData;
			
			if ( !$this->database->isConnected() )
			{
				trigger_error(__METHOD__.'(), database is not connected.', E_USER_WARNING);
				
				return [];
			}

			$query =
				'SELECT `p_username`, `p_firstname`, `p_lastname`, `p_sexe`, `p_birthdate`, '.
				'`p_address`, `p_address_number`, `p_box_number`, `p_city`, `p_postal_code`, '.
				'`p_phone_number`, `p_mobile_phone_number`, `p_fax_number`, `p_country`, `p_lang`';

			if ( $this->extraFields )
			{
				foreach ( $this->extraFields as $fieldName => $fieldType )
					$query .= ', `'.$fieldName.'`';
			}

			$query .= ' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `id` = '.$this->id.' LIMIT 1 ;';

			if ( $data = $this->database->getRow($query) )
				$data['p_sexe'] = intval($data['p_sexe']);

			return $data;
		}
		
		/**
		 * Forces a profile data refresh.
		 *
		 * @return bool
		 */
		public function refreshProfile (): bool
		{
			if ( $userData = $this->database->getRow('SELECT '.$this->selectFields().' FROM `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` WHERE `id` = '.$this->id.' LIMIT 1;') )
				return $this->setUserData($userData);
			
			return false;
		}
		
		/**
		 * Changes one field of the user profile.
		 *
		 * @param string $field The name of the field.
		 * @param string $value The new value of the field.
		 * @return bool
		 */
		public function setProfileData (string $field, string $value): bool
		{
			switch ( $field )
			{
				case 'p_username' :
				case 'p_firstname' :
				case 'p_lastname' :
				case 'p_address' :
				case 'p_address_number' :
				case 'p_box_number' :
				case 'p_city' :
				case 'p_postal_code' :
				case 'p_phone_number' :
				case 'p_mobile_phone_number' :
				case 'p_fax_number' :
				case 'p_birthdate' :
				case 'p_lang' :
					$queryField = '`'.$field.'` = \''.addslashes($value).'\' ';

					$this->profileData[$field] = $value;
					break;
					
				case 'p_country' :
					if ( strlen($value) == 2 )
					{
						$queryField = '`'.$field.'` = \''.$value.'\' ';

						$this->profileData[$field] = $value;
					}
					else
					{
						trigger_error(__METHOD__.'(), "'.$value.'" is not a valid country !', E_USER_WARNING);
						
						return false;
					}
					break;
					
				case 'p_sexe' :
					$checkedValue = min(2, intval($value));
					
					$queryField = '`'.$field.'` = '.$checkedValue.' ';

					$this->profileData[$field] = $checkedValue;
					break;
					
				default :
					if ( isset( $this->extraFields[$field] ) )
					{
						switch ( $this->extraFields[$field][0] )
						{
							case User::Int :
								$checkedValue = intval($value);
								
								$queryField = '`'.$field.'` = '.$checkedValue.' ';
								break;
								
							case User::Bool :
								$checkedValue = $value ? 1 : 0;
								
								$queryField = '`'.$field.'` = '.$checkedValue.' ';
								break;
								
							case User::Float :
								$checkedValue = floatval($value);
								
								$queryField = '`'.$field.'` = '.$checkedValue.' ';
								break;
						
							case User::Double :
								$checkedValue = doubleval($value);
								
								$queryField = '`'.$field.'` = '.$checkedValue.' ';
								break;
						
							case User::String :
							default :
								$checkedValue = $value;
								
								$queryField = '`'.$field.'` = \''.addslashes($checkedValue).'\' ';
								break;
						}

						$this->profileData[$field] = $checkedValue;
					}
					else
					{
						trigger_error(__METHOD__.'(), "'.$field.'" is not a valid key !', E_USER_WARNING);
						
						return false;
					}
					break;
			}

			$statement =
				'UPDATE `'.$this->settings->get(self::TableNameKey, self::DefaultTableName).'` '.
				'SET '.$queryField.' '.
				'WHERE `id` = '.$this->id.' '.
				'LIMIT 1;';
			
			if ( $this->database->execute($statement) === false )
			{
				trigger_error(__METHOD__.'(), failed to save a profile field. Query is "'.$statement.'".');
				
				return false;
			}
			
			return true;
		}
		
		/**
		 * Returns one field of the profile.
		 *
		 * @param string $field Une chaîne de caractères pour le nom du champ concerné.
		 * @return string
		 */
		public function getProfileData (string $field): string
		{
			if ( !array_key_exists($field, $this->profileData) )
			{
				trigger_error(__METHOD__.'(), The field "'.$field.'" is not part of profile ! If this warning seems weird, check "user_enable_profile" configuration. By default, the profile is not loaded automatically.', E_USER_WARNING);
				
				return '';
			}
			
			return $this->profileData[$field];
		}
		
		/**
		 * Returns the IP address as long int.
		 *
		 * @return int
		 */
		public static function getCurrentIP (): int
		{
			if ( !isset($_SERVER['REMOTE_ADDR']) )
				return 0;

			return ip2long($_SERVER['REMOTE_ADDR']);
		}
		
		/**
		 * Crée la clause SELECT de la requête Mysql pour récupérer le profil incluant les données supplémentaires.
		 *
		 * @return string
		 */
		private function selectFields (): string
		{
			$select = '`id`, `c_date`, `c_ip`, `c_useragent`, `m_date`, `last_connexion`, `last_ip`, `last_useragent`, `login_count`, `logout_count`, `username`, `password`, `authkey`, `authkey_life`, `level`, `email`, `email_error_type`, `suspended`';
			
			if ( $this->settings->get(self::EnableMultipleConnexionKey) )
			{
				$select .= ', `p_username`, `p_firstname`, `p_lastname`, `p_sexe`, `p_birthdate`, `p_address`, `p_address_number`, `p_box_number`, `p_city`, `p_postal_code`, `p_phone_number`, `p_mobile_phone_number`, `p_fax_number`, `p_country`, `p_lang`';
				
				if ( $this->extraFields )
				{
					foreach ( $this->extraFields as $fieldName => $fieldType )
						$select .= ', `'.$fieldName.'`';
				}
			}
			
			return $select;
		}

		/**
		 * Destroys every authentication keys on the server side. Disable every connexions.
		 *
		 * @param AbstractSettings $settings A reference to a valid setting object.
		 * @param Database $database A reference to a valid database object.
		 * @return bool
		 */
		public static function disconnectAll (AbstractSettings $settings, Database $database): bool
		{
			if ( $database->execute('UPDATE `'.$settings->get(self::TableNameKey, self::DefaultTableName).'` SET `authkey` = NULL;') === false )
			{
				trigger_error(__METHOD__.'(), failed to kill all authkey.', E_USER_WARNING);
				
				return false;
			}
			
			return true;
		}

		/**
		 * Une méthode statique pour créer la table dans la base de données ou afficher la requête équivalente.
		 *
		 * @param PDO|null $database Un objet PDO. Par défaut à null.
		 * @param AbstractSettings|null $settings Un objet Settings. Par défaut à null.
		 * @return bool|string
		 */
		public static function buildTable (PDO $database = null, AbstractSettings $settings = null): bool|string
		{
			if ( is_null($settings) )
				$settings = StaticSettings::instance();

			$tableName = $settings->get(self::TableNameKey, self::DefaultTableName);

			$statement =
				'CREATE TABLE IF NOT EXISTS `' . $tableName . '` '.
				'('.
				'`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, '.
				'`c_date` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`c_ip` BIGINT(20) UNSIGNED NOT NULL DEFAULT "0", '.
				'`c_useragent` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL, '.
				'`m_date` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`last_connexion` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`last_ip` BIGINT(20) UNSIGNED NOT NULL DEFAULT "0", '.
				'`last_useragent` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL, '.
				'`login_count` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`logout_count` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`username` VARCHAR(' . $settings->get(self::NameMaxCharKey, 16) . ") NOT NULL, ".
				'`password` VARCHAR(32) NOT NULL, '.
				'`authkey` VARCHAR(64) DEFAULT NULL, '.
				'`authkey_life` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`level` TINYINT(3) UNSIGNED NOT NULL DEFAULT "0", '.
				'`email` VARCHAR(127) NOT NULL, '.
				'`email_error_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT "0", '.
				'`suspended` TINYINT(1) UNSIGNED NOT NULL DEFAULT "0", '.
				'`p_username` VARCHAR(' . ( $settings->get(self::NameMaxCharKey, 16) + 16 ) . ") NOT NULL, ".
				'`p_firstname` VARCHAR(64) DEFAULT NULL, '.
				'`p_lastname` VARCHAR(64) DEFAULT NULL, '.
				'`p_sexe` TINYINT(1) UNSIGNED NOT NULL DEFAULT "2", '.
				'`p_birthdate` DATE DEFAULT NULL, '.
				'`p_address` VARCHAR(127) DEFAULT NULL, '.
				'`p_address_number` VARCHAR(8) DEFAULT NULL, '.
				'`p_box_number` VARCHAR(8) DEFAULT NULL, '.
				'`p_city` VARCHAR(64) DEFAULT NULL, '.
				'`p_postal_code` VARCHAR(16) DEFAULT NULL, '.
				'`p_phone_number` VARCHAR(32) DEFAULT NULL, '.
				'`p_mobile_phone_number` VARCHAR(32) DEFAULT NULL, '.
				'`p_fax_number` VARCHAR(32) DEFAULT NULL, '.
				'`p_country` VARCHAR(2) DEFAULT NULL, '.
				'`p_lang` VARCHAR(8) DEFAULT NULL, '.
				'PRIMARY KEY (`id`), '.
				'UNIQUE KEY `username` (`username`), '.
				'UNIQUE KEY `authkey` (`authkey`), '.
				'UNIQUE KEY `email` (`email`)'.
				') '.
				'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';

			if ( is_null($database) )
				return $statement;

			if ( $database->exec($statement) !== false )
				return true;

			return false;
		}
	}
