<?php

	/*
	* Libraries/php/LTK/WebdevMonitor.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe informe a propos de l'environnement de dev.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
    class WebdevMonitor
	{
		const WebdevMonitorRepositoryPathKey = 'webdevmonitor_repository_path';
		const WebdevMonitorDatabaseHostnameNKey = 'webdevmonitor_database_hostname';
		const WebdevMonitorDatabaseNameKey = 'webdevmonitor_database_name';
		const WebdevMonitorDatabaseUsernameKey = 'webdevmonitor_database_username';
		const WebdevMonitorDatabasePasswordKey = 'webdevmonitor_database_password';
		const WebdevMonitorApacheLogNameNKey = 'webdevmonitor_apache_log_name';

		private ?AbstractSettings $settings;
		private array $projects = [];
		private array $databases = [];
		private array $apacheLogs = [];

		/**
		 * Le constructeur.
		 *
		 * @param AbstractSettings|null $settings Une interface de paramétrage. Si aucune n'est passée, la classe va lire automatique dans StaticSettings.
		 */
		public function __construct (AbstractSettings $settings = null)
		{
			$this->settings = $settings ?? StaticSettings::instance();

			/* Retrieve git projects from directory. */
			if ( !$this->getProjectsFromDir($this->settings->get(self::WebdevMonitorRepositoryPathKey, './')) )
				trigger_error('No project found ! Check settings.', E_USER_NOTICE);

			/* Retrieve main database informations. */
			$database = new Database(Settings::build([
				Database::HostnameKey => $settings->get(self::WebdevMonitorDatabaseHostnameNKey, 'localhost'),
				Database::DatabaseNameKey => $settings->get(self::WebdevMonitorDatabaseNameKey),
				Database::UsernameKey => $settings->get(self::WebdevMonitorDatabaseUsernameKey, 'root'),
				Database::PasswordKey => $settings->get(self::WebdevMonitorDatabasePasswordKey)
			]));

			if ( !$this->getDatabases($database) )
				trigger_error('No database found ! Check settings.', E_USER_NOTICE);
			
			/* Retrieve apache log. */
			$logName = $settings->get(self::WebdevMonitorApacheLogNameNKey, 'error.log');

			$logs = '';

			if ( !System::run('tail /var/log/apache2/'.escapeshellcmd($logName), $logs) )
				trigger_error('No apache log found ! Check settings.', E_USER_NOTICE);

			$this->apacheLogs[] = $logs;
		}

		public function projects (): array
		{
			return $this->projects;
		}

		public function databases (): array
		{
			return $this->databases;
		}

		public function apacheLogs (): array
		{
			return $this->apacheLogs;
		}

		static private function gitStatus (string $path): array
		{
			$out = System::runSimple('cd '.escapeshellarg($path).' && git status -s');

			if ( $out === false )
				return ['info', 'No GIT'];

			if ( empty($out) )
				return ['success', 'Working tree clean.'];

			return ['warning', 'Work in progress !'];
		}

		private function getProjectsFromDir (string $basepath): bool
		{
			$entries = glob($basepath.'*');

			if ( empty($entries) )
				return false;
			
			foreach ( $entries as $entry )
			{
				if ( is_dir($entry) === false )
					continue;

				$path = realpath($entry);

				$tmp = explode(DIRECTORY_SEPARATOR, $entry);

				$this->projects[] = [
					'title' => ucfirst(end($tmp)),
					'local_url' => 'http://cairn-projects/'.end($tmp),
					'path' => $path,
					'git_status' => self::gitStatus($path)
				];
			}
			
			return !empty($this->projects);
		}

		private function getTables (Database $database, string $databaseName): array
		{
			$output = [];

			$query = 
				'SELECT `table_name` '.
				'FROM `information_schema`.`tables` '.
				'WHERE `table_schema` = "'.$databaseName.'";';

			if ( $tables = $database->getArray($query) )
			{
				foreach ( $tables as $table )
					$output[] = $table['table_name'];
			}

			return $output;
		}

		private function getDatabases (Database $database): bool
		{
			if ( !$database->isConnected() )
				return false;

			$query = 
				'SELECT `schema_name` '.
				'FROM `information_schema`.`schemata`;';

			if ( $databases = $database->getArray($query) )
			{
				foreach ( $databases as $database )
				{
					$this->databases[] = [
						'name' => $database['schema_name'],
						'tables' => $this->getTables($database, $database['schema_name'])
					];
				}
			}

			return !empty($this->databases);
		}
	}
