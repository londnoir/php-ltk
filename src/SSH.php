<?php

	/*
	* Libraries/php/LTK/SSH.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Class wich simulate a SSH connexion for copying or executing command. Machines must know each other before and use SSH key.
	 *
	 * Cet objet est configurable avec la class Config et les clés suivantes :\n
	 * 'ssh_default_ssh_bin', une chaîne de caractères indiquant le chemin du programme SSH à utiliser. Par défaut '/usr/bin/ssh'.
	 * 'ssh_default_scp_bin', une chaîne de caractères indiquant le chemin du programme SCP à utiliser. Par défaut '/usr/bin/scp'.
	 * 'ssh_default_server', une chaîne de caractères indiquant le serveur à joindre par défaut.
	 * 'ssh_default_port', une nombre indiquant le port par défaut à utiliser. Par défaut, le 22.
	 * 'ssh_default_username', une chaîne de caractères indiquant l'utilisateur du serveur employé par défaut.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
   	class SSH
	{
		const DefaultUsernameKey = 'ssh_default_username';
		const DefaultPortKey = 'ssh_default_port';
		const DefaultServerKey = 'ssh_default_server';
		const DefaultSSHBinKey = 'ssh_default_ssh_bin';
		const DefaultSCPBinKey = 'ssh_default_scp_bin';

		private AbstractSettings $settings;
		private string $server;
		private int $port;
		private string $username;

		/**
		 * Le constructeur.
		 *
		 * @param AbstractSettings|null $settings Une interface de settings. Si aucune n'est passée, la classe va lire automatique dans StaticSettings.
		 */
		public function __construct (AbstractSettings $settings = null)
		{
			$this->settings = $settings ?: StaticSettings::instance();

			$this->server = $this->settings->get(self::DefaultServerKey);
			$this->port = intval($this->settings->get(self::DefaultPortKey, 22));
			$this->username = $this->settings->get(self::DefaultUsernameKey, 'root');
			
			if ( empty($this->server) )
				trigger_error(__METHOD__.'(), No server.', E_USER_NOTICE);
		}

		/**
		 * Retourne le paramètre pour la commande SSH.
		 *
		 * @return string
		 */
		private function getSSHServer (): string
		{
			return ( $this->username ? $this->username.'@' : null ).$this->server;
		}
				
		/**
		 * Exécute une commande sur un serveur distant.
		 *
		 * @param string $distantCommand La commande.
		 * @param string &$out Permet de lire la sortie de la commande. Optionnel.
		 * @return bool
		 */
		public function executeCommand (string $distantCommand, string &$out = ''): bool
		{
			$command = $this->settings->get(self::DefaultSSHBinKey, '/usr/bin/ssh').' -p'.$this->port.' '.escapeshellarg($this->getSSHServer()).' '.escapeshellarg($distantCommand);
			
			return System::run($command, $out);
		}
		
		/**
		 * Effectue une conversion de la vidéo dans un format spécifique.
		 *
		 * @param string $source Le chemin du fichier local à envoyer.
		 * @param string $destination Le chemin du fichier sur le serveur de destination.
		 * @param string &$out Permet de lire la sortie de la commande. Optionnel.
		 * @return bool
		 */
		public function copy (string $source, string $destination, string &$out = ''): bool
		{
			$command = $this->settings->get(self::DefaultSCPBinKey, '/usr/bin/scp').' -P'.$this->port.' -q '.escapeshellarg($source).' '.escapeshellarg($this->getSSHServer()).':'.escapeshellarg($destination);
			
			return System::run($command, $out);
		}
	}
	