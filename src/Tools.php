<?php

	/*
	* Libraries/php/LTK/Tools.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;

	/**
	 * Cette classe contient les outils de tout type.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class Tools
	{
		/** @internal */
		private function __construct () {}

		/**
		 * Affiche le contenu d'une variable (en mode plain/text) en coupant la sortie PHP.
		 * Si les données sont de type "string", "echo" sera simplement utilisé. 
		 * Si le type est "array", "print_r()" sera utilisé, dans tout les autres cas "var_dump()" sera utilisé.
		 *
		 * @param mixed $variable La variable à afficher.
		 * @param bool $forceVarDump Force l'utilisation de la fonction var_dump() pour tout les types.
		 */
		public static function varDump (mixed $variable, bool $forceVarDump = false)
		{
			header('Content-Type: text/plain; charser=UTF-8');

			if ( !$forceVarDump && is_string($variable) )
				echo $variable."\n";
			else if ( !$forceVarDump && is_array($variable) )
				print_r($variable);
			else
				var_dump($variable);

			exit(0);
		}

		/**
		 * Permet d'inclure via la fonction PHP include() des fichiers dans une variable.
		 *
		 * @param string $filepath Le script PHP à inclure.
		 * @return string
		 */
		public static function includeToBuffer (string $filepath): string
		{
			ob_start();
			
			include($filepath);
			
			$buffer = ob_get_contents();
			
			ob_end_clean();

			return $buffer;
		}

		/**
		 * Vérifie la présence d'un cookie après un temps déterminé.
		 * Protection basique pour calmer les requêtes intempestives.
		 *
		 * @param string $name Une chaîne de caractères pour le nom du cookie.
		 * @param int $time Un entier pour déterminer en secondes le temps que le cookie est valide.
		 * @return bool
		 */
		public static function checkSpeedModerator (string $name, int $time = 300): bool
		{
			if ( self::getValue($name, 'c', 'bool') )
				return false;

			setcookie($name, '1', time() + $time, '/');

			return true;
		}

		/**
		 * Vérifie que la chaîne de caractères est bien une signature SHA1.
		 *
		 * @param string $string Une chaîne de caractères à vérifier.
		 * @return bool
		 */
		public static function isSHA1 (string $string): bool
		{
			if ( strlen($string) !== 40 )
				return false;

			return preg_match('#^([0-9a-fA-F]{40})$#', $string);
		}

		/**
		 * Vérifie que la chaîne de caractères est bien une signature MD5.
		 *
		 * @param string $string Une chaîne de caractères à vérifier.
		 * @return bool
		 */
		public static function isMD5 (string $string): bool
		{
			if ( strlen($string) !== 32 )
				return false;

			return preg_match('#^([0-9a-fA-F]{32})$#', $string);
		}
		
		/**
		 * Génère un mot de passe aléatoire en suivant une chaîne de caractères.
		 *
		 * @param int $count Un entier pour la longueur du mot de passe désiré.
		 * @param string $charsList Une chaîne de caractères dans laquelle seront utilisé les caractères pour générer le mot de passe. Par défaut à 'a-zA-Z0-9'.
		 * @return string
		 */
		public static function generateBasicPassword (int $count, string $charsList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string
		{
			if ( $count <= 0 )
			{
				trigger_error('Count parameter must be positive and non-zero !', E_USER_WARNING);

				return '';
			}

			$buffer = '';

			$charsList = str_shuffle($charsList);
			$endKey = strlen($charsList) - 1;

			for ( $i = 0; $i < $count; $i++ )
				$buffer .= $charsList[rand(0, $endKey)];

			return $buffer;
		}

		/**
		 * Télécharge un fichier depuis un fichier vers un emplacement sur le serveur.
		 *
		 * @param string $fileURL Une chaîne de caractères pour l'URL du fichier à télécharger.
		 * @param string $fileDestinationPath Une chaîne de caractères pour le chemin du fichier a sauvegarder sur le serveur.
		 * @return bool
		 */
		public static function download (string $fileURL, string $fileDestinationPath): bool
		{
			$fileURL = str_replace(' ', '%20', html_entity_decode($fileURL));

			$dir = explode('/', $fileDestinationPath);
			array_pop($dir);
			$dir = implode('/', $dir);

			if ( !Path::build($dir) )
				return false;

			if ( file_exists($fileDestinationPath) )
				chmod($fileDestinationPath, 0777);

			if ( ($rh = fopen($fileURL, 'rb')) === false )
				return false;

			if ( ($wh = fopen($fileDestinationPath, 'wb')) === false )
				return false;

			while ( !feof($rh) )
			{
				if ( fwrite($wh, fread($rh, 1024)) === false )
				{
					fclose($rh);
					fclose($wh);

					return false;
				}
			}

			fclose($rh);
			fclose($wh);

			return true;
		}

		/** 
		 * Returns a web page content in a string.
		 *
		 * @param string $url a string for the web page URL.
		 * @return string
		 */
		public static function downloadWebPage (string $url): string
		{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$buffer = curl_exec($ch);

			curl_close($ch);
			
			return $buffer;
		}

		/**
		 * Simule une requête POST sans utiliser cURL.
		 *
		 * @param string $url L'url ciblée. Elle sera vérifiée.
		 * @param array $data Les données à transmettre sous forme de tableau clé/valeurs. Exemple : $a['une_variable'] = 'sa_valeur'.
		 * @param array $additionalHeaders Permet d'ajouter des en-têtes supplémentaires sous forme de tableau clé/valeurs. Exemple : $a['Server'] = 'Mon serveur'.
		 * @param int $port Le numéro de port à employer. Par défaut, 80.
		 * @return array
		 */
		public static function postRequest (string $url, array $data, array $additionalHeaders = [], int $port = 80): array
		{
			$response = [
				'header' => '',
				'body' => ''
			];

			$urlComp = parse_url($url);

			if ( $fp = fsockopen($urlComp['host'], $port) )
			{
				$tmp = [];

				foreach ( $data as $key => $value )
					$tmp[] = $key.'='.$value;

				$dataString = implode('&', $tmp);

				$postRequest =
					'POST '.$urlComp['path'].' HTTP/1.0'."\r\n".
					'Host: '.$urlComp['host']."\r\n".
					'Content-Type: application/x-www-form-urlencoded'."\r\n".
					'Content-Length: '.strlen($dataString)."\r\n";
					
				if ( $additionalHeaders )
				{
					foreach ( $additionalHeaders as $headerName => $headerValue )
					{
						$test = strtolower($headerName);

						switch ( $test )
						{
							case 'host' :
							case 'content-type' :
							case 'content-length' :
								trigger_error(__FUNCTION__.'(), '.$headerName.' header is already present. It cannot be added twice !');
								break;

							default:
								$postRequest .= $headerName.': '.$headerValue."\r\n";
								break;
						}
					}
				}
				
				/* Header/data separator */
				$postRequest .= 
					"\r\n".
					$dataString."\r\n";

				/* Send the request. */
				if ( fputs($fp, $postRequest) )
				{
					/* Get response header. */
					$currentLine = null;
					
					while ( $currentLine != "\r\n" )
					{
						$currentLine = fgets($fp, 128);
						$response['header'] .= $currentLine;
					}

					/* Get response body. */
					while ( !feof($fp) )
						$response['body'] .= fgets($fp, 128);
				}

				fclose($fp);
			}

			return $response;
		}

		/**
		 * Vérifie qu'une chaîne de caractères ne contient pas une balise Javascript.
		 *
		 * @param string $buffer Une chaîne de caractères à vérifier.
		 * @return bool
		 */
		public static function isJSFree (string $buffer): bool
		{
			if ( empty($buffer) )
				return true;

			$terms = [
				'<script', 
				'script/>', 
				'type="text/javascript"',
				'language="javascript"',
				'$.('
			];

			foreach ( $terms as $term )
				if ( stripos($buffer, $term) !== false )
					return false;

			return true;
		}

		/**
		 * Vérifie si un argument existe sur la commande passé en mode CLI.
		 *
		 * @param string $argName Nom de l'argument.
		 * @param string $alternateArgName Aternate way of spelling the argument.
		 * @return bool
		 */
		public static function isArgumentPresent (string $argName, string $alternateArgName = ''): bool
		{
			for ( $arg = 0; $arg < $_SERVER['argc']; $arg++ )
			{
				if ( $_SERVER['argv'][$arg] === $argName )
					return true;

				if ( !empty($alternateArgName) && $_SERVER['argv'][$arg] === $alternateArgName )
					return true;
			}

			return false;
		}

		/**
		 * Retourne la valeur d'un argument passer au script en mode CLI.
		 * NOTE: Argument du type "--variable value"
		 *
		 * @param string $argName Nom de l'argument.
		 * @param string $defaultValue La valeur par défaut, si l'argument n'existe pas.
		 * @return string
		 */
		public static function getArgument (string $argName, string $defaultValue = ''): string
		{
			for ( $arg = 0; $arg < $_SERVER['argc']; $arg++ )
			{
				if ( $_SERVER['argv'][$arg] === $argName )
				{
					/* Go to the next argument. */
					$arg++;

					if ( $arg < $_SERVER['argc'] )
						return $_SERVER['argv'][$arg];

					trigger_error(__METHOD__.', the argument "'.$argName.'" exists but there is no value after !', E_USER_WARNING);

					break;
				}
			}

			return $defaultValue;
		}

		/**
		 * Retourne la valeur d'un argument passer au script en mode CLI.
		 * NOTE: Argument du type "--variable=value"
		 *
		 * @param string $argName Nom de l'argument.
		 * @param string $defaultValue La valeur par défaut, si l'argument n'existe pas.
		 * @param string $separator Quel sépateur utiliser. Par défaut '='.
		 * @return string
		 */
		public static function getGluedArgument (string $argName, string $defaultValue = '', string $separator = '='): string
		{
			for ( $arg = 0; $arg < $_SERVER['argc']; $arg++ )
			{
				if ( stripos($_SERVER['argv'][$arg], $argName) !== false )
				{
					$chunks = explode($separator, $_SERVER['argv'][$arg]);

					if ( count($chunks) >= 2 )
						return $chunks[1];

					trigger_error(__METHOD__.', the argument "'.$_SERVER['argv'][$arg].'" is illformed !', E_USER_WARNING);

					break;
				}
			}

			return $defaultValue;
		}

		/**
		 * Permet de lire une variable dans les tableaux super-globaux de PHP ($_GET, $_POST, ...) en appliquant des règles de conversion et de sécurité.
		 *
		 * @param string $index Correspond au nom de la variable recherchée.
		 * @param string $order Permet d'indiquer l'ordre des tableaux super-globaux pour effectuer la recherche.
		 * La recherche s'interrompt dés qu'un des tableaux contient la variable.\n
		 * Exemple :\n
		 * "gpcs" recherchera la variable dans l'ordre des tableaux suivants : $_GET, $_POST, $_COOKIE et $_SESSION.\n
		 * "cp" : $_COOKIE puis $_POST.
		 * @param string $type Une chaîne de caractères permettant d'indiquer le type de la variable recherchée, une conversion aura lieu.\n
		 * Les types peuvent être : "string_b64", "string_define", "string_md5", "string_trim", "string_public", "string", "double", "float", "array_int", "array_uint", "array_string", "array_string_public", "array_serialized", "array_packed" (Path::arrayFormEncode()), "int", "uint" ou "bool".
		 * @param mixed $default Une valeur par défaut à donner dans le cas où la variable n'est pas trouvée pas dans les tableaux. Par défaut, la méthode retournera une valeur nulle dans le type demandé. 0 pour un nombre, false pour un booléen, null pour une chaîne de caractères.
		 * @return mixed
		 */
		public static function getValue (string $index, string $order = 'gp', string $type = 'string', mixed $default = false): mixed
		{
			if ( !$index )
				return null;

			$count = strlen($order);
			$value = false;

			/* diff between string and array values. */
			switch ( $type )
			{
				case 'array_int' :
				case 'array_uint' :
				case 'array_string' :
				case 'array_string_public' :
					for ( $i = 0; $i < $count; $i++ )
					{
						switch ( $order[$i] )
						{
							case 'g' :
								$value = ( isset($_GET[$index]) && is_array($_GET[$index]) && count($_GET[$index]) ) ? $_GET[$index] : false;
								break;

							case 'p' :
								$value = ( isset($_POST[$index]) && is_array($_POST[$index]) && count($_POST[$index]) ) ? $_POST[$index] : false;
								break;

							case 'c' :
								$value = ( isset($_COOKIE[$index]) && is_array($_COOKIE[$index]) && count($_COOKIE[$index]) ) ? $_COOKIE[$index] : false;
								break;

							case 's' :
								$value = ( isset($_SESSION[$index]) && is_array($_SESSION[$index]) && count($_SESSION[$index]) ) ? $_SESSION[$index] : false;
								break;

							default :
								return 0;
						}

						if ( $value !== false )
							break;
					}
					break;

				default :
					for ( $i = 0; $i < $count; $i++ )
					{
						switch ( $order[$i] )
						{
							case 'g' :
								$value = ( isset($_GET[$index]) && is_string($_GET[$index])  && strlen($_GET[$index]) ) ? $_GET[$index] : false;
								break;

							case 'p' :
								$value = ( isset($_POST[$index]) && is_string($_POST[$index]) && strlen($_POST[$index]) ) ? $_POST[$index] : false;
								break;

							case 'c' :
								$value = ( isset($_COOKIE[$index]) && is_string($_COOKIE[$index]) && strlen($_COOKIE[$index]) ) ? $_COOKIE[$index] : false;
								break;

							case 's' :
								$value = isset($_SESSION[$index]) ? $_SESSION[$index] : false;
								break;

							default :
								return 0;
						}

						if ( $value !== false )
							break;
					}
					break;
			}

			if ( $value === false )
			{
				if ( $default === false )
				{
					switch ( $type )
					{
						case 'string_b64' :
						case 'string_define' :
						case 'string_md5' :
						case 'string_trim' :
						case 'string_public' :
						case 'string' :
							return '';

						case 'double' :
						case 'float' :
							return 0.0;

						case 'array_int' :
						case 'array_uint' :
						case 'array_string' :
						case 'array_string_public' :
						case 'array_serialized' :
						case 'array_packed' :
							return [];

						case 'int' :
						case 'uint' :
						case 'bool' :
						default :
							return 0;
					}
				}
				else
				{
					/* Return default set by the user. */
					return $default;
				}
			}
			/* Process return type. */
			else
			{
				switch ( $type )
				{
					case 'string' :
						return strval($value);

					case 'uint' :
						return max(0, intval($value));

					case 'int' :
						return intval($value);

					case 'bool' :
						return $value ? 1 : 0;

					case 'string_b64' :
						return base64_decode($value);

					case 'string_define' :
						return defined($value) ? constant($value) : '';

					case 'string_md5' :
						return md5($value);

					case 'string_trim' :
						return trim($value);

					case 'string_public' :
						return self::isJSFree($value) ? $value : '';

					case 'double' :
						return doubleval($value);

					case 'float' :
						return floatval($value);

					case 'array_int' :
						foreach ($value as $k => $v)
							$value[$k] = intval($v);
						return $value;

					case 'array_uint' :
						foreach ($value as $k => $v)
							$value[$k] = max(0, intval($v));
						return $value;

					case 'array_string' :
						return $value;

					case 'array_string_public' :
						foreach ($value as $k => $v)
							if ( !self::isJSFree($v) )
								$value[$k] = '';
						return $value;

					case 'array_serialized' :
						return unserialize($value);

					case 'array_packed' :
						return $value ? Path::arrayFormDecode($value) : [];

					default :
						return $value;
				}
			}
		}

		/**
		 * Affiche le contenu du variable en mode texte et arrête l'exécution du script.
		 *
		 * @param mixed $variable La variable à debugger.
		 * @param bool $details Un booléen qui permet d'avoir plus d'infos sur les types. Par défaut, false.
		 */
		public static function breakpoint (mixed $variable, bool $details = false)
		{
			header('Content-Type: text/plain; charset=utf-8');
			
			if ( $details )
				var_dump($variable);
			else
				print_r($variable);
				
			exit(1);
		}

		public static array $cleanChars = [
			'Ã©' => 'é',
			'Ã¨' => 'è',
			'Ã  ' => 'à',
			'Ã­' => 'í',
			'Ã ' => 'à',
			'Ã§' => 'ç',
			'Ã´' => 'ô',
			'Ã¶' => 'ö',
			'Ã±' => 'ñ',
			'Ãª' => 'ê',
			'Ã¤' => 'ä',
			'Ã¯' => 'ï',
			'Ã‰' => 'É' /* FAILS */
		];

		/**
		 * Corrige une chaîne mal encodée.
		 *
		 * @param string $string Chaîne à corriger.
		 * @param array $chars Liste de caractères à remplacer.
		 * @param bool $missingChar Permet de savoir si un caractère n'a pas été trouvé.
		 * @return string
		 */
		public static function utf8fix (string $string, array $chars, bool & $missingChar = false): string
		{
			while ( ($pos = strpos($string, 'Ã')) !== false )
			{
				/* 1. On extrait le mauvais caractère. */
				$badUTF8 = substr($string, $pos, 4);

				/* 2. On vérifie si on connait ce caractère. */
				if ( array_key_exists($badUTF8, $chars) )
				{
					$goodUTF8 = $chars[$badUTF8];
				}
				else
				{
					$goodUTF8 = '?';

					$missingChar = true;
				}

				/* 3. On remplace le caractère défectueux. */
				$string = str_replace($badUTF8, $goodUTF8, $string);
			}

			return $string;
		}

		/**
		 * Retourne les informations d'exécution du script.
		 * Il est important que la $GLOBALS['ltk_top'] soit présente au début du script. Elle se situe par défaut dans "./r_config/boot.php".
		 *
		 * @param bool $return N'affiche pas directement à l'écran.
		 * @return string
		 */
		public static function printStatistics (bool $return = false): string
		{
			$buffer = '<!-- PHP Script generated '.( isset($GLOBALS['ltk_top']) ? 'in '.round(microtime(true) - $GLOBALS['ltk_top'], 4).' seconds ' : null ).'with '.round( memory_get_peak_usage(true) / 1048576, 2).' Mib -->'."\n";

			if ( !$return )
				echo $buffer;

			return $buffer;
		}
	};
