<?php

	/*
	* Libraries/php/LTK/FileSettings.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Settings class implementation using file.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class FileSettings extends AbstractSettings
	{
		private string $filepath;
		private FileSettingsType $type;
		private bool $writeOnClose;

		/**
		 * Constructeur. Lis le fichier.
		 *
		 * @param string $filepath Le chemin du fichier.
		 * @param FileSettingsType|null $type Le type de fichier contenant les variables.
		 * @param bool $writeOnClose
		 */
		public function __construct (string $filepath, FileSettingsType $type = null, bool $writeOnClose = false)
		{
			$this->filepath = $filepath;
			$this->type = is_null($type) ? new FileSettingsType('json') : $type;
			$this->writeOnClose = $writeOnClose;

			if ( file_exists($this->filepath) )
			{
				if ( is_readable($this->filepath) )
				{
					$content = file_get_contents($this->filepath);

					if ( !empty($content) )
					{
						if ( !$this->decodeData($content) )
							trigger_error(__METHOD__.'(), unable to decode file "'.$this->filepath.'" !', E_USER_ERROR);
					}
				}
				else
				{
					trigger_error(__METHOD__.'(), file "'.$this->filepath.'" exists but is not readable !', E_USER_ERROR);
				}
			}
			else
			{
				$dir = Path::extractPath($this->filepath);

				if ( !Path::build($dir) || !is_writable($dir) )
					trigger_error(__METHOD__.'(), directory "'.$dir.'" is not accessible or writable !', E_USER_ERROR);
			}
		}

		/**
		* Destructeur. Sauvegarde le fichier.
		*/
		public function __destruct ()
		{
			if ( !$this->writeOnClose )
				return;

			if ( !file_put_contents($this->filepath, $this->getEncodedData()) )
				trigger_error(__METHOD__.'(), file "'.$this->filepath.'" is not writable !', E_USER_ERROR);
		}

		/**
		* Permet de décoder le contenu d'un fichier en fonction de son type.
		*
		* @internal
		* @param string $content Le contenu du fichier.
		* @return bool.
		*/
		private function decodeData (string $content): bool
		{
			switch ( $this->type->selectedKey() )
			{
				case 'json' :
					$entries = json_decode($content, true);

					if ( $entries !== false )
					{
						foreach ( $entries as $entry )
							$this->set($entry[0], $entry[1], $entry[2]);

						return true;
					}
					break;

				case 'xml' :
				case 'ini' :
					break;
			}

			return false;
		}

		/**
		* Permet d'encoder le contenu dans un fichier en fonction de son type.
		*
		* @internal
		* @return string.
		*/
		private function getEncodedData (): string
		{
			switch ( $this->type->selectedKey() )
			{
				case 'json' :
					$lines = [];

					foreach ( $this->asArray() as $key => $value )
						$lines[] = [$key, $value, $this->isReadOnly($key) ? 1 : 0];

					return json_encode($lines, JSON_PRETTY_PRINT);

				case 'xml' :
				case 'ini' :
					break;
			}

			return '';
		}

		/**
		 * Permet de construire d'instancier un objet FileSettings depuis un tableau de données.
		 * Simple raccourcit d'instanciation et de l'utilisateur de la méthode FileSettings::setArray().
		 *
		 * @param string $filepath A filepath to the settings file.
		 * @param array $data Un tableau associatif dont les clés seront les noms des variables, les valeurs seront les valeurs des variables.
		 * @return FileSettings.
		 */
		static public function build (string $filepath, array $data): FileSettings
		{
			$settings = new FileSettings($filepath);
			$settings->setArray($data);
			
			return $settings;
		}
	}
