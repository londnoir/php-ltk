<?php

	/*
	* Libraries/php/LTK/MessageBus.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;

	use PDO;

	/**
	* Class responsible for logging execution message and save them.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class MessageBus
	{
		/* Type of message. */
		const Informations = 'Informations';
		const Warning = 'Warning';
		const Error = 'Error';
		const Fatal = 'Fatal';
		const Positive = 'Positive';
		const Negative = 'Negative';

		const TableNameKey = 'messagebus_table_name';
		const DefaultTableName = 'ltk_log';
		const LogDirectoryKey = 'messagebus_log_directory';
		const EnabledKey = 'messagebus_enabled';
		const DoNotUseEchoKey = 'messagebus_do_not_use_echo';
		const EnableLogNotificationKey = 'messagebus_enable_log_notification';
		const AlwaysSendLogToFileKey = 'messagebus_always_send_log_to_file';
		const EnableDatabaseKey = 'messagebus_enable_database';
		const AutoDisplayNotificationsKey = 'messagebus_auto_display_notifications';

		private ?AbstractSettings $settings ;
		private ?Database $database;
		private string $tag;
		private string $userIP;
		private string $scriptName;
		private $fileHandle = null;
		private bool $enableDatabase = false;
		private array $notifications = [];
		private $standardPHPErrorsHandler;
		private bool $enabled;
		private bool $doNotUseEcho;
		private bool $enableLogNotification;
		private bool $alwaysSendLogToFile;

		/**
		 * Le constructeur.
		 *
		 * @param string $tag Un tag pour trier les messages.
		 * @param Database|null $database Un accès à une base de données pour sauvegarder les log en db. Optionnel.
		 * @param AbstractSettings|null $settings Une interface de paramétrage. Si aucune n'est passée, la classe va lire automatique dans StaticSettings.
		 */
		public function __construct (string $tag = 'default', Database $database = null, AbstractSettings $settings = null)
		{
			$this->settings = $settings ?: StaticSettings::instance();
			/* Use the database */
			if ( $database )
			{
				$this->database = $database;

				if ( $this->database->isConnected() )
					$this->enableDatabase = (bool)$this->settings->get(self::EnableDatabaseKey, true);
			}

			$this->tag = $tag;
			$this->userIP = $_SERVER['REMOTE_ADDR'] ?? '[CLI]';
			$this->scriptName = $_SERVER['SCRIPT_NAME'] ?? 'unknow';

			/* Options */
			$this->enabled = (bool)$this->settings->get(self::EnabledKey, true);
			$this->doNotUseEcho = (bool)$this->settings->get(self::DoNotUseEchoKey);
			$this->enableLogNotification = (bool)$this->settings->get(self::EnableLogNotificationKey);
			$this->alwaysSendLogToFile = (bool)$this->settings->get(self::AlwaysSendLogToFileKey);

			/* Create a check log file. */
			if ( $this->createLogFile() )
				$this->fileHandle = fopen($this->filepath(), 'a+');

			/* PHP errors handler replacement. */
			$this->standardPHPErrorsHandler = set_error_handler([$this, "PHPErrorsHandler"]);
		}

		/**
		 * Le destructeur.
		 */
		public function __destruct ()
		{
			/* Close log file */
			if ( $this->fileHandle )
				fclose($this->fileHandle);

			/* Auto output notifications. */
			if ( $this->settings->get(self::AutoDisplayNotificationsKey, false) )
				foreach ( $this->notifications(true) as $notification )
					echo $notification['type'].' : '.$notification['message']."\n";
		}

		/**
		 * Création du fichier de log.
		 *
		 * @internal
		 * @return bool.
		 */
		private function createLogFile (): bool
		{
			$filepath = $this->filepath();

			if ( file_exists($filepath) )
				return is_writeable($filepath);

			$directory = Path::extractPath($filepath);

			if ( !Path::build($directory) || !is_writeable($directory) )
			{
				trigger_error(__METHOD__.'(), unable to create/write "'.$directory.'" directory !', E_USER_WARNING);

				return false;
			}

			touch($filepath);

			return true;
		}

		/**
		 * Vérifie la validité du paramètre de severité.
		 *
		 * @internal
		 * @param string $severity Check the severity parameter.
		 * @return string A valid severity.
		 */
		private function checkSeverity (string $severity): string
		{
			switch ( $severity )
			{
				case self::Informations :
				case self::Warning :
				case self::Error :
				case self::Fatal :
					return $severity;
				
				default:
					trigger_error(__METHOD__.'(), severity "'.$severity.'" is not handled !', E_USER_WARNING);

					return self::Warning;
			}
		}

		/**
		* Vérifie la validité du paramètre de type.
		*
		* @internal
		* @param string $type Check the severity parameter.
		* @return string A valid severity.
		*/
		private function checkType (string $type): string
		{
			switch ( $type )
			{
				case self::Informations :
				case self::Positive :
				case self::Negative :
					return $type;
				
				default:
					trigger_error(__METHOD__.'(), type "'.$type.'" is not handled !', E_USER_WARNING);

					return self::Informations;
			}
		}

		/**
		 * Convertit une sévérité de log en type de notification.
		 *
		 * @internal
		 * @param string $severity.
		 * @return string
		 */
		static private function severityToType (string $severity): string
		{
			switch ( $severity )
			{
				case self::Informations :
					return self::Informations;

				case self::Warning :
				case self::Error :
				case self::Fatal :
					return self::Negative;
				
				default:
					trigger_error(__METHOD__.'(), severity "'.$severity.'" is not handled !', E_USER_WARNING);

					return self::Negative;
			}
		}

		/**
		 * Retourne le chemin complet du fichier de log.
		 *
		 * @internal
		 * @return string
		 */
		private function filepath (): string
		{
			return $this->settings->get(self::LogDirectoryKey, Path::getTemporaryDirectory('php-ltk.log_dir')).'ltk_'.$this->tag.'.log';
		}

		/**
		 * Permet, si on a passé une base de données au constructeur de
		 * tout de même désactiver l'enregistrement en base de données.
		 *
		 * @internal
		 * @param bool $state.
		 */
		public function enableDatabase (bool $state)
		{
			$this->enableDatabase = $state;
		}

		/**
		 * Enregistre un log. Un log est destiné à l'administration du site.
		 *
		 * @param string $message Le contenant du message à logger.
		 * @param string $severity Le niveau du message. Par défaut, MessageBus::Warning.
		 * @return bool
		 */
		public function log (string $message, string $severity = self::Warning): bool
		{
			if ( empty($message) )
			{
				trigger_error(__METHOD__.'(), message empty !', E_USER_WARNING);

				return false;
			}

			/* Send a copy to notifications system. */
			if ( $this->enableLogNotification )
				$this->notify($message, self::severityToType($severity));

			/* We don't save anything ! */
			if ( $this->enabled === false )
				return true;

			$severity = $this->checkSeverity($severity);

			/* If database is enabled we send the log to it first. */
			if ( $this->enableDatabase )
			{
				$query = Database::writeInsertQuery($this->settings->get(self::TableNameKey, self::DefaultTableName), [
					'tag' => $this->tag,
					'user_ip' => ip2long($this->userIP),
					'script' => $this->scriptName,
					'message' => $message,
					'severity' => $severity
				]);

				if ( $this->database->execute($query) )
				{	
					/* If we want a copy in log file, 
					 * we do interrupt the function. */
					if ( $this->alwaysSendLogToFile === false )
						return true;
				}
			}

			/* Save into file. */
			$line = '['.time().']['.$severity.'] '.$message.PHP_EOL;

			if ( $this->fileHandle )
			{
				fwrite($this->fileHandle, $line);

				return true;
			}

			/* On failure we display the log on page. */
			if ( $this->doNotUseEcho === false )
				echo $line;

			return false;
		}

		/**
		 * Enregistre une notification. Une notification est destinée à l'utilisateur.
		 *
		 * @param string $message Le contenant du message à notifier.
		 * @param string $type Le niveau de la notification. Par défaut, MessageBus::Informations.
	 	 * @return bool
 		 */
		public function notify (string $message, string $type = self::Informations): bool
		{
			if ( empty($message) )
			{
				trigger_error(__METHOD__.'(), message empty !', E_USER_WARNING);

				return false;
			}

			$this->notifications[] = [
				'time' => time(),
				'message' => $message,
				'type' => $this->checkType($type)
			];

			return true;
		}

		/**
		 * Retourne les notifications.
		 *
		 * @param bool $autoFlush Permet de détruire les notifications une fois retournée par cette méthode. Par défaut, false.
		 * @param string $filter Permet de filtrer sur le niveau de la notification. Par défaut, toutes notifications seront envoyées.
		 * @return array
		 */
		public function notifications (bool $autoFlush = false, string $filter = ''): array
		{
			if ( empty($filter) )
			{
				$notifications = $this->notifications;
			}
			else
			{
				$notifications = [];

				foreach ( $this->notifications as $notification )
				{
					if ( $filter !== $notification['type'] )
						continue;

					$notifications[] = $notification;
				}
			}

			/* Flush notifications. */
			if ( $autoFlush )
				$this->notifications = [];

			return $notifications;
		}

		/**
		 * Method that intercepts error message from PHP.
		 *
		 * @param int $errno The PHP error number.
		 * @param string $errstr The error message.
		 * @param string $errfile The file where the error is.
		 * @param int $errline The number of the line in the file where the error is.
		 * @return bool
		 */
		public function PHPErrorsHandler (int $errno, string $errstr, string $errfile, int $errline): bool
		{
			if ( !(error_reporting() & $errno) )
				return false;

			$severity = match ($errno) {
				E_USER_NOTICE, E_STRICT => self::Informations,
				E_WARNING, E_CORE_WARNING, E_COMPILE_WARNING, E_USER_WARNING, E_PARSE, E_NOTICE, E_DEPRECATED, E_USER_DEPRECATED => self::Warning,
				E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR => self::Error,
			};

			$this->log('PHP '.PHP_VERSION.' ('.PHP_OS.') error number #'.$errno.', file "'.$errfile.'", line #'.$errline.' : '.$errstr, $severity);

			/* Don't execute PHP internal error handler */
			return true;
		}

		/**
		 * Une méthode statique pour créer la table dans la base de données ou afficher la requête équivalente.
		 *
		 * @param PDO|null $database Un objet PDO. Par défaut à null.
		 * @param AbstractSettings|null $settings Un objet Settings. Par défaut à null.
		 * @return bool|string
		 */
		public static function buildTable (PDO $database = null, AbstractSettings $settings = null): bool|string
		{
			if ( is_null($settings) )
				$settings = StaticSettings::instance();

			$tableName = $settings->get(self::TableNameKey, self::DefaultTableName);

			$statement = 
				'CREATE TABLE IF NOT EXISTS `' . $tableName . '` '.
				'('.
				'`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, '.
				'`c_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, '.
				'`tag` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL, '.
				'`user_ip` INT(10) UNSIGNED NOT NULL DEFAULT "0", '.
				'`script` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL, '.
				'`message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL, '.
				'`severity` enum("' . self::Informations . '","' . self::Warning . '","' . self::Error . '","' . self::Fatal . '") COLLATE utf8mb4_bin NOT NULL DEFAULT "' . self::Warning . '", '.
				'PRIMARY KEY (`id`)'.
				') '.
				'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';

			if ( is_null($database) )
				return $statement;

			return $database->exec($statement) !== false;
		}
	}
