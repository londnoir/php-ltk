/*global unescape*/

/*
* Libraries/js/main.functions.js
* This file is part of PHP-LTK 
*
* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
*
* PHP-LTK is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* PHP-LTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
  
var nav = navigator.userAgent;

var FF = (nav.match('Firefox') || nav.match('Iceweasel')) ? 1 : 0;
var IE7 = (nav.match('MSIE 7.0')) ? 1 : 0;
var IE6 = (nav.match('MSIE 6.0') && !IE7) ? 1 : 0;

// NOTE : document.domain == $_SERVER['HTTP_HOST']
  
/**
*
*  MD5 (Message-Digest Algorithm)
*  http://www.webtoolkit.info/
*
**/
var MD5 = function (string)
{
 
	function RotateLeft(lValue, iShiftBits) {
		return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
	}
 
	function AddUnsigned(lX,lY) {
		var lX4,lY4,lX8,lY8,lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			} else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		} else {
			return (lResult ^ lX8 ^ lY8);
		}
 	}
 
 	function F(x,y,z) { return (x & y) | ((~x) & z); }
 	function G(x,y,z) { return (x & z) | (y & (~z)); }
 	function H(x,y,z) { return (x ^ y ^ z); }
	function I(x,y,z) { return (y ^ (x | (~z))); }
 
	function FF(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function GG(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function HH(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function II(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function ConvertToWordArray(string) {
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1=lMessageLength + 8;
		var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
		var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
		var lWordArray=Array(lNumberOfWords-1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while ( lByteCount < lMessageLength ) {
			lWordCount = (lByteCount-(lByteCount % 4))/4;
			lBytePosition = (lByteCount % 4)*8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount-(lByteCount % 4))/4;
		lBytePosition = (lByteCount % 4)*8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
		lWordArray[lNumberOfWords-2] = lMessageLength<<3;
		lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
		return lWordArray;
	};
 
	function WordToHex(lValue) {
		var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
		for (lCount = 0;lCount<=3;lCount++) {
			lByte = (lValue>>>(lCount*8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
		}
		return WordToHexValue;
	};
 
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	};
 
	var x=Array();
	var k,AA,BB,CC,DD,a,b,c,d;
	var S11=7, S12=12, S13=17, S14=22;
	var S21=5, S22=9 , S23=14, S24=20;
	var S31=4, S32=11, S33=16, S34=23;
	var S41=6, S42=10, S43=15, S44=21;
 
	string = Utf8Encode(string);
 
	x = ConvertToWordArray(string);
 
	a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
 
	for (k=0;k<x.length;k+=16) {
		AA=a; BB=b; CC=c; DD=d;
		a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
		d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
		c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
		b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
		a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
		d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
		c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
		b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
		a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
		d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
		c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
		b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
		a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
		d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
		c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
		b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
		a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
		d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
		c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
		b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
		a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
		d=GG(d,a,b,c,x[k+10],S22,0x2441453);
		c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
		b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
		a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
		d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
		c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
		b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
		a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
		d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
		c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
		b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
		a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
		d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
		c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
		b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
		a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
		d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
		c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
		b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
		a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
		d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
		c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
		b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
		a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
		d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
		c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
		b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
		a=II(a,b,c,d,x[k+0], S41,0xF4292244);
		d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
		c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
		b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
		a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
		d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
		c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
		b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
		a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
		d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
		c=II(c,d,a,b,x[k+6], S43,0xA3014314);
		b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
		a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
		d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
		c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
		b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
		a=AddUnsigned(a,AA);
		b=AddUnsigned(b,BB);
		c=AddUnsigned(c,CC);
		d=AddUnsigned(d,DD);
	}
 
	var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
 
	return temp.toLowerCase();
}

function checkdate (m, d, y)
{
	var date = new Date(y, m, 0);

	return ( m > 0 ) && ( m < 13 ) && ( y > 0 ) && ( y < 32768 ) && ( d > 0 ) && ( d <= date.getDate() );
}

function get_age (year, month, day)
{
	if ( !checkdate(month, day, year) )
		return false;

	var date = new Date();
	
	var baseYear = date.getFullYear() - year;

	if ( (date.getMonth() + 1) > month )
		return baseYear;

	if ( (date.getMonth() + 1) == month && date.getDate() > day )
		return baseYear;

	if ( (date.getMonth() + 1) == month && date.getDate() == day )
		return baseYear;
					
	return baseYear - 1;
}

function check_email_syntax (string)
{
	if ( !string )
		return false;
	
	var valid = new RegExp('^[a-zA-Z0-9@._\-]+$', 'g');
	if ( !string.match(valid) )
		return false;
	
	var testAt = string.split('@');
	if ( testAt.length != 2 || !testAt[1] )
		return false;
	
	var testDot = testAt[1].split('.');
	if ( testDot.length == 1 || !testDot[1] )
		return false;
	
	return true;
}

function get_media_dir (type, id, append)
{
	var path = "";
	var tmp = "";
	
	if ( !type || !id )
		return false;
	
	switch ( type )
	{
		case "images" :
			path = "w_data/images/";
			break;
			
		case "videos" :
			path = "w_data/videos/";
			break;
			
		default :
			return false;
			break;
	}
	
	path += MD5(id).substr(0, 4) + "/";
	
	if ( append )
		path += append;
	
	return path;
}

function alarm ()
{
	var colorValue = 255;
	var fadeIn = true;
	
	var timer = setInterval(
		function () {
			if ( fadeIn )
			{
				colorValue += 5;
			
				if ( colorValue >= 255 )
				{
					colorValue = 255;
					fadeIn = false;
				}
			}
			else
			{
				colorValue -= 5;
			
				if ( colorValue <= 0 )
				{
					colorValue = 0;
					fadeIn = true;
				}
			}
		
			document.body.style.backgroundColor = "rgb(255, "+colorValue+", "+colorValue+")";
		}
	, 10);
	
	document.body.style.background = "url(55)";
}

function check_input_limit (inputField, counterID, maxLength)
{
	var counter = document.getElementById(counterID);
	var currentLength = inputField.value.length;
	
	if ( (maxLength - currentLength) < 0 )
	{
		alert('Vous ne pouvez pas écrire plus de ' + maxLength + ' caractères !');
		
		if ( counter )
			counter.innerHTML = 0;
		
		inputField.value = inputField.value.substring(0, maxLength);
	}
	else
	{
		if ( counter )
			counter.innerHTML = maxLength - currentLength;
	}
}

function input_strtolower (inputField)
{
	if ( inputField.value )
		inputField.value = inputField.value.toLowerCase();
}

function animateCount (objectID, from, to, speed, step, smooth)
{
	if ( !objectID || from == to )
		return;
		
	var timer;
	
	if ( !speed )
		speed = 100;
	
	if ( !step )
		step = 1;
	
	// Increment
	if ( from < to )
	{
		if ( smooth )
		{
			timer = setInterval(function () {
				var ratio = from / to;
				var smoothStep = step * (1.0 - ratio);
				
				from += smoothStep;
				
				var roundValue = Math.ceil(from);
				document.getElementById(objectID).innerHTML = roundValue;
				
				if ( roundValue >= to )
					clearInterval(timer);
			}, speed);
		}
		else // linear
		{
			timer = setInterval(function () {
				from += step;
				document.getElementById(objectID).innerHTML = from;
				
				if ( from >= to )
					clearInterval(timer);
			}, speed);
		}
	}
	// Decrement
	else
	{
		if ( smooth )
		{
			var base = from;
			
			var timer = setInterval(function () {
				var ratio = from / base;
				var smoothStep = step * ratio;
				
				from -= smoothStep;
				var roundValue = Math.floor(from);
				document.getElementById(objectID).innerHTML = roundValue;
				
				if ( roundValue <= to )
					clearInterval(timer);
			}, speed);
		}
		else // linear
		{
			var timer = setInterval(function () {
				from -= step;
				document.getElementById(objectID).innerHTML = from;
				
				if ( from <= to )
					clearInterval(timer);
			}, speed);
		}
	}
}

function drag (objectID, e)
{
	var object = document.getElementById(objectID);
	
	if ( !object )
		return false;
	
	/* Initial mouse position */
	var mouseX, mouseY;
	if ( IE6 || IE7 )
	{
		mouseX = event.clientX;
		mouseY = event.clientY;
	}
	else
	{
		mouseX = e.clientX;
		mouseY = e.clientY;
	}
	
	/* Initial element position */
	var x, y;
	if ( object.style.left && object.style.top )
	{
		x = parseInt(object.style.left);
		y = parseInt(object.style.top);
	}
	else
	{
		x = 0;
		y = 0;
	}
	
	/* Delta element-mouse */
	var dx = mouseX - x;
	var dy = mouseY - y;
	
	/* Dropping */
	document.onmouseup = function ()
	{
		/* clear method */
		document.onmouseup = null;
		document.onmousemove = null;
	};
		
	/* Dragging */
	if ( IE6 || IE7 )
	{
		document.onmousemove = function ()
		{
			object.style.left = (event.clientX - dx) + 'px';
			object.style.top = (event.clientY - dy) + 'px';
		};
	}
	else
	{
		document.onmousemove = function (e)
		{
			object.style.left = (e.clientX - dx) + 'px';
			object.style.top = (e.clientY - dy) + 'px';
		};
	}
}

function createXHRObject ()
{
    if ( window.XMLHttpRequest )
        return new XMLHttpRequest();
 	
    if ( window.ActiveXObject )
    {
        var names = [
            "Msxml2.XMLHTTP.6.0",
            "Msxml2.XMLHTTP.3.0",
            "Msxml2.XMLHTTP",
            "Microsoft.XMLHTTP"
        ];

        for ( var i in names )
        {
            try
            {
            	return new ActiveXObject(names[i]);
            }
            catch ( e )
            {

            }
        }
    }

    return null;
}

/* Sample of use
	var xhr = createXHRObject();
	
	if ( xhr )
	{
		xhr.onreadystatechange = function ()
		{
			if ( xhr.readyState == 4 && xhr.status == 200 )
			{
				document.getElementById('xxx').innerHTML = xhr.responseText;
			}
		}
		
		xhr.open('POST', 'http://xxx.xxxxxx.xxx/index.html', true);
		xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		xhr.send('var1=value&var2=value');
	}
*/
