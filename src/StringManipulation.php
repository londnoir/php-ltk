<?php

	/*
	* Libraries/php/LTK/StringManipulation.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/* NOTE: workaround en php. */
	if ( !function_exists('mb_str_pad') )
	{
		/**
		 * Complète une chaîne jusqu'à une taille donnée.
		 *
		 * @param string $input La chaîne d'entrée.
		 * @param int $pad_length Si la valeur de pad_length est négative, plus petite que, ou égale à la taille courante de la chaîne input, input est retournée inchangée et input sera retourné.
		 * @param string $pad_string Le paramètre pad_string peut être tronqué si le nombre de caractères de complétion n'est pas multiple de la taille de pad_string.
		 * @param int $pad_type L'argument optionnel pad_type peut être l'une des constantes suivantes : STR_PAD_RIGHT, STR_PAD_LEFT, ou STR_PAD_BOTH. Si pad_type n'est pas spécifié, il prend la valeur par défaut de STR_PAD_RIGHT.
		 * @param string $encoding Le paramètre encoding est l'encodage des caractères. S'il est omis, l'encodage de caractères interne sera utilisé.
		 * @return string
		 */
		function mb_str_pad (string $input, int $pad_length, string $pad_string = ' ', int $pad_type = STR_PAD_RIGHT, string $encoding = ''): string
		{
			return str_pad($input, strlen($input) - mb_strlen($input, $encoding ?: mb_internal_encoding()) + $pad_length, $pad_string, $pad_type);
		}
	}

	/**
	 * Cette classe contient les outils pour manipuler les chaînes de caractères.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class StringManipulation
	{
		/** @brief Mode d'encodage "Base 64". */
		const Base64 = 0;
		/** @brief Mode d'encodage "Quoted printable". */
		const QEncoding = 1;

		/** @internal */
		private function __construct () {}

		/**
		 * Décode une chaîne de caractères de type "a=1&b=2&c[]=3&c[]=4" et retourne le résultat dans un tableau associatif.
		 *
		 * @param string $query La chaîne à décoder, provenant par exemple de $_SERVER['QUERY_STRING'].
		 * @param bool $silent Retire les messages d'erreur.
		 * @return array
		 */
		static public function parseQuery (string $query, bool $silent = false) : array
		{
			$output = [];

			$variables = explode('&', $query);

			foreach ( $variables as $variable )
			{
				/* No extra '&' */
				if ( empty($variable) )
				{
					if ( !$silent )
						trigger_error('Empty variable caused by an extra "&" !', E_USER_WARNING);

					continue;
				}

				list($name, $value) = explode('=', $variable, 2);

				/* No unamed variable. */
				if ( empty($name) )
				{
					if ( !$silent )
						trigger_error('Unamed variable in the query !', E_USER_WARNING);

					continue;
				}

				/* Array variable */
				if ( str_contains($name, '[]') )
				{
					/* Clean key */
					$name = str_replace('[]', null, $name);

					if ( array_key_exists($name, $output) )
					{
						if ( is_array($output[$name]) )
						{
							$output[$name][] = $value;

							continue;
						}
						
						if ( !$silent )
							trigger_error('Variable "'.$name.'" was not an array and is overwritten as it appears more than one time in the query !', E_USER_WARNING);
					}

					$output[$name] = [$value];

					continue;
				}

				/* Simple variable */
				if ( !$silent && array_key_exists($name, $output) )
					trigger_error('Variable "'.$name.'" is overwritten as it appears more than one time in the query !', E_USER_WARNING);

				$output[$name] = $value;
			}

			return $output;
		}

		/**
		* Encode une chaîne "Multipurpose Internet Mail Extensions" suivant la norme RFC2047
		*
		* @param string $string Une chaîne à encoder.
		* @param int $mode Le mode d'encodage à utiliser. Doit être une constante de la classe, EncodedString::Base64 ou EncodedString::QEncoding.
		* @param string $charset Le jeu de caractère du texte source. Par défaut, php tentera de le déterminer via mb_detect_encoding().
		* @return string La chaîne encodée ou ''.
		*/
		static public function encodeToRFC2047 (string $string, $mode, string $charset = ''): string
		{
			if ( empty($charset) )
				$charset = mb_detect_encoding($string);

			switch ( $mode )
			{
				case self::Base64 :
					return '=?'.$charset.'?B?'.base64_encode($string).'?=';

				case self::QEncoding :
					return '=?'.$charset.'?Q?'.quoted_printable_encode($string).'?=';

				default:
					return '';
			}
		}

		/**
		 * Décode une chaîne "Multipurpose Internet Mail Extensions"
		 *
		 * @param string $string Une chaîne à décoder.
		 * @return string
		 */
		static public function decodeFromRFC2047 (string $string): string
		{
			$string = trim($string);
			
			if ( $string[0] != '=' || $string[strlen($string) - 1] != '=' )
				return '';

			$chunks = explode('?', $string, 4);

			if ( count($chunks) !== 4 )
				return '';

			$charset = $chunks[1];
			$encoding = $chunks[2];
			$content = substr($chunks[3], 0, -2);

			unset($chunks);

			switch ( $encoding )
			{
				case 'B' :
				case 'b' :
					$buffer = base64_decode($content);
					break;

				case 'Q' :
				case 'q' :
					$buffer = quoted_printable_decode($content);
					break;

				default :
					return '';
			}

			/* FIXME : Weak */
			if ( $charset != 'utf-8' && $charset != 'UTF-8' )
				$buffer = utf8_encode($buffer);
			
			return $buffer;
		}

		/** 
	 	 * Convertit une chaîne de caractères ASCII en valeur binaire (8).
		 *
		 * @param string $string La chaîne à convertir.
		 * @return string
		 */
		static public function encodeToBinary (string $string): string
		{
			$binaries = '';

			$chars = str_split($string);

			foreach ( $chars as $char )
			{
				$decimal = ord($char);

				$binary = strval(decbin($decimal));

				$binaries .= str_pad($binary, 8, '0', STR_PAD_LEFT);
			}

			return $binaries;
		}

		/** 
		 * Convertit une chaîne de caractères binaires en chaîne ASCII.
		 * NOTE: La chaîne en entrée ne doit contenir que des 0 et des 1, ainsi qu'être divisible par 8.
		 *
		 * @param string $binaries La chaîne à convertir.
		 * @return string
		 */
		static public function decodeFromBinary (string $binaries): string
		{
			if ( preg_match('#^([01]+)$#', $binaries) == false )
			{
				trigger_error(__METHOD__.', input is not binary !', E_USER_WARNING);

				return '';
			}

			if ( strlen($binaries) % 8 > 0 )
			{
				trigger_error(__METHOD__.', input is not divisible by 8 !', E_USER_WARNING);

				return '';
			}

			$string = '';

			for ( $i = 0; $i < strlen($binaries); $i += 8 )
			{
				$binary = substr($binaries, $i, 8);

				$decimal = bindec($binary);

				$string .= chr($decimal);
			}

			return $string;
		}

		/**
		 * Convertit un texte pour pouvoir être affiché dans les attributs de balises html.
		 *
		 * @param string $plainText Le texte à vérifier.
		 * @param string $charset La norme d'encodage. Par défaut, 'UTF-8';
		 * @return string
		 */
		static public function sanitizeForHTMLAttribute (string $plainText, string $charset = 'UTF-8'): string
		{
			$plainText = htmlspecialchars($plainText, ENT_QUOTES, $charset);
			return preg_replace('#([\n\r\t]+)#', null, $plainText);
		}

		/**
		 * Raccourci un texte.
		 *
		 * @param string $buffer Une chaîne de caractères à raccourcir.
		 * @param int $limit Indique le nombre de caractères que le texte ne doit pas dépasser. Par défaut, la limit est fixée à 50.
		 * @param string $charset La norme d'encodage. Par défaut, 'UTF-8';
		 * @return string
		 */
		static public function summarize (string $buffer, int $limit = 50, string $charset = 'UTF-8'): string
		{
			$buffer = html_entity_decode($buffer, ENT_QUOTES, $charset);

			if ( strlen($buffer) > $limit )
			{
				$buffer = mb_substr($buffer, 0, $limit);

				$buffer .= '...';
			}

			return htmlspecialchars($buffer, ENT_COMPAT, $charset);
		}

		/**
		 * Met en majuscule la première lettre de chaque mot d'une chaîne de caractères.
		 *
		 * @param string $string Une chaîne de caractères à traiter.
		 * @param bool $onlyFirstChar Un booléen qui permet de s'assurer que seul le premier caractère d'un mot est en majuscule. Par défaut, false.
		 * @param array $avoidWords Une tableau de chaînes de caractères contenant les mots à ne pas mettre en majuscule.
		 * @return string
		 */
		static public function stringUcfirst (string $string, bool $onlyFirstChar = false, array $avoidWords = []): string
		{
			if ( $onlyFirstChar )
				$string = strtolower($string);

			$words = explode(' ', $string);

			$separators = array('\'', '-');
			
			foreach ( $words as $key => $word )
			{
				/* Check for sub separators. */
				$hasSeparators = false;

				foreach ( $separators as $separator )
				{
					if ( strstr($word, $separator) !== false )
					{
						$hasSeparators = true;

						$subWords = explode($separator, $word);

						foreach ( $subWords as $subKey => $subWord )
						{
							/* Check if the sub word can be processed. */
							if ( in_array($subWord, $avoidWords) )
								continue;

							$subWords[$subKey] = ucfirst($subWord);
						}

						/* Recompose the word for the new separator. */
						$word = implode($separator, $subWords);	
					}
				}

				/* If sub separators has been detected, the word is processed. */
				if ( $hasSeparators )
				{
					$words[$key] = $word;

					continue;
				}

				/* Check if the word can be processed. */
				if ( in_array($word, $avoidWords) )
					continue;

				$words[$key] = ucfirst($word);
			}
			
			return implode(' ', $words);
		}
	};
