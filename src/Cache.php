<?php

	/*
	* Libraries/php/LTK/Cache.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	/**
	* Système de cache simple.
	* Ce système fonctionne sur des blocs de textes à restituer ou des tableaux de valeur en php.
	*
	* Exemple d'utilisation avec un tableau de données :\n
	* <pre>
	*	$cache = new LTK\Cache('array-test', true);
	*	
	*	if ( $test = $cache->check(5) )
	*	{
	*		var_dump($test);
	*	}
	*	else
	*	{
	*		$test = array(
	*			0 => 'no',
	*			1 => 'yes'
	*		);
	*		
	*		if ( $cache->save($test) )
	*			echo 'Cache saved !';
	*	}
	* </pre>
	* Cet objet est configurable avec StaticSettings et les clés suivantes : CachePathKey and CacheEnabledKey.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class Cache
	{
		private string $filePath = '';
		private bool $isVarsContent = false;

		const CachePathKey = 'cache_path';
		const CacheEnabledKey = 'cache_enabled';

		const DefaultCacheDirectoryName = 'php-ltk.cache_dir';
		
		/** 
		* Le constructeur par défaut. 
		*
		* @param string $cacheName Une chaîne de caractères pour nommer le cache.
		* @param bool $isVarsContent Un booléen pour indique s'il s'agit ou non de variable PHP. Par défaut, le cache est considéré comme du simple texte.
		*/
		public function __construct (string $cacheName, bool $isVarsContent = false)
		{
			$cachePath = self::cachePath();
			
			if ( Path::build($cachePath) )
			{
				$this->filePath = $cachePath.$cacheName.'.cache';
				$this->isVarsContent = (bool)$isVarsContent;
			}
			else
			{
				trigger_error(__METHOD__.'(), "'.$cachePath.'" repertory is not available for writing !', E_USER_ERROR);
			}
		}

		static private function cachePath (): string
		{
			return StaticSettings::instance()->get(self::CachePathKey, Path::getTemporaryDirectory(self::DefaultCacheDirectoryName));
		}
		
		/** 
		* Cette méthode vérifie que le cache est toujours valide.
		*
		* @param int $lifetime Un nombre entier pour donner la durée de vie en seconde autorisée du cache.
		* @return mixed Une chaine de caractères ou un tableau dans le cas d'un cache valide, sinon false.
		*/
		public function check (int $lifetime): mixed
		{
			if ( !StaticSettings::instance()->get(self::CacheEnabledKey, true) )
				return false;

			if ( file_exists($this->filePath) && filemtime($this->filePath) >= (time() - $lifetime) )
			{
				/* Get the file content. */
				$fileContent = file_get_contents($this->filePath);
			
				/* Unserialize PHP vars if requested. */
				if ( $this->isVarsContent )
					$fileContent = unserialize($fileContent);
			
				return $fileContent;
			}
			
			return false;
		}

		/** 
		* Sauvegarde un tampon de données dans le cache.
		*
		* @param mixed $cacheContent Une valeur quelconque.
		* @return bool Un booléen où vrai indique que le cache est sauvegardé.
		*/
		public function save (mixed $cacheContent): bool
		{
			if ( !StaticSettings::instance()->get(self::CacheEnabledKey, true) )
				return true;
			
			/* Avoid to cache empty content. */
			if ( empty($cacheContent) )
				return false;
			
			/* Serialize PHP vars if requested. */
			if ( $this->isVarsContent )
				$cacheContent = serialize($cacheContent);
			
			if ( !file_put_contents($this->filePath, $cacheContent) )
			{
				trigger_error(__METHOD__.'(), "'.$this->filePath.'" file cannot be modified !', E_USER_WARNING);
			
				return false;
			}
					
			return true;
		}
		
		/** 
		* Vide entièrement le répertoire de cache.
		*
		* @return bool Un booléen où vrai indique qu'il n'y a pas plus rien dans le cache.
		*/
		static public function cleanFolder (): bool
		{
			$cachePath = self::cachePath();

			/* Check repertory. */
			if ( !file_exists($cachePath) || !is_dir($cachePath) )
				return true;
			
			/* NOTE: all cache file have .cache extension ! */
			if ( $tmpFiles = glob($cachePath.'*.cache') )
			{
				$total = count($tmpFiles);
				$done = 0;
			
				foreach ( $tmpFiles as $tmpFile )
				{
					if ( is_writable($tmpFile) )
					{
						if ( unlink($tmpFile) )
							$done++;
					}
					else
					{
						trigger_error(__METHOD__.'(), "'.$tmpFile.'" file cannot be deleted !', E_USER_WARNING);
					}
				}
			
				if ( !$done )
				{
					trigger_error(__METHOD__.'(), no file can be deleted !', E_USER_WARNING);
					
					return false;
				}
				elseif ( $done < $total )
				{
					trigger_error(__METHOD__.'(), '.( $total - $done ).' file(s) still present in the cache repertory !', E_USER_WARNING);
					
					return false;
				}
			}
			
			return true;
		}
	}
	