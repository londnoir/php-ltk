<!--
	index.php
	This file is part of PHP-LTK

	Copyright (C) 2021 - LondNoir <londnoir@gmail.com>

	PHP-LTK is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	PHP-LTK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ooops !</title>

    <style>
        body {
            background: #112542;
        }

        div.quote {
            margin: 32px;
            padding: 32px;
            box-shadow: 5px 5px 15px 5px #0A192E;
            color: white;
        }

        p.line {
            margin:0 0 12px 0;
            padding:0;
            font-family: serif;
            font-size: 2.5em;
            font-style: italic;
        }

        a {
            color: white;
        }

        p.footer {
            margin:64px;
            text-align: right;
            color: lightgrey;
            font-size: 0.75em;
        }
    </style>
</head>
<body>
<div class="quote">
    <p class="line">
        <b>Alice</b> : Where is the way ?
    </p>
    <p class="line">
        <b>Cheshire Cat</b> : It may be by
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?? 'http://' . $_SERVER['HTTP_HOST']; ?>" title="Go away !">here ...</a> or
        <a href="/" title="Go away too !">there ...<a>
    </p>
</div>
<p class="footer">PHP-LTK navigation failure page.<br />If you see this page, don't panic, you probably hit a wrong link on the website. It will be fixed.</p>
</body>
</html>
