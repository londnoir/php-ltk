<?php

	/*
	* Libraries/php/LTK/DatabaseSettings.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	use PDO;

	/**
	* Settings class implementation using the database with a PDO driver.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	final class DatabaseSettings extends AbstractSettings
	{
		const TableNameKey = 'database_settings_table_name';
		const DefaultTableName = 'ltk_settings';

		private Database $database;
		private string $table;
		private int $limit = 1000;
		private array $changedKeys = [];

		/**
		* Constructeur. Lis les paramètres dans la base de données.
		*
		* @param Database $database La base de données contenant les paramètres.
		* @param string $table Le nom de la table comportant les données. Par défaut "ltk_settings"
		*/
		public function __construct (Database $database, string $table = self::DefaultTableName)
		{
			$this->database = $database;
			$this->table = $table;

			$query = 
				'SELECT `id`, `name`, `value`, `is_read_only` '.
				'FROM `'.$this->table.'` '.
				'LIMIT '.$this->limit.';';

			if ( $rows = $this->database->getArray($query) )
				foreach ( $rows as $row )
					parent::set($row['name'], $row['value'], $row['is_read_only'] ? true : false);
		}

		/**
		* Destructeur. Sauvegarde les paramètres dans la base de données.
		*/
		public function __destruct ()
		{
			foreach ( $this->changedKeys as $key )
			{
				$value = $this->get($key);
				$isReadOnly = $this->isReadOnly($key) ? 1 : 0;

				$query = Database::writeInsertOrUpdateQuery($this->table, [
					'name' => $key,
					'value' => $value,
					'is_read_only' => $isReadOnly
				], [
					'value' => $value,
					'is_read_only' => $isReadOnly
				]);

				echo $query."\n";

				$this->database->execute($query);
			}
		}

		/**
		* Enregistre une variable.
		*
		* @param string $key Le nom de la variable.
		* @param mixed $value La valeur de la variable.
		* @param bool $readOnly Une fois enregistré, la valeur est assurée immuable. Par défaut, false.
		* @return bool. True si la variable est bien enregistrée.
		*/
		public function set (string $key, mixed $value, bool $readOnly = false): bool
		{
			if ( parent::set($key, $value, $readOnly) )
			{
				$this->changedKeys[] = $key;

				return true;
			}

			return false;
		}

		/**
		 * Permet de construire d'instancier un objet DatabaseSettings depuis un tableau de données.
		 * Simple raccourcit d'instanciation et de l'utilisateur de la méthode DatabaseSettings::setArray().
		 *
		 * @param Database $database A reference to a database.
		 * @param array $data Un tableau associatif dont les clés seront les noms des variables, les valeurs seront les valeurs des variables.
		 * @return DatabaseSettings.
		 */
		static public function build (Database $database, array $data): DatabaseSettings
		{
			$settings = new DatabaseSettings($database);
			$settings->setArray($data);
			
			return $settings;
		}

		/**
		 * Une méthode statique pour créer la table dans la base de données ou afficher la requête équivalente.
		 *
		 * @param PDO|null $database Un objet PDO. Par défaut à null.
		 * @param AbstractSettings|null $settings Un objet Settings. Par défaut à null.
		 * @return bool|string
		 */
		public static function buildTable (PDO $database = null, AbstractSettings $settings = null): bool|string
		{
			if ( is_null($settings) )
				$settings = StaticSettings::instance();

			$tableName = $settings->get(self::TableNameKey, self::DefaultTableName);

			$statement = 
				'CREATE TABLE IF NOT EXISTS `' . $tableName . '` '.
				'('.
				'`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, '.
				'`name` VARCHAR(255) NOT NULL, '.
				'`value` VARCHAR(32) NOT NULL, '.
				'`is_read_only` TINYINT(1) UNSIGNED NOT NULL DEFAULT "0", '.
				'PRIMARY KEY (`id`), '.
				'UNIQUE KEY `name` (`name`)'.
				') '.
				'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';

			return is_null($database) ? $statement : ( $database->exec($statement) !== false );
		}
	}
