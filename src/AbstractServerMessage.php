<?php

	/*
	* Libraries/php/LTK/AbstractServerMessage.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	* Classe abstraite contenant les données communes aux classes ServerAnswer et ServerResponse.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/	
	abstract class AbstractServerMessage
	{
		/** Code standard signifiant aucune erreur. */
		const Success = 0;
		/** Code standard signifiant qu'une erreur s'est produite. */
		const Failed = 1;
		/** Code standard signifiant que la réponse est JSON est invalide. */
		const Invalid = 65534;
		/** Code standard signifiant que la réponse n'est pas initialisée. */
		const Undefined = 65535;

		protected int $code = self::Undefined;
		protected string $message = '';
		protected array $data = [];
	}
	