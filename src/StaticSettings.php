<?php

	/*
	* Libraries/php/LTK/StaticSettings.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Settings class implementation using volative data.
	 * This version uses a singleton approach in regard of Settings class.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class StaticSettings extends AbstractSettings
	{
		static private ?StaticSettings $pointer = null;

		/** @internal Constructeur privé. Cette classe est singleton. */
		private function __construct () {}

		/**
		 * Retourne l'instance unique de cette classe ou l'instancie pour la première fois.
		 *
		 * @return StaticSettings
		 */
		static public function instance (): StaticSettings
		{
			if ( is_null(self::$pointer) )
				self::$pointer = new StaticSettings();

			return self::$pointer;
		}
	}
