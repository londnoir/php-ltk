<?php

	/*
	* Libraries/php/LTK/AbstractSettings.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	/**
	* Interface to all type of Settings classes.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	abstract class AbstractSettings
	{
		private array $data = [];
		private array $isReadOnly = [];
		
		/**
		* Enregistre une variable.
		*
		* @param string $key Le nom de la variable.
		* @param mixed $value La valeur de la variable.
		* @param bool $readOnly Une fois enregistré, la valeur est assurée immuable. Par défaut, false.
		* @return bool
		*/
		public function set (string $key, mixed $value, bool $readOnly = false): bool
		{
			if ( $this->isReadOnly($key) )
			{
				trigger_error(__METHOD__.', key "'.$key.'" already exists and is read-only !', E_USER_WARNING);

				return false;	
			}

			$this->data[$key] = $value;
			$this->isReadOnly[$key] = (bool)$readOnly;

			return true;
		}

		/**
		* Enregistre une série de variable via un tableau associatif.
		*
		* @param array $data Un tableau associatif dont les clés seront les noms des variables, les valeurs seront les valeurs des variables.
		* @param bool $readOnly Une fois enregistré, les valeurs sont assurées immuables. Par défaut, false.
		* @return bool
		*/
		public function setArray (array $data, bool $readOnly = false): bool
		{
			foreach ( $data as $key => $value )
				if ( !$this->set($key, $value, $readOnly) )
					return false;

			return true;
		}
		
		/**
		* Retourne une variable ou une valeur par défaut.
		*
		* @param string $key Le nom de la variable.
		* @param mixed|null $default La valeur par défaut de la variable.
		* @return mixed
		*/
		public function get (string $key, mixed $default = null): mixed
		{
			if ( array_key_exists($key, $this->data) )
				return $this->data[$key];

			return $default;
		}

		/**
		* Retourne l'entierté des variables disponibles dans un tableau associatif.
		*
		* @return array
		*/
		public function asArray (): array
		{
			return $this->data;
		}

		/**
		* Permet de savoir si une variable est en lecture seule.
		*
		* @param string $key Le nom de la variable.
		* @return bool
		*/
		public function isReadOnly (string $key): bool
		{
			return ( array_key_exists($key, $this->data) && $this->isReadOnly[$key] );
		}

		/**
		* Affiche une liste des variables présentes.
		*/
		public function print ()
		{
			foreach ( $this->data as $key => $value )
			{
				echo $key.' => '.$value.( $this->isReadOnly($key) ? ' [Read-Only]' : null )."\n";
			}
		}
	};
