<?php

	namespace LTK;

	class Algorithm
	{
		/** @internal */
		private function __construct () {}

		/**
		 * Checks if any item of an array validates the condition.
		 *
		 * @param array $collection The array of item to check with.
		 * @param callable $condition The callable condition with two parameters. callable(item, index).
		 * @return bool
		 */
		public static function anyOf (array $collection, callable $condition): bool
		{
			foreach ( $collection as $index => $item )
				if ( $condition($item, $index) )
					return true;

			return false;
		}

		/**
		 * Checks if all items of an array validates the condition.
		 *
		 * @param array $collection The array of item to check with.
		 * @param callable $condition The callable condition with two parameters. callable(item, index).
		 * @return bool
		 */
		public static function allOf (array $collection, callable $condition): bool
		{
			foreach ( $collection as $index => $item )
				if ( !$condition($item, $index) )
					return false;

			return true;
		}

		/**
		 * Checks if no item at all of an array validates the condition.
		 *
		 * @param array $collection The array of item to check with.
		 * @param callable $condition The callable condition with two parameters. callable(item, index).
		 * @return bool
		 */
		public static function noneOf (array $collection, callable $condition): bool
		{
			foreach ( $collection as $index => $item )
				if ( $condition($item, $index) )
					return false;

			return true;
		}

		/**
		 * Checks if only one item of an array validates the condition.
		 *
		 * @param array $collection The array of item to check with.
		 * @param callable $condition The callable condition with two parameters. callable(item, index).
		 * @return bool
		 */
		public static function oneOf (array $collection, callable $condition): bool
		{
			$count = 0;

			foreach ( $collection as $index => $item )
			{
				if ( $condition($item, $index) )
				{
					$count++;

					if ( $count > 1 )
						return false;
				}
			}

			return $count === 1;
		}

		/**
		 * De-duplicates values name with a number ({VALUE} #x) in an array.
		 *
		 * @param array $array A reference to an array.
		 * @return array
		 */
		public static function deduplicateValues (array $array): array
		{
			/* 1. Scan the input array in two passes to get an array of unique duplicated values. */
			$duplicatedValues = [];

			for ( $i = 0; $i < count($array); $i++ )
			{
				for ( $j = $i + 1; $j < count($array); $j++ )
				{
					if ( $array[$i] !== $array[$j] )
						continue;
					
					if ( !in_array($array[$i], $duplicatedValues) )
						$duplicatedValues[] = $array[$i];
				}
			}

			/* 2. Rebuild the array with occurence number added at the end of duplicated values. */
			foreach ( $duplicatedValues as $duplicatedValue )
			{
				$occurrence = 0;

				foreach ( $array as $key => $value )
				{
					if ( $value !== $duplicatedValue )
						continue;
					
					$array[$key] = $value . ' #' . $occurrence++;
				}
			}

			return $array;
		}

		/**
		 * Performs a check on algorithm.
		 */
		public static function _test ()
		{
			$tests = [
				[0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0],
				[0, 0, 0, 1, 0, 0, 1],
				[1, 1, 1, 1]
			];

			$predicate = function ($item) {
				return $item == 1;
			};

			foreach ( $tests as $test )
			{
				echo 
					print_r($test, true) . PHP_EOL .
					'Any of is 1 : ' . ( self::anyOf($test, $predicate) ? 'Yes' : 'No' ) . PHP_EOL .
					'All of are 1 : ' . ( self::allOf($test, $predicate) ? 'Yes' : 'No' ) . PHP_EOL .
					'None of is 1 : ' . ( self::noneOf($test, $predicate) ? 'Yes' : 'No' ) . PHP_EOL . 
					'One of is 1 : ' . ( self::oneOf($test, $predicate) ? 'Yes' : 'No' ) . PHP_EOL . 
					PHP_EOL;
			}
		}
	}