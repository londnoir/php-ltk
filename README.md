# PHP-LTK Revision 3

Stand for php Light Tools Kit.

This is a bunch of useful classes for common operations.

# Example of use :

```

    require_once 'php-ltk/autoload.php';

    use LTK\{
      Database,
      Settings,
    };

    $database = new Database(Settings::build([
      Database::HostnameKey => 'localhost',
      Database::DatabaseNameKey => 'dummy',
      Database::UsernameKey => 'dummyuser',
      Database::PasswordKey => 'dummypassword'
    ]));

    if ( $database->isConnected() )
    {
      echo 'Connexion up !'."\n";

    }
    else
    {
      echo 'No connexion !'."\n";
    }

```
