/*global unescape*/

/*
* Libraries/js/cookie.js
* This file is part of PHP-LTK 
*
* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
*
* PHP-LTK is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* PHP-LTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
    
function get_cookie_value (offset)
{
	"use strict";

	var endstr = document.cookie.indexOf (";", offset);
	
	if ( endstr === -1 )
	{
		endstr = document.cookie.length;
	}
	
	return unescape(document.cookie.substring(offset, endstr));
}

function get_cookie (name)
{
	"use strict";

	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	var j;
	
	while ( i < clen )
	{
		j = i + alen;
		
		if ( document.cookie.substring(i, j) === arg )
		{
			return get_cookie_value (j);
		}
		
		i = document.cookie.indexOf(" ", i) + 1;
		
		if ( i === 0 )
		{
			break;
		}
	}
	
	return null;
}

function set_cookie (name, value, expires, path, domain, secure)
{
	"use strict";

	document.cookie = 
		name + "=" + escape (value) + 
		((expires) ? "; expires=" + expires.toGMTString() : "") + 
		((path) ? "; path=" + path : "") + 
		((domain) ? "; domain=" + domain : "") + 
		((secure) ? "; secure" : "");
}

function delete_cookie (name, path, domain)
{
	"use strict";

	if ( get_cookie(name) )
	{
		document.cookie = 
			name + "=" +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "") +
			"; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}
