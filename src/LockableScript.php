<?php

	/*
	* Libraries/php/LTK/AbstractLockableScript.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Trait permettant d'implémenter une logique de verrou exclusif sur un script.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	trait LockableScript
	{
		private string $lockName = '';
		private bool $abortedExecution = false;

		/**
		 * Permet d'obtenir le verrouillage du script. Si le verrou n'est pas disponible (exécution en cours), le script s'arrête.
		 *
		 * @param int $time Le nombre de secondes que le verrou est valide. Permet de nettoyer une verrou obsolète si le script a crashé.
		 */
		protected function lock (int $time = 3600)
		{
			/* Création d'un nom de fichier unique dans les répertoires temporaires. */
			$this->lockName = sys_get_temp_dir().DIRECTORY_SEPARATOR.'php-ltk_'.md5($_SERVER['SCRIPT_FILENAME']).'_lock';

			/* NOTE: Opération atomique. 
			 * Le @ est important pour taire le warning s'il n'arrive 
			 * pas écrire le répertoire, ce que l'on veut. */
			if ( !@mkdir($this->lockName) )
			{
				/* Vérifie qu'on a pas un vieux lock pourri à cause d'un crash de script. */
				if ( filemtime($this->lockName) > (time() - $time) )
				{
					/* NOTE: On ne veut évidemment pas que le destructeur 
					 * de cette instance débloque la situation. */
					$this->abortedExecution = true;

					trigger_error(__METHOD__.'(), unable to get the lock for execution !', E_USER_WARNING);

					exit(1);
				}

				/* On ravive la date. */
				touch($this->lockName);
			}
		}

		/**
		 * Permet de déverouiller proprement le script.
		 */
		protected function unlock ()
		{
			/* Cette instance n'a pas pu s'exécuter, 
			 * donc on ne retire pas le lock. */
			if ( $this->abortedExecution )
				return;

			/* Déblocage automatique du script. */
			if ( file_exists($this->lockName) )
				rmdir($this->lockName);
		}
	}
