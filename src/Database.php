<?php

	/*
	* Libraries/php/LTK/Database.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;

	use Exception;
	use PDO;

	/**
	* Class responsible for connecting a database extening PDO class from PHP with some extras methods to build fast query.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	final class Database extends PDO
	{
		const HostnameKey = 'database_hostname';
		const DatabaseNameKey = 'database_name';
		const UsernameKey = 'database_username';
		const PasswordKey = 'database_password';
		const ConnexionCharsetKey = 'database_connexion_charset';

		const StringType = 'string';
		const IntegerType = 'int';
		const FloatType = 'float';

		private ?AbstractSettings $settings = null;
		private string $hostname = 'localhost';
		private string $databaseName = '';
		private string $username = '';
		private string $password = '';
		private string $databaseType = 'mysql';
		private string $connexionCharset = 'utf8mb4';

		private bool $isConnected = false;

		/**
		 * Le constructeur.
		 *
		 * @param AbstractSettings|null $settings Une interface de paramétrage. Si aucune interface n'est passée, la classe tente de lire automatiquement dans StaticSettings.
		 */
		public function __construct (AbstractSettings $settings = null)
		{
			$this->settings = $settings ?: StaticSettings::instance();

			/* Auto-connexion. */
			if ( $this->readSettings($this->settings) )
			{
				$this->isConnected = $this->tryConnexion();

				if ( !$this->isConnected )
					trigger_error(__METHOD__.'(), auto-connexion failed !', E_USER_WARNING);
			}
		}

		/**
		 * Lance une nouvelle connexion.
		 *
		 * @return bool Le status de la connexion.
		 */
		public function connect (AbstractSettings $settings = null): bool
		{
			if ( !$this->readSettings($settings ?: StaticSettings::instance()) )
				return false;

			$this->isConnected = $this->tryConnexion();

			return $this->isConnected;
		}

		/**
		 * Permet de savoir si une connexion à été précédemment établie.
		 * NOTE: Ne test pas la connexion en tant que tel.
		 *
		 * @return bool Le status de la connexion.
		 */
		public function isConnected (): bool
		{
			return $this->isConnected;
		}

		/**
		 * Permet de savoir sur quelle base de données le client est connecté.
		 *
		 * @return string Le bonome de la base de données.
		 */
		public function databaseName (): string
		{
			return $this->databaseName;
		}

		/**
		 * Permet de se connecter à une autre base de données en gardant les mêmes informations de connexion.
		 * Induit une déconnexion et une reconnexion.
		 *
		 * @param string $name Le nom de la base de données.
		 * @return bool Le status de la connexion.
		 */
		public function changeDatabase (string $name): bool
		{
			$this->databaseName = $name;

			return ( $this->isConnected = $this->tryConnexion() );
		}

		/**
		 * Simule un ping sur la base de données pour savoir si elle répond.
		 *
		 * @return bool Le status de la connexion.
		 */
		public function ping (): bool
		{
			if ( parent::query('SELECT 1;') === false )
				return false;
			
			return true;
		}

		/**
		 * Regarde si une table existe dans la base de données.
		 *
		 * @param string $name Le nom de la table à vérifier.
		 * @return bool L'existence de la table.
		 */
		public function tableExists (string $name): bool
		{
			return $this->execute('SHOW TABLES FROM `'.$this->databaseName.'` LIKE "'.$name.'";');
		}

		/**
		 * Un raccourcit pour les requêtes de type INSERT/UPDATE.
		 *
		 * @param string $statement La requête SQL.
		 * @return bool|int false en cas d'erreur où le nombre de ligne affecté. NOTE: peut être 0, utiliser "!== false".
		 */
		public function execute (string $statement): bool|int
		{
			/* 1. Check connexion. */
			if ( !$this->isConnected() )
			{
				trigger_error(__METHOD__.', try to make a request without being first connected to the data base !', E_USER_WARNING);

				return false;
			}

			/* 2. Raw PDO query. */
			$PDOStatement = parent::query($statement);

			if ( $PDOStatement === false )
			{
				$error = parent::errorInfo();

				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

				return false;
			}

			/* 3. Return results. */
			return $PDOStatement->rowCount();
		}
		
		/**
		 * Un raccourcit pour les requêtes de type SELECT où le résultat n'est qu'une seule valeur.
		 *
		 * @param string $statement La requête SQL.
		 * @param string $type Le type de la valeur. Le type peut être Database::IntegerType, Database::FloatType ou Database::StringType (par défaut).
		 * @return mixed|bool false en cas d'erreur où la valeur dans le type demandé. NOTE: utiliser "!== false".
		 */
		public function getResult (string $statement, string $type = self::StringType)
		{
			/* 1. Check connexion. */
			if ( !$this->isConnected() )
			{
				trigger_error(__METHOD__.', try to make a request without being first connected to the data base !', E_USER_WARNING);

				return false;
			}

			/* 2. Raw PDO query. */
			$PDOStatement = parent::query($statement);

			if ( $PDOStatement === false )
			{
				$error = parent::errorInfo();

				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

				return false;
			}

			/* 3. Return results. */
			$value = $PDOStatement->fetchColumn();
		
			switch ( $type )
			{
				case self::IntegerType :
					return intval($value);
				
				case self::FloatType :
					return floatval($value);
				
				default :
					return $value;
			}
		}
		
		/**
		 * Un raccourcit pour les requêtes de type SELECT où le résultat est une ligne de données.
		 *
		 * @param string $statement La requête SQL.
		 * @param int $mode Une constante de la classe PDO pour le mode de récupération des données. Par défaut le mode est PDO::FETCH_ASSOC.
		 * http://php.net/manual/fr/book.pdo.php pour plus d'informations à propos de PDO.
		 * @return array|bool false en cas d'erreur où un tableau de la ligne demandée. NOTE: utiliser "!== false".
		 */
		public function getRow (string $statement, int $mode = self::FETCH_ASSOC)
		{
			/* 1. Check connexion. */
			if ( !$this->isConnected() )
			{
				trigger_error(__METHOD__.', try to make a request without being first connected to the data base !', E_USER_WARNING);

				return false;
			}

			/* 2. Raw PDO query. */
			$PDOStatement = parent::query($statement);

			if ( $PDOStatement === false )
			{
				$error = parent::errorInfo();

				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

				return false;
			}

			/* 3. Return results. */
			return $PDOStatement->rowCount() ? $PDOStatement->fetch($mode) : [];
		}
		
		/**
		 * Un raccourcit pour les requêtes de type SELECT où le résultat contient plusieurs lignes de données.
		 *
		 * @param string $statement La requête SQL.
		 * @param string $fieldAsKey Le nom d'un champ dont les valeurs sont uniques. 
		 * Si ce paramètre est utilisé, les indices du tableau principal seront remplacés par la valeur du champ.
		 * Exemple, une sélection particulière d'utilisateurs avec comme indice leur ID permettra de rechercher le bon utilisateur sans parcourir tout le tableau.
		 * @param int $mode Une constante de la classe PDO pour le mode de récupération des données. Par défaut le mode est PDO::FETCH_ASSOC.
		 * http://php.net/manual/fr/book.pdo.php pour plus d'informations à propos de PDO.
		 * @return bool|array false en cas d'erreur où un tableau contenant les lignes demandées. NOTE: utiliser "!== false".
		 */
		public function getArray (string $statement, string $fieldAsKey = '', int $mode = self::FETCH_ASSOC): bool|array
		{
			/* 1. Check connexion. */
			if ( !$this->isConnected() )
			{
				trigger_error(__METHOD__.', try to make a request without being first connected to the data base !', E_USER_WARNING);

				return false;
			}

			/* 2. Raw PDO query. */
			$PDOStatement = parent::query($statement);

			if ( $PDOStatement === false )
			{
				$error = parent::errorInfo();

				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$error[2], E_USER_WARNING);

				return false;
			}

			/* 3. Return results. */
			if ( $PDOStatement->rowCount() )
			{
				/* NOTE: Pas de traitement par clé. */
				if ( empty($fieldAsKey) )
					return $PDOStatement->fetchAll($mode);

				$results = [];
			
				while ( $row = $PDOStatement->fetch($mode) )
				{
					if ( !array_key_exists($fieldAsKey, $row) )
					{
						trigger_error(__METHOD__.'(), the key "'.$fieldAsKey.'" not exists inside the results.', E_USER_WARNING);

						return false;
					}
				
					$key = $row[$fieldAsKey];
				
					if ( array_key_exists($key, $results) )
					{
						trigger_error(__METHOD__.'(), the key "'.$fieldAsKey.'" is not an unique field. A previous row will be erased !', E_USER_WARNING);
					}
				
					$results[$key] = $row;
				}
			
				return $results;
			}
		
			return [];
		}

		/**
		 * Ajouter le caractère '`' autour des noms de champs, table ou base de données.
		 *
		 * @internal
		 * @param string $name Le nom à traiter.
		 * @return string
		 */
		static private function name (string $name): string
		{
			if ( !str_contains($name, '.') )
				return '`'.$name.'`';

			$chunks = explode('.', $name);

			foreach ( $chunks as &$chunk )
				$chunk = '`'.$chunk.'`';

			return implode('.', $chunks);			
		}

		/**
		 * Sécurise l'ajout d'une valeur dans une requête SQL.
		 *
		 * @internal
		 * @param mixed $data La valeur à traiter.
		 * @return string
		 */
		static private function value (mixed $data): string
		{
			if ( is_numeric($data) )
				return $data;

			if ( is_array($data) || is_object($data) )
				return '"' . addslashes(serialize($data)) . '"';

			return '"' . addslashes($data) . '"';
		}

		/**
		 * Construit une clause WHERE.
		 *
		 * @param array $equalConditions
		 * @param array $likeConditions
		 * @param string $glue
		 * @return string.
		 */
		static public function buildWhere (array $equalConditions, array $likeConditions = [], string $glue = 'AND'): string
		{
			$chunks = [];

			foreach ( $equalConditions as $field => $what )
				$chunks[] = self::name($field).' = '.self::value($what);

			foreach ( $likeConditions as $field => $what )
				$chunks[] = self::name($field).' LIKE "'.addslashes($what).'"';

			return implode(' '.$glue.' ', $chunks);
		}

		/**
		 * Fonction utilitaire qui permet de construire rapidement une requête de type "INSERT ON DUPLICATE KEY UPDATE" en utilisant directement des tableaux de données PHP.
		 * Cette méthode ajouter automatiquement des slashes au données de type texte.
		 *
		 * @param string $table Le nom de la table cible.
		 * @param array $insertVars Un tableau de clés/valeurs dans le cas d'une insertion.
		 * @param array $updateVars Un tableau de clés/valeurs dans le cas d'une mise à jour.
		 * @return string|null
		 */
		static public function writeInsertOrUpdateQuery (string $table, array $insertVars, array $updateVars): ?string
		{
			if ( empty($insertVars) )
			{
				trigger_error(__METHOD__.'(), no INSERT variable !', E_USER_WARNING);
				
				return null;
			}

			if ( empty($updateVars) )
			{
				trigger_error(__METHOD__.'(), no UPDATE variable ! Use Database::writeInsert() writeInsertQuery.', E_USER_WARNING);
				
				return null;
			}

			/* Insert part of the query. */
			$insertFields = [];
			$insertValues = [];
			
			foreach ( $insertVars as $field => $value )
			{
				$insertFields[] = self::name($field);
				$insertValues[] = self::value($value);
			}

			/* Update part of the query. */
			$updatesAssignements = [];
			
			foreach ( $updateVars as $field => $value )
				$updatesAssignements[] = self::name($field).' = '.self::value($value);
			
			return
				'INSERT INTO '.self::name($table).' ('.implode(', ', $insertFields).') '.
				'VALUES ('.implode(', ', $insertValues).') '.
				'ON DUPLICATE KEY UPDATE '.implode(', ', $updatesAssignements).';';
		}

		/**
		 * Fonction utilitaire qui permet de construire rapidement une requête de type "INSERT" en utilisant directement des tableaux de données PHP.
		 * Cette méthode ajouter automatiquement des slashes au données de type texte.
		 *
		 * @param string $table Le nom de la table cible.
		 * @param array $insertVars Un tableau de clés/valeurs où les clés correspondent à des champs dans la table ciblée.
		 * @param bool $ignore Spécifie d'ignorer les erreurs de doublons. Désactivé par défaut.
		 * @return string|null
		 */
		static public function writeInsertQuery (string $table, array $insertVars, bool $ignore = false): ?string
		{
			if ( empty($insertVars) )
			{
				trigger_error(__METHOD__.'(), no variable !', E_USER_WARNING);
				
				return null;
			}

			$insertFields = [];
			$insertValues = [];
			
			foreach ( $insertVars as $field => $value )
			{
				$insertFields[] = self::name($field);
				$insertValues[] = self::value($value);
			}
			
			return 
				'INSERT'.( $ignore ? ' IGNORE' : null ).' INTO '.self::name($table).' '.
				'('.implode(', ', $insertFields).') '.
				'VALUES ('.implode(', ', $insertValues).');';
		}

		/**
		 * Fonction utilitaire qui permet de construire rapidement une requête de type "UPDATE" en utilisant directement des tableaux de données PHP.
		 * Cette méthode ajouter automatiquement des slashes au données de type texte.
		 *
		 * @param string $table Une chaîne de caractères pour le nom de la table cible.
		 * @param array $variables Un tableau de clés/valeurs où les clés correspondent à des champs dans la table ciblée.
		 * @param string|int $id Spécifie l'ID de l'enregistrement à modifier.
		 * @param string $idFieldName Le nom du champ pour l'ID d'enregistrement. Par défaut, "id".
		 * @return string|null
		 */
		static public function writeUpdateQuery (string $table, array $variables, $id, string $idFieldName = 'id'): ?string
		{
			if ( empty($variables) || empty($id) )
			{
				trigger_error(__METHOD__.'(), no variable or ID !', E_USER_WARNING);
				
				return null;
			}

			$updates = [];
			
			foreach ( $variables as $field => $value )
				$updates[] = self::name($field).' = '.self::value($value);
			
			return 
				'UPDATE '.self::name($table).' '.
				'SET '.implode(', ', $updates).' '.
				'WHERE '.self::name($idFieldName).' = '.self::value($id) .' '.
				'LIMIT 1;';
		}

		/**
		 * Fonction qui lit hors d'un objet de type AbstractSettings les paramètres de connexion à la base de données.
		 *
		 * @internal
		 * @param AbstractSettings $settings Les paramètres associés à la base de données.
		 * @return bool.
		 */
		private function readSettings (AbstractSettings $settings): bool
		{
			$this->hostname = $settings->get(self::HostnameKey, 'localhost');
			$this->databaseName = $settings->get(self::DatabaseNameKey);
			$this->username = $settings->get(self::UsernameKey);
			$this->password = $settings->get(self::PasswordKey);
			$this->connexionCharset = $settings->get(self::ConnexionCharsetKey, 'utf8mb4');

			if ( empty($this->hostname) || empty($this->databaseName) || empty($this->username) )
			{
				trigger_error(__METHOD__.'(), some settings are missing in order to connect a database ! Check the hostname, databasename and/or username.', E_USER_WARNING);

				return false;
			}

			return true;
		}

		/**
		 * Lance une tentative de connexion à la base de données.
		 *
		 * @internal
		 * @return bool.
		 */
		private function tryConnexion (): bool
		{
			try
			{
				$dsn = $this->databaseType.':dbname='.$this->databaseName.';host='.$this->hostname;
			
				parent::__construct($dsn, $this->username, $this->password, [
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "'.$this->connexionCharset.'";',
					PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
				]);
			}
			catch ( Exception $e )
			{
				trigger_error(__METHOD__.'(), '.$e, E_USER_WARNING);

				return false;
			}

			return true;
		}
	};
