<?php

	/*
	* Libraries/php/LTK/Captcha.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	namespace LTK;

	/**
	 * Classe de génération de code captcha basique.
	 *
	 * L'idée est de créer un nombre aléatoire de base qui permet de créer une chaîne
	 * de caractères aléatoire, le code captcha. Ce code sera ensuite passé sous forme d'image accompagné du nombre aléatoire
	 * de base dans le formulaire à protéger. Lors de la soumission de ce formulaire, on regénère le code
	 * captcha avec le nombre de base passé dans le formulaire afin de le comparer avec le code recopié par l'utilisateur.
 	 * @warning Ce système est assez faible et ne doit être en aucun cas utiliser pour protéger des données sensibles.
 	 * Ce sytème est prévu pour contrer des robots spammeurs avec une perte de performance néglieable et une mise en place simple.
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class Captcha
	{
		private bool $initial = true;
		private int $base = 0;
		private $code = null;

		/**
		 * Constructeur par défaut.
		 */
		public function __construct ()
		{

		}

		/**
		 * Initialise un nouveau captcha.
		 *
		 * @return int Le base code à passer dans Captcha::recreateFromBase().
		 */
		public function generate (): int
		{
			$this->initial = true;
			$this->base = rand(100000, 999999);
			
			$this->computeCode();

			return $this->base;
		}

		/**
		 * Ré-initialise un précédent captcha. Cette version sert à afficher l'image.
		 *
		 * @param int Le base code à passer dans Captcha::fromBase().
		 */
		public function recreateFromBase (int $base)
		{
			$this->initial = false;
			$this->base = $base;

			$this->computeCode();
		}
		
		private function computeCode ()
		{
			$compB = substr(time(), 0, 7);
			$compB = intval($compB);
			
			$compute = ($this->base * $compB) / ($this->base + $compB);
			
			$this->code = substr($compute, 0, 6);
		}
				
		/**
		 * Permet de vérifier le code renvoyer par l'utilisateur.
		 *
		 * @param string $token Un chaîne de caractères contenant le token caché dans le formulaire.
		 * @param string $code Un chaîne de caractères contenant le code recopié par l'utilisateur.
		 * @return bool
		 */
		public function checkCode (string $token, string $code): bool
		{
			return $token === md5($code);
		}
		
		/**
	 	 * Retourne le chiffre de base employé par la génération captcha en cours.
 		 * Permet de pouvoir créer l'image correspondant au token dans un autre script.
		 *
		 * @return int
		 */
		public function base (): int
		{
			return $this->base;
		}
		
		/**
		 * Retourne le token à dissimuler dans le formulaire.
		 *
		 * @return string
		 */
		public function token (): string
		{
			return md5($this->code);
		}
		
		/**
		 * Permet de créer l'image affichant le code à recopier.
		 * Ceci est à éxécuter dans un script à part en utilisant le chiffre de base (Captcha::getBase()).
		 *
		 * @return bool
		 */
		public function createImageOutput (): bool
		{
			if ( $this->initial )
			{
				trigger_error(__METHOD__.'(), you must use Captcha::recreateFromBase($code) to use this method !');

				return false;
			}
		
			header('Content-type: image/jpeg');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			
			$image = imageCreate(64, 16);
			imageColorAllocate($image, 127, 127, 127);
		
			$textColor = imageColorAllocate($image, 255, 0, 0);
		
			imageString($image, 5, 7, 1, $this->code ? $this->code : 'NOCODE', $textColor);
			imagejpeg($image, null, 60);
			imageDestroy($image);

			return true;
		}
	}
	
