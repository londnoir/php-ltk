<?php

	/*
	* Libraries/php/LTK/EmailAddress.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;
	
	/**
	* Classe facilitant l'usage des adresses email.
	*
	* @author LondNoir <londnoir@gmail.com>
	*/
	class EmailAddress
	{
		private string $location = '';
		private string $host = '';
		private string $name = '';

		/**
		 * Constructor.
		 *
		 * @param string $string Une chaîne de caractères contenant une adresse à analysze. Exemple 'john.doe@domain.net' ou 'John Doe <john.doe@domain.net>'. Par défaut ''.
		 */
		public function __construct (string $string = '')
		{
			if ( empty($string) )
				return;

			$endPosition = strlen(trim($string)) - 1;

			/* expecting "John Doe" <john.doe@domain.net> */
			if ( $string[$endPosition] === '>' )
			{
				if ( ($startPosition = strpos($string, '<')) !== false )
				{
					$base = substr($string, $startPosition + 1, $endPosition - $startPosition - 1);

					$this->extract($base);

					/* Extract the name before the address. */
					$this->name = substr($string, 0, $startPosition - 1);
					$this->name = str_replace('"', null, $this->name);
					$this->name = trim($this->name);
				}
			}
			/* expecting john.doe@domain.net */
			else
			{
				$this->extract($string);
			}
		}

		/**
		 * Vérifie la syntaxe d'une adresse email.
		 *
		 * @param string $string L'adresse email à vérifier.
		 * @return bool
		 */
		static public function checkSyntax (string $string): bool
		{
			return (new EmailAddress($string))->isValid();
		}

		/**
		 * Vérifie la syntaxe d'une adresse email et la retourne.
		 *
		 * @param string $string L'adresse email à vérifier.
		 * @param bool $simplify Ne retourne que 'john.doe@example.com' sur une adresse de type 'John Doe <john.doe@example.com>'
		 * @return string
		 */
		static public function sanitize (string $string, bool $simplify = false): string
		{
			$address = new EmailAddress($string);

			if ( $address->isValid() )
				return $simplify ? $address->address() : $address->addressComplete();

			return '';
		}

		/**
		 * Vérifie que l'adresse email n'utilise pas le même nom de domaine que celui du serveur.
		 * Il faut configurer la variable 'site_domain' avec la classe StaticSettings.
		 *
		 * @param string $string L'adresse email à vérifier.
		 * @param string $domain Un nom de domaine. Sinon vérifiera la clé 'site_domain' dans StaticSettings.
		 * @return bool.
		 */
		static public function isUsingSiteDomain (string $string, string $domain = ''): bool
		{
			return str_contains($domain ?? StaticSettings::instance()->get('site_domain'), $string);
		}

		/**
		 * Permet de déterminer la première partie de l'adresse email.
		 *
		 * @param string $location Une chaîne de caractères.
		 * @return self
		 */
		public function setLocation (string $location): self
		{
			$this->location = $location;

			return $this;
		}

		/**
		 * Permet de déterminer le domaine de l'adresse email (La seconde partie).
		 *
		 * @param string $host Une chaîne de caractères.
		 * @return self
		 */
		public function setHost (string $host): self
		{
			$this->host = $host;

			return $this;
		}

		/**
		 * Permet de déterminer un nom associé à l'adresse email.
		 *
		 * @param string $name Une chaîne de caractères.
		 * @return self
		 */
		public function setName (string $name): self
		{
			$this->name = $name;

			return $this;
		}

		/**
		 * Retourne la première partie de l'adresse email.
		 *
		 * @return string.
		 */
		public function location (): string
		{
			return $this->location;
		}
		
		/**
		 * Retourne le domaine de l'adresse email.
		 *
		 * @return string.
		 */
		public function host (): string
		{
			return $this->host;
		}
		
		/**
		 * Retourne le nom associé à l'adresse mail s'il existe.
		 *
		 * @return string.
		 */
		public function name (): string
		{
			return $this->name;
		}
		
		/**
		 * Permet de savoir si l'adresse email analysée ou manuellement rentrée est valide.
		 *
		 * @return bool.
		 */
		public function isValid (): bool
		{
			return $this->location && $this->host;
		}

		/**
		 * Retourn l'adresse email. Exemple: 'john.doe@example.com'
		 *
		 * @return string.
		 */		
		public function address (): string
		{
			return $this->location.'@'.$this->host;
		}

		/**
		 * Retourn l'adresse email complète. Exemple: 'John Doe <john.doe@example.com>'
		 *
		 * @return string.
		 */	
		public function addressComplete (): string
		{
			return ( $this->name ?: ucfirst($this->location) ).' <'.$this->location.'@'.$this->host.'>';
		}
		
		/**
		 * Extrait les composantes d'une adresse email de type 'john.doe@example.com'.
		 *
		 * @internal
		 * @param string $base L'addresse email.
		 * @return bool
		 */
		private function extract (string $base): bool
		{
			$base = strtolower($base);

			if ( ($quoted = strpos($base, '"')) > 0 )
				return false;

			if ( $quoted === 0 )
			{
				if ( ($pos = strpos($base, '"@')) !== false )
				{
					$this->location = substr($base, 0, $pos + 1);
					$this->host = substr($base, $pos + 2);

					return true;
				}

				return false;
			}

			/* split by '@' char. */
			$tmp = explode('@', $base);

			/* There must be only two address part. */
			if ( count($tmp) !== 2 )
				return false;

			if ( empty($tmp[0]) )
				return false;

			if ( empty($tmp[1]) )
				return false;

			/* If domain has an extension, it must contains a least 2 chars. */
			$tmpB = explode('.', $tmp[1]);
			/* Remove any comments from extension. */
			$extension = preg_replace('#\(([^)]+)\)#', null, end($tmpB));
			if ( strlen($extension) < 2 )
				return false;

			$this->location = $tmp[0];
			$this->host = $tmp[1];

			return true;
		}
	}
