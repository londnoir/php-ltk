<?php

	/*
	* Libraries/php/LTK/Lock.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe permet de bloquer des exécutions concurentes.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class Lock
	{
		private string $lockName = '';

		/**
		 * Constructeur
		 *
		 * @param string $lockName Le nom du lock.
		 */
		public function __construct ($lockName)
		{
			$this->lockName = $lockName;
		}
		
		/**
		 * Verrouille l'exécution d'un script en se basant sur son nom.
		 *
		 * @return bool
		 */
		public function lock (): bool
		{
			return @mkdir($this->lockName);
		}

		/**
		 * Déverrouille l'exécution d'un script.
		 *
		 * @return bool
		 */
		public function unlock (): bool
		{
			if ( !file_exists($this->lockName) )
				return true;

			return rmdir($this->lockName);
		}

		/**
		 * Génére un nom de verrou pour le script en cours.
		 *
		 * @return string
		 */
		static public function lockName (): string
		{
			return sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'php-ltk_' . md5($_SERVER['SCRIPT_FILENAME']) . '_lock';
		}
		
		/**
		 * Si Lock::unlock() n'a pas été appelé durant le script ou à cause d'un crash, 
		 * une vérification sur le temps peut-être effectuée avec cette méthode pour débloquer la situation.
		 *
		 * @param int $time Le délais après qu'un verrou est perçu comme obsolète.
		 * @return bool
		 */
		public function removeOldLock (int $time): bool
		{
			$timeLimit = time() - $time;
			
			if ( !file_exists($this->lockName) )
				return true;

			if ( filemtime($this->lockName) < $timeLimit )
				return rmdir($this->lockName);
			
			return false;
		}
	}
