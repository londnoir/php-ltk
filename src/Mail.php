<?php

	/*
	* Libraries/php/LTK/Mail.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	use PDO;

	/**
	 * Création de mail standard.
	 * Cette classe permet d'écrire un mail de type text ou HTML en respectant les standards afin
	 * d'éviter d'être pris pour de spam. Elle permet également de stocker les emails en base de données 
	 * pour programmer un envoi différé.
	 * Cet objet est configurable avec Config et les clés suivantes :\n
	 * - 'mail_table_name', le nom de la table qui sera utilisée pour enregistrer les emails. Par défaut, la table se nomme 'mails_queue'\n
	 * - 'mail_domain', le nom de domaine duquel son envoyé les messages. Attention, il faut que le nom de domaine soit effectivement où se trouve la machine qui envoit les emails. ("botname" <service\@DOMAINE>)\n
	 * - 'mail_bot_name', le nom du bot par défaut qui envoit les emails quand aucun expéditeur n'est spécifié. ("BOTNAME" <service\@domaine>)\n
	 * - 'mail_service', le nom du service par défaut qui envoit les emails. ("botname" <SERVICE\@domaine>)\n
	 * - 'mail_unsubscribe_link', lien de désincription par défaut. (Mesure anti-spam)\n
	 * - 'mail_unsubscribe_email', adresse email par défaut pour pour les demandes de désinscription. (Mesure anti-spam)
	 *
	 * @author LondNoir <londnoir@gmail.com>
	 */
	class Mail
	{
		const Boundary = '_php_ltk__bound_';

		const TableNameKey = 'mail_table_name';
		const DefaultTableName = 'ltk_mails';

		private ?Database $database = null;
		/* Web site dependant configuration. */
		private string $domain = '';
		private string $botName = '';
		private string $service = '';
		/* Mail info. */
		private int $databaseId = 0;
		private string $messageID = '';
		private bool $isSent = false;
		private int $priority = 127;
		private string $type = '';
		private int $sendAt = 0;
		private bool $noReply = false;
		/* Mail format. */
		private string $mode = 'text/plain';
		private string $charset = 'utf-8';
		/* Mail destination. */
		private string $to;
		private string $cc = '';
		private string $bcc = '';
		/* Mail content. */
		private string $subject;
		private string $message;
		private array $attachments = [];
		private bool $allowEmptyContent = false;
		/* Mail from. */
		private string $senderName = '';
		private string $senderEmail = '';
		private string $replyTo = '';
		/* Mailing list unsubscribe. */
		private string $unsubscribeLink = '';
		private string $unsubscribeEmail = '';
		private array $additionalsData = [];

		/**
		 * Le constructeur.
		 *
		 * @param string|array $to Une adresse ou un tableau d'adresses.
		 * @param string $subject Le sujet du message.
		 * @param string $message Le contenu du message.
		 * @param Database|null $database Un objet Database, active le système d'enregistrement en base de données. Par défaut à null.
		 */
		public function __construct (string|array $to = '', string $subject = '', string $message = '', Database $database = null)
		{
			$this->to = $this->getAddressesString($to);
			$this->subject = $subject;
			$this->message = $message;

			if ( $database )
				$this->database = $database;
		}
				
		/**
		 * Modifie le ou les destinataires du message.
		 *
		 * @param array|string $addresses Une adresse ou un tableau d'adresses.
		 * @return self
		 */
		public function setTo (array|string $addresses): self
		{
			$this->to = $this->getAddressesString($addresses);
			
			return $this;
		}
		
		/**
		 * Liste des adresses en copie carbone.
		 *
		 * @param array|string $addresses Une adresse ou un tableau d'adresses.
		 * @return self
		 */
		public function setCopyCarbon (array|string $addresses): self
		{
			$this->cc = $this->getAddressesString($addresses);
			
			return $this;
		}
		
		/**
		 * Liste des adresses en copie carbone cachée.
		 *
		 * @param array|string $addresses Une adresse ou un tableau d'adresses.
		 * @return self
		 */
		public function setBlindCopyCarbon (array|string $addresses): self
		{
			$this->bcc = $this->getAddressesString($addresses);
			
			return $this;
		}
		
		/**
		 * Spécifie le titre du message.
		 *
		 * @param string $subject Le sujet du message.
		 * @return self
		 */
		public function setSubject (string $subject): self
		{
			$this->subject = $subject;

			return $this;
		}
		
		/**
		 * Spécifie le contenu du message.
		 *
		 * @param string $message Le contenu du message.
		 * @return self
		 */
		public function setMessage (string $message): self
		{
			$this->message = $message;

			return $this;
		}

		/**
		 * Modifie l'en-tête "From:" pour ce message. Permet de bypasser la configuration.
		 * Exemple: "From: "[BotName]" <[Email]>"
		 *
		 * @warning Utiliser un nom de domaine qui correspond au serveur qui envoit réellement le mail.
		 * @param string $botName Le nom du botmail.
		 * @param string $email Le nom de domaine.
		 * @return self
		 */
		public function setFrom (string $botName, string $email): self
		{
			$this->botName = $botName;

			/* Check the Sender: */
			if ( $address = EmailAddress::sanitize($email) )
			{
				$chunks = explode('@', $address);

				$this->domain = $chunks[1];
				$this->service = $chunks[0];
			}
			else
			{
				trigger_error(__METHOD__.'(), email address "'.$email.'" is invalid !', E_USER_WARNING);
			}

			return $this;
		}
				
		/**
		 * Modifie le nom du botmail dans l'en-tête "From:" pour ce message.
		 * Exemple: "From: "[BotName]" <ServiceName@domainName>"
		 *
		 * @param string $name Le nom du botmail.
		 * @return self
		 */
		public function setBotName (string $name): self
		{
			$this->botName = $name;

			return $this;
		}

		/**
		 * Modifie le nom du service dans l'en-tête "From:" pour ce message.
		 * Exemple: "From: "BotName" <[ServiceName]@domainName>"
		 *
		 * @param string $name Le nom du service.
		 * @return self
		 */
		public function setServiceName (string $name): self
		{
			$this->service = $name;

			return $this;
		}

		/**
		 * Modifie le nom de domaine pour ce message.
		 * Exemple: "From: "BotName" <ServiceName@[domainName]>"
		 *
		 * @warning Utiliser un nom de domaine qui correspond au serveur qui envoit réellement le mail.
		 * @param string $name Le nom de domaine.
		 * @return self
		 */
		public function setDomainName (string $name): self
		{
			$this->domain = $name;

			return $this;
		}

		/**
		 * Ajoute une en-tête "Sender:" pour spécifier qui envoi ce message depuis un service.
		 * L'envoyeur officiel de l'en-tête "From:" reste le serveur.
		 *
		 * @deprecated
		 * @param string $name Le nom de l'utilisateur.
		 * @param string $email L'adresse email de l'utilisateur.
		 * @param string $replyTo L'adresse de réponse ("Reply-to:"). Utilisera par défaut l'adresse du deuxième paramètre.
		 * @return self
		 */
		public function setHumanoidSender (string $name, string $email, string $replyTo = ''): self
		{
			$this->senderName = $name;
			
			/* Check the Sender: */
			if ( $address = EmailAddress::sanitize($email) )
			{
				$this->senderEmail = $address;

				/* Check the Reply-to: */
				if ( empty($replyTo) )
				{
					$this->replyTo = $address;
				}
				else
				{
					if ( $replyTo = EmailAddress::sanitize($replyTo) )
						$this->replyTo = $replyTo;
					else
						trigger_error(__METHOD__.'(), reply-to email address "'.$replyTo.'" is invalid !', E_USER_WARNING);
				}
			}
			else
			{
				trigger_error(__METHOD__.'(), email address "'.$email.'" is invalid !', E_USER_WARNING);
			}
			
			return $this;
		}
		
		/**
		 * Permet d'envoyer un email avec un contenu vide sans déclencher d'avertissement.
		 *
		 * @return self
		 */
		public function allowEmptyContent (): self
		{
			$this->allowEmptyContent = true;

			return $this;
		}
		
		/**
		 * Permet l'utilisation de la base de données pour stocker le message.
		 *
		 * @param Database $database Un object de type Database.
		 * @return self
		 */
		public function enableDatabase (Database $database): self
		{
			$this->database = $database;
			
			return $this;
		}

		/**
		 * Permet d'ajouter une donnée supplémentaire dans la base de données.
		 *
		 * @param string $key Un nom du champs.
		 * @param string $value La valeur du champs.
		 * @return self
		 */
		public function addAdditionalData (string $key, string $value): self
		{
			$this->additionalsData[$key] = $value;

			return $this;
		}

		/**
		 * Attribue un type du message.
		 * NOTE: Seulement utile pour trier les emails dans la base de données.
		 *
		 * @param string $type Le type du message.
		 * @return self
		 */
		public function setType (string $type): self
		{
			$this->type = $type;

			return $this;
		}
		
		/**
		 * Modifie la priorité d'envoi du message. Plus le chiffre est élevé, plus le message sera envoyé rapidement.
		 *
		 * @param int $priority Un nombre entier de 0 à 255.
		 * @return self
		 */
		public function setPriority (int $priority): self
		{
			$priority = intval($priority);
			
			if ( $priority >= 0 && $priority <= 255 )
				$this->priority = $priority;
			
			return $this;
		}
		
		/**
		 * Indique dans l'en-tête que le message n'attend pas de réponse.
		 *
		 * @return self
		 */
		public function setNoReply (): self
		{
			$this->noReply = true;
			
			return $this;
		}
				
		/**
		 * Spécifie le format du message.
		 *
		 * @param string $mode Une chaîne de caractères avec un des formats suivants :\n
		 * "text/plain", simple texte.\n
		 * "text/html", format HTML.\n
		 * "text/rtf" Rich Text Format
		 * @param string $charset Une chaîne de caractères pour le format de codage de caractères. "utf-8" par défaut.
		 * @return self
		 */
		public function setMailFormat (string $mode, string $charset = 'utf-8'): self
		{
			$mode = strtolower($mode);

			$this->mode = match ($mode) {
				'text/plain',
				'text/html',
				'text/rtf' => $mode,
				default => 'text/plain',
			};
			$charset = strtolower($charset);
			$this->charset = match ($charset) {
				'utf-7',
				'utf-8',
				'utf-16',
				'utf-32',
				'iso-8859-1',
				'iso-8859-2',
				'iso-8859-3',
				'iso-8859-4',
				'iso-8859-5',
				'iso-8859-6',
				'iso-8859-7',
				'iso-8859-8',
				'iso-8859-9',
				'iso-8859-10',
				'iso-8859-11',
				'iso-8859-13',
				'iso-8859-14',
				'iso-8859-15',
				'iso-8859-16' => $charset,
				default => 'utf-8',
			};
			
			return $this;
		}
		
		/**
		 * Modifie les méthodes de désinscription.
		 *
		 * @param string $link L'URL de désinscription.
		 * @param string $email L'adresse email de désinscription. Optionnel.
		 * @return self
		 */
		public function setListUnsubscribe (string $link, string $email = ''): self
		{
			$this->unsubscribeLink = $link;
			
			if ( !empty($email) )
			{
				if ( $email = EmailAddress::sanitize($email) )
					$this->unsubscribeEmail = $email;
				else
					trigger_error(__METHOD__.'(), unsubscribe email address "'.$email.'" is invalid !', E_USER_WARNING);
			}
			
			return $this;
		}
		
		/**
		 * Spécifie la date après laquelle le message peut-être envoyé.
		 *
		 * @param int $time La date (UNIX timestamp).
		 * @return self
		 */
		public function sendAt (int $time): self
		{
			$this->sendAt = $time;
			
			return $this;
		}
		
		/**
		 * Ajoute une pièce jointe au message.
		 *
		 * @param string $filepath Le chemin du fichier.
		 * @param string $fileMimeType Le type mime du fichier. Sera recherché automatiquement.
		 * @return bool
		 */
		public function attachFile (string $filepath, string $fileMimeType = ''): bool
		{
			if ( !file_exists($filepath) )
			{
				trigger_error(__METHOD__.'(), file "'.$filepath.'" not exists !', E_USER_WARNING);
				
				return false;
			}
			
			/* Get the filename. */
			$tmp = explode('/', $filepath);
			$fileName = end($tmp);
			
			/* Get the mime type. */
			if ( is_null($fileMimeType) )
				$fileMimeType = 'application/octet-stream';
			
			/* Transform file content for mail. */
			$fileContent = file_get_contents($filepath);
			$fileContent = base64_encode($fileContent);
			$fileContent = chunk_split($fileContent);
			
			/* Add to attachment. */
			$this->attachments[] =
				'Content-Type: '.$fileMimeType.'; name="'.$fileName.'"'."\n".
				'Content-Transfer-Encoding: base64'."\n".
				'Content-Disposition: attachment; filename="'.$fileName.'"'."\n\n".
				$fileContent;
			
			return true;	
		}
		
		/**
		 * Vérifie plusieurs adresses email séparées par virgule ou dans un tableau. 
		 * Les adresses non valides seront retirées.
		 *
		 * @param array|string $addresses Une adresse ou un tableau d'adresses.
		 * @param string $separator Le séparateur des adresses en sortie.
		 * @return string
		 */
		static public function getAddressesString (array|string $addresses, string $separator = ','): string
		{		
			if ( is_array($addresses) )
			{
				/* Array which take all valid addresses. */
				$tmp = [];

				foreach ( $addresses as $address )
				{
					$address = EmailAddress::sanitize($address);

					if ( empty($address) )
					{
						trigger_error(__METHOD__.'(), email address "'.$address.'" is invalid !', E_USER_WARNING);

						continue;
					}

					$tmp[] = $address;
				}

				/* Create a cool string with all validated addresses. */
				return empty($tmp) ? '' : implode($separator, $tmp);
			}

			$address = EmailAddress::sanitize($addresses);

			if ( empty($address) )
			{
				trigger_error(__METHOD__.'(), email address "'.$address.'" is invalid !', E_USER_WARNING);

				return '';
			}

			return $address;
		}
		
		/**
		 * Génère l'en-tête "Message-ID:"
		 *
		 * @internal
		 * @return string
		 */
		private function generateMessageID (): string
		{
			$a = time();
			$b = rand(1000000000,9999999999);
			
			if ( $this->database && $this->databaseId )
				return $a.'.'.$b.'.'.$this->databaseId.'@'.$this->domain;
			else
				return $a.'.'.$b.'@'.$this->domain;
		}

		/**
		 * Génère l'en-tête du message selon ses paramètres.
		 *
		 * @internal
		 * @param bool $wholeHeader Ajoute "To:" et "Subject:".
		 * @return string
		 */
		private function generateHeaders (bool $wholeHeader = false): string
		{		
			/* Basic headers. */
			$headers = 
				'MIME-Version: 1.0'."\r\n".
				'Content-Type: '.( empty($this->attachments) ? $this->mode.'; charset='.$this->charset : 'multipart/mixed; boundary="'.self::Boundary.'"' )."\r\n".
				/* NOTE: lang with no accents takes 7bits. */
				'Content-Transfer-Encoding: 8bit'."\r\n".
				'Content-Transfer-Encoding: quoted-printable'."\r\n".
				'From: '.( $this->senderName ? $this->senderName : $this->botName ).' <'.$this->service.'@'.$this->domain.'>'."\r\n";
			
			/* Set a real sender. */
			if ( $this->senderEmail )
				$headers .= 'Sender: "'.$this->senderName.'" <'.$this->senderEmail.'>'."\r\n";
			
			/* Return-Path: */

			/* Reply-To: */
			if ( $this->noReply )
				$headers .= 'Reply-To: noreply@'.$this->domain."\r\n";
			else
				$headers .= 'Reply-To: '.( $this->replyTo ? $this->replyTo : $this->service.'@'.$this->domain )."\r\n";
						
			/* Copy carbon list. */
			if ( $this->cc )
				$headers .= 'Cc: '.$this->cc."\r\n";
			
			/* Blind copy carbon list. */
			if ( $this->bcc )
				$headers .= 'Bcc: '.$this->bcc."\r\n";

			/* Message-ID (Anti-spam identification). */
			if ( $this->messageID )
				$headers .= 'Message-ID: <'.$this->messageID.'>'."\r\n";
			
			/* List-Unsubscribe. */
			{
				/* List unsubscribe. */
				$luMethods = [];

				if ( $this->unsubscribeEmail )
					$luMethods[] = '<mailto:'.$this->unsubscribeEmail.'>';

				if ( $this->unsubscribeLink )
					$luMethods[] = '<'.$this->unsubscribeLink.'>';

				if ( !empty($luMethods) )
					$headers .= 'List-Unsubscribe: '.implode(',', $luMethods)."\r\n";
			}

			$headers .= 
				'X-Mailer: PHP/'.phpversion()."\r\n".
				'Date: '.date('r')."\r\n";

			if ( $wholeHeader )
			{
				$headers .= 
					'To: '.$this->to."\r\n".
					'Subject: '.$this->encodeSubject()."\r\n";
			}

			return $headers;
		}
		
		/**
		 * Génère le corps du message.
		 *
		 * @internal
		 * @return string
		 */
		private function generateContent (): string
		{
			if ( empty($this->attachments) )
				return $this->message;
			
			/* Message. */
			$buffer = 
				'--'.self::Boundary."\n".
				'Content-Type: '.$this->mode.'; charset='.$this->charset."\n\n".
				$this->message."\n";
			
			/* Attachments. */
			foreach ( $this->attachments as $attachment )
			{
				$buffer .= 
					'--'.self::Boundary."\n".
					$attachment;
			}
			
			/* End of boundary. */
			$buffer .= '--'.self::Boundary.'--'."\n";
			
			return $buffer;
		}
		
		/**
		 * Encode le sujet du message pour l'envoi.
		 *
		 * @internal
		 * @return string
		 */
		private function encodeSubject (): string
		{
			return $this->subject ? StringManipulation::encodeToRFC2047($this->subject, StringManipulation::Base64, $this->charset) : '';
		}
		
		/**
		 * Vérifie que le message peut-être envoyé.
		 *
		 * @internal
		 * @return bool
		 */
		private function checkMailStatus (): bool
		{
			if ( empty($this->to) )
			{
				trigger_error(__METHOD__.'(), email destination is empty !', E_USER_WARNING);

				return false;
			}

			if ( $this->allowEmptyContent == false )
			{
				if ( !$this->subject )
				{
					trigger_error(__METHOD__.'(), email subject is empty !', E_USER_WARNING);

					return false;
				}
					
				if ( !$this->message )
				{
					trigger_error(__METHOD__.'(), email content is empty !', E_USER_WARNING);

					return false;
				}
			}

			/* Set default parameters. */
			if ( empty($this->domain) )
			{
				$default = '';
				
				if ( isset($_SERVER['HTTP_HOST']) )
				{
					$tmp = explode('.', $_SERVER['HTTP_HOST']);
					
					$count = count($tmp);
					
					if ( $count < 2 )
						trigger_error(__METHOD__.'(), domain "'.$_SERVER['HTTP_HOST'].'" is not suitable for generating server email address !', E_USER_ERROR);
					elseif ( $count == 2 )
						$default = $_SERVER['HTTP_HOST'];
					else
						$default = $tmp[$count - 2].'.'.$tmp[$count - 1];
				}
				
				$this->domain = StaticSettings::instance()->get('mail_domain', $default);

				if ( empty($this->domain) )
				{
					trigger_error(__METHOD__.'(), there is no domain to complete the server email address !', E_USER_WARNING);
					
					return false;
				}
			}
			
			/* Message-ID generation. */
			if ( empty($this->messageID) )
				$this->messageID = $this->generateMessageID();
			
			if ( empty($this->botName) )
				$this->botName = StaticSettings::instance()->get('mail_bot_name', 'MailBot');
			
			if ( empty($this->service) )
				$this->service = StaticSettings::instance()->get('mail_service', 'system');
			
			if ( empty($this->unsubscribeLink) )
				$this->unsubscribeLink = StaticSettings::instance()->get('mail_unsubscribe_link');
			
			if ( empty($this->unsubscribeEmail) )
				$this->unsubscribeEmail = StaticSettings::instance()->get('mail_unsubscribe_email');
						
			return true;
		}
		
		/**
		 * Sauvegarde le message en cours dans la base de données.
		 *
		 * @return int
		 */
		public function save (): int
		{
			if ( $this->database == null )
			{
				trigger_error(__METHOD__.'(), Mail class need to be initialized with a Database object in order to use this method !', E_USER_WARNING);

				return 0;
			}

			if ( !$this->checkMailStatus() )
				return 0;

			$table = StaticSettings::instance()->get('mail_table_name', 'mails_queue');

			$data = [
				'send_at' => $this->sendAt,
				'domain' => $this->domain,
				'bot_name' => $this->botName,
				'service' => $this->service,
				'mode' => $this->mode,
				'charset' => $this->charset,
				'priority' => $this->priority,
				'type' => $this->type,
				'to' => $this->to,
				'cc' => $this->cc,
				'bcc' => $this->bcc,
				'subject' => $this->subject,
				'message' => $this->message,
				'attachments' => serialize($this->attachments),
				'sender_name' => $this->senderName,
				'sender_email' => $this->senderEmail,
				'reply_to' => $this->replyTo,
				'unsubscribe_link' => $this->unsubscribeLink,
				'unsubscribe_email' => $this->unsubscribeEmail
			];

			/* Add other data. */
			if ( $this->additionalsData )
			{
				foreach ( $this->additionalsData as $name => $value )
					$data[$name] = $value;
			}
			
			if ( $this->databaseId )
			{
				$data['m_date'] = time();

				$query = Database::writeUpdateQuery($table, $data, $this->databaseId);

				if ( $this->database->execute($query) !== false )
					return $this->databaseId;
			}
			else
			{
				$data['c_date'] = time();

				$query = Database::writeInsertQuery($table, $data);
				
				if ( $this->database->execute($query) )
				{
					/* Retrieve database ID. */
					$this->databaseId = $this->database->lastInsertId();
					
					/* Generate a new Message-id with the database ID. */
					$this->messageID = $this->generateMessageID();

					/* Mise à jour du message ID dans la base de données. */
					$query = Database::writeUpdateQuery($table, [
						'm_date' => time(),
						'message_id' => $this->messageID
					], $this->databaseId);
												
					if ( $this->database->execute($query) === false )
						trigger_error(__METHOD__.'(), unable to save message id ('.$this->messageID.') to the mail #'.$this->databaseId.' !', E_USER_WARNING);
					
					/* Keep the ID, if the message is sent directly after saving. */
					return $this->databaseId;
				}
			}
			
			return 0;
		}
		
		/**
		 * Charge un message depuis la base de données.
		 *
		 * @param int $databaseId L'ID d'enregistrement du message.
		 * @return bool
		 */
		public function load (int $databaseId): bool
		{
			if ( $this->database != null )
			{
				trigger_error(__METHOD__.'(), Mail class need to be initialized with a Database object in order to use this method !', E_USER_WARNING);

				return false;
			}

			$statement =
				'SELECT `id`, `domain`, `bot_name`, `service`, `mode`, `charset`, `priority`, `type`, '.
				'`to`, `cc`, `bcc`, `subject`, `message`, `attachments`, `sender_name`, `sender_email`, '.
				'`reply_to`, `unsubscribe_link`, `unsubscribe_email`, `is_sent` '.
				'FROM `'.StaticSettings::instance()->get('mail_table_name', 'mails_queue').'` '.
				'WHERE `id` = '.$databaseId.' LIMIT 1;';

			$PDOStatement = $this->database->query($statement);
			
			if ( $PDOStatement === false )
				return false;

			if ( $mailData = $PDOStatement->fetch(PDO::FETCH_ASSOC) )
			{
				$this->loadFromArray($mailData);
				
				return true;
			}

			return false;		
		}
		
		/**
		 * Converti les données de la base de données vers les variables internes.
		 *
		 * @param array $mailData
		 * @internal
		 */
		private function loadFromArray (array $mailData)
		{
			$this->domain = $mailData['domain'];
			$this->botName = $mailData['bot_name'];
			$this->service = $mailData['service'];
			$this->databaseId = intval($mailData['id']);
			$this->isSent = (bool)$mailData['is_sent'];
			$this->priority = intval($mailData['priority']);
			$this->type = $mailData['type'];
			$this->mode = $mailData['mode'];
			$this->charset = $mailData['charset'];
			$this->to = $mailData['to'];
			$this->cc = $mailData['cc'];
			$this->bcc = $mailData['bcc'];
			$this->subject = $mailData['subject'];
			$this->message = $mailData['message'];
			$this->attachments = unserialize($mailData['attachments']);
			$this->senderName = $mailData['sender_name'];
			$this->senderEmail = $mailData['sender_email'];
			$this->replyTo = $mailData['reply_to'];
			$this->unsubscribeLink = $mailData['unsubscribe_link'];
			$this->unsubscribeEmail = $mailData['unsubscribe_email'];
		}
		
		/**
		 * Envoi le message. Si le message est sauvegardé en base de données, il mettera son status d'envoi a jour.
		 * Un message envoyé équivaut a un message envoyé en local sur le serveur au logiciel de mailing.
		 *
		 * @param bool $fake Un booléen pour faussé l'envois du message. Le comportement de la classe et le marquage en base de données restent inchangés ! Désactivé par défaut.
		 * @return bool
		 */
		public function send (bool $fake = false): bool
		{
			if ( !$this->checkMailStatus() )
				return false;
			
			if ( $fake )
				return true;
				
			if ( mail($this->to, $this->encodeSubject(), $this->generateContent(), $this->generateHeaders()) )
			{
				/* If mail is sent before saving. */
				$this->isSent = true;
				
				/* Notification if mail is saved into a database. */
				if ( $this->database && $this->databaseId )
				{
					$query = 
						'UPDATE `'.StaticSettings::instance()->get('mail_table_name', 'mails_queue').'` SET '.
						'`done_at` = UNIX_TIMESTAMP(), '.
						'`is_sent` = 1 '.
						'WHERE `id` = '.$this->databaseId.' LIMIT 1;';
					
					if ( $this->database->exec($query) === false )
						trigger_error(__METHOD__.'(), unable to notify mail #'.$this->databaseId.' !', E_USER_ERROR);
				}
				
				return true;
			}
			else
			{
				/* Notification if mail is saved into a database. */
				if ( $this->database && $this->databaseId )
				{
					$query = 
						'UPDATE `'.StaticSettings::instance()->get('mail_table_name', 'mails_queue').'` SET '.
						'`m_date` = UNIX_TIMESTAMP(), '.
						'`error` = `error` + 1 '.
						'WHERE `id` = '.$this->databaseId.' LIMIT 1;';
					
					if ( $this->database->exec($query) === false )
						trigger_error(__METHOD__.'(), unable to notify mail #'.$this->databaseId.' !', E_USER_ERROR);
				}
				
				trigger_error(__METHOD__.'(), standard PHP mail() function is not configured ! [DESTINATION:'.$this->to.']', E_USER_WARNING);

				return false;
			}
		}
		
		/**
		 * Retourne la source du message.
		 *
		 * @param bool $mimeConversion This will convert the source for other mail system.
		 * @return string
		 */
		public function getRawSource (bool $mimeConversion = false): string
		{
			if ( !$this->checkMailStatus() )
				return '';

			$buffer = 
				$this->generateHeaders(true)."\r\n".
				$this->generateContent();

			if ( $mimeConversion )
				return rtrim(strtr(base64_encode($buffer), '+/', '-_'), '=');

			return $buffer;
		}
		
		/**
		 * Retourne le destinataire du message.
		 *
		 * @return string
		 */
		public function to (): string
		{
			return $this->to;
		}
		
		/**
		 * Retourne le sujet encodé.
		 *
		 * @return string
		 */
		public function subject (): string
		{
			return $this->encodeSubject();
		}
		
		/**
		 * Retourne le contenu du message.
		 *
		 * @return string
		 */
		public function content (): string
		{
			return $this->generateContent();
		}
		
		/**
		 * Génère et retourne les en-têtes additionnels du message.
		 *
		 * @return string
		 */
		public function additionalHeader (): string
		{
			return $this->generateHeaders();
		}
		
		/**
		 * Donne l'ID d'enregistrement du message dans la base de données.
		 *
		 * @return int
		 */
		public function id (): int
		{
			return $this->databaseId;
		}
		
		/**
		 * Retourne le message-id généré pour ce message.
		 *
		 * @return string
		 */
		public function messageId (): string
		{
			if ( $this->checkMailStatus() )
				return $this->messageID;
			
			return '';
		}
		
		/**
		 * Permet de savoir si le message est envoyé ou non.
		 *
		 * @return bool
		 */
		public function isSent (): bool
		{
			return $this->isSent;
		}
		
		/**
		 * Vérifie qu'il n'y a pas des messages à envoyer dans la base de données. 
		 * Cette méthode a pour but d'être appelée périodiquement pour disperser les envois massifs sur le temps.
		 *
		 * @warning Il faut sauvegarder le message en base de donnée (Mail::save()) au lieu de l'envoyer directement (Mail::send()).
		 * Exemple: Appeler cette méthode toutes les 5 minutes pour envoyer au maximum 10 messages limite l'envois à 2880 courriers sur 24H.
		 * @param Database $database Une connexion à la base de données.
		 * @param int $limit Le nombre maximum de message à traiter.
		 * @param bool $fake Fausse l'envoi des messages. Le comportement de la classe et le marquage en base de données restent inchangés ! Désactivé par défaut.
		 * @param string $customWhere Ajoute une condition supplémentaire, elle sera ajoutée après un AND. Par défaut ''.
		 * @return bool|array
		 */
		static public function periodicalSendingJob (Database $database, int $limit = 1, bool $fake = false, string $customWhere = ''): array|bool
		{
			$results = array(
				'total' => 0,
				'sent' => 0,
				'error' => 0
			);
			
			$statement =
				'SELECT COUNT(1) '.
				'FROM `'.StaticSettings::instance()->get('mail_table_name', 'mails_queue').'` '.
				'WHERE `is_sent` = 0 '.
				'AND `error` < 4 '.
				'AND `send_at` < UNIX_TIMESTAMP() '.
				( $customWhere ? 'AND '.$customWhere.' ' : null ).';';
			
			$PDOStatement = $database->query($statement);

			if ( $PDOStatement === false )
			{
				trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$database->errorInfo()[2], E_USER_WARNING);
				
				return false;
			}

			$results['total'] = intval($PDOStatement->fetchColumn());

			/* If messages exists. */
			if ( $results['total'] > 0 )
			{
				$statement = 
					'SELECT `id`, `domain`, `bot_name`, `service`, `mode`, `charset`, `priority`, `type`, '.
					'`to`, `cc`, `bcc`, `subject`, `message`, `attachments`, `sender_name`, `sender_email`, '.
					'`reply_to`, `unsubscribe_link`, `unsubscribe_email`, `is_sent` '.
					'FROM `'.StaticSettings::instance()->get('mail_table_name', 'mails_queue').'` '.
					'WHERE `is_sent` = 0 '.
					'AND `error` < 4 '.
					'AND `send_at` < UNIX_TIMESTAMP() '.
					( $customWhere ? 'AND '.$customWhere.' ' : null ).
					'ORDER BY `priority` DESC LIMIT '.$limit.';';

				$PDOStatement = $database->query($statement);
				
				if ( $PDOStatement === false )
				{
					trigger_error(__METHOD__.'(), Query: '.$statement.'<br />Response: '.$database->errorInfo()[2], E_USER_WARNING);
					
					return false;
				}

				while ( $mailData = $PDOStatement->fetch(PDO::FETCH_ASSOC) )
				{
					/* NOTE: Pass database variable, so send() can mark the mail as sent. */
					$mail = new Mail();
					$mail->enableDatabase($database);
					$mail->loadFromArray($mailData);
									
					if ( $mail->send($fake) )
						$results['sent']++;
					else
						$results['error']++;
				}
			}

			return $results;
		}

		/**
		 * Une méthode statique pour créer la table dans la base de données ou afficher la requête équivalente.
		 *
		 * @param PDO|null $database Un objet PDO. Par défaut à null.
		 * @param AbstractSettings|null $settings Un objet Settings. Par défaut à null.
		 * @return bool|string
		 */
		public static function buildTable (PDO $database = null, AbstractSettings $settings = null): bool|string
		{
			if ( is_null($settings) )
				$settings = StaticSettings::instance();

			$tableName = $settings->get(self::TableNameKey, self::DefaultTableName);

			$statement = 
				'CREATE TABLE IF NOT EXISTS `'.$tableName.'` ('.
				'`id` int(16) unsigned NOT NULL AUTO_INCREMENT, '.
				'`message_id` varchar(127) NOT NULL, '.
				'`c_date` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`m_date` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`send_at` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`done_at` int(11) unsigned NOT NULL DEFAULT "0", '.
				'`bot_name` varchar(64) NOT NULL, '.
				'`service` varchar(64) NOT NULL, '.
				'`domain` varchar(127) NOT NULL, '.
				'`mode` varchar(16) NOT NULL DEFAULT "text/plain", '.
				'`charset` varchar(8) NOT NULL DEFAULT "utf-8", '.
				'`priority` smallint(3) unsigned NOT NULL DEFAULT "127", '.
				'`type` varchar(64) NOT NULL, '.
				'`to` mediumtext NOT NULL, '.
				'`cc` mediumtext NOT NULL, '.
				'`bcc` mediumtext NOT NULL, '.
				'`subject` varchar(255) NOT NULL, '.
				'`message` text NOT NULL, '.
				'`attachments` BLOB NOT NULL, '.
				'`sender_name` varchar(64) NOT NULL, '.
				'`sender_email` varchar(127) NOT NULL, '.
				'`reply_to` varchar(127) NOT NULL, '.
				'`unsubscribe_link` varchar(255) NOT NULL, '.
				'`unsubscribe_email` varchar(127) NOT NULL, '.
				'`is_sent` tinyint(1) unsigned NOT NULL DEFAULT "0", '.
				'`error` smallint(3) unsigned NOT NULL DEFAULT "0", '.
				'PRIMARY KEY (`id`)'.
				') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';
			
			if ( is_null($database) )
				return $statement;
			
			if ( $database->exec($statement) !== false )
				return true;
			
			return false;
		}
	}
	