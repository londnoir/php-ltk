<?php

	/*
	* Libraries/php/LTK/Path.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	/**
	 * Cette classe contient les outils pour manipuler les chemins et les URL.
	 * 
	 * @author LondNoir <londnoir@gmail.com>
	 */
	final class Path
	{
		/** @internal */
		private function __construct () {}

		/**
		 * Retourne une chemin dans les dossiers temporaire. Typiquement sous linux "/tmp/{$append}".
		 *
		 * @param string $append Une chaîne de caractères contenant ce qu'on veut accrocher derrière.
		 * @return string
		 */
		static public function getTemporaryDirectory (string $append = ''): string
		{
			return sys_get_temp_dir() . DIRECTORY_SEPARATOR . $append;
		}

		/**
		 * Vérifie et construit un chemin sur le serveur.
		 *
		 * @param string $path Une chaîne de caractères contenant le chemin complet d'un répertoire.
		 * @param int $permission Un nombre octal pour indiquer les permissions sur le répertoire. Par défaut, la valeur est 0755.
		 * @return bool
		 */
		static public function build (string $path, int $permission = 0755): bool
		{
			$currentDir = null;

			foreach ( explode(DIRECTORY_SEPARATOR, $path) as $dir )
			{
				$parent = $currentDir;
				$currentDir .= $dir.DIRECTORY_SEPARATOR;

				if ( !file_exists($currentDir) )
				{
					if ( !is_writable($parent) )
						return false;

					if ( !mkdir($currentDir, $permission) )
						return false;
				}
				elseif ( !is_dir($currentDir) )
				{
					return false;
				}
			}

			return true;
		}

		/**
		 * Vérifie que le chemin est un dossier et inscriptible.
		 *
		 * @param string $path Une chaîne de caractères contenant le chemin complet d'un répertoire.
		 * @return bool Indique si le répertoire est diponible ou non en écriture.
		 */
		static public function isWritable (string $path): bool
		{
			if ( file_exists($path) === false )
				return false;

			if ( is_dir($path) === false )
				return false;

			if ( is_writable($path) === false )
				return false;

			return true;
		}

		/**
		 * Retourne le nom du fichier en fin d'un chemin ou d'une URL.
		 * La fonction ne retournera rien dans le cas d'un chemin ou d'une URL finissant par "\".
		 * Identique à la fonction php basename()
		 *
		 * @param string $path Une chaîne de caractères contenant un chemin ou une URL.
		 * @return string
		 */
		static public function extractFilename (string $path): string
		{
			if ( $path )
			{
				/* We don't want any variables if URL is passed to this function. */
				$tmp = self::removeVarsFromURL($path);
				
				/* Explode by slashes. */
				$tmp = explode(DIRECTORY_SEPARATOR, $tmp);
				
				/* Return the last part. 
				 * NOTE: this can be null. */
				return end($tmp);
			}

			return '';
		}
		
		/**
		 * Retourne l'extension d'un fichier ou d'un nom de domaine.
		 * La fonction ne retournera rien s'il n'y a effectivement pas d'extension.
		 *
		 * @param string $path Une chaîne de caractères contenant le nom d'un fichier, un chemin complet ou une URL.
		 * @return string
		 */
		static public function extractExtension (string $path): string
		{
			if ( $path && ($tmp = self::extractFilename($path)) )
			{
				$tmp = explode('.', $tmp);
				
				return ( count($tmp) > 1 ) ? end($tmp) : '';
			}

			return '';
		}
		
		/**
		* Retourne le chemin d'un fichier (ou une URL) en retirant le fichier.
		* 
		* @param string $path Une chaîne de caractères contenant le chemin.
		* @param string &$object Une référence à une chaîne de caractères contenant ce qui a été retiré du chemin. Optionnel.
		* @return string
		*/
		static public function extractPath (string $path, string &$object = ''): string
		{
			if ( empty($path) )
				return '';

			$tmp = explode(DIRECTORY_SEPARATOR, $path);
			
			$object = array_pop($tmp);
			
			return implode(DIRECTORY_SEPARATOR, $tmp).DIRECTORY_SEPARATOR;
		}

		/**
		 * Tente de reconstruire le chemin jusqu'à la racine du site sur le serveur depuis la configuration du serveur.
		 *
		 * @param string $append Attache une chaîne derrière le chemin. Optionnel.
		 * @return string
		 */
		static public function getSitePath (string $append = ''): string
		{
			$path = '';

			/* NOTE: If true, then we are in CLI mode. */
			if ( isset($_SERVER['PWD']) )
				$path = $_SERVER['PWD'];
			/* NOTE: If true, then we are in web mode. */
			else if ( isset($_SERVER['DOCUMENT_ROOT']) )
				$path = $_SERVER['DOCUMENT_ROOT'];
			
			/* NOTE: If true, then we are in deep shit... */
			if ( empty($path) )
			{
				trigger_error('Unable to use "php-ltk", the path cannot be determined and autoload will fail !', E_USER_WARNING);

				exit(1);
			}

			/* Add Slash at the end. */
			if ( $path[strlen($path) - 1] !== DIRECTORY_SEPARATOR )
				$path .= DIRECTORY_SEPARATOR;

			return $path.$append;
		}


		/**
		 * Tente de reconstruire l'URL depuis la configuration du serveur.
		 *
		 * @param string $append Attache une chaîne derrière l'URL. Optionnel.
		 * @return string
		 */
		static public function getSiteURL (string $append = ''): string
		{
			if ( !isset($_SERVER['HTTP_HOST']) )
				return '';

			return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].DIRECTORY_SEPARATOR.$append;
		}

		/**
		 * Retourne l'URL absolue du script.
		 *
		 * @return string
		 */
		static public function getAbsoluteURL (): string
		{
			if ( !array_key_exists('HTTP_HOST', $_SERVER) )
				return '';

			return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}

		/**
		 * Permet de construire une URL avec un tableau de paramètre.
		 *
		 * @param string $file L'URL relative ou absolue d'une page web. Dans le cas d'une URL relative, la variable 'site_url' de la class Config sera utilisée.
		 * @param array $params Un tableau associatif où les clés formeront les noms de variables et seront affichées derrière l'URL conformément à la méthode GET.
		 * @param bool $isHTML Indiquez que l'URL sera afficher au sein d'un document html. Les caractères spéciaux seront convertit par des entités html. Par défaut, l'option est activée.
		 * @return string
		 */
		static public function buildURL (string $file, array $params = [], bool $isHTML = true): string
		{
			/* Set the base absolute URL. */
			if ( strstr($file, $_SERVER['REQUEST_SCHEME'].'://') === false )
			{
				$url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].DIRECTORY_SEPARATOR.$file;
			}
			else
			{
				$url = $file;
			}
			
			/* The '&' char is restricted in HTML context. */
			$andChr = $isHTML ? '&amp;' : '&';

			if ( $params )
			{
				/* Assembling vars. */
				$vars = [];

				foreach ( $params as $key => $value )
					$vars[] = $key.'='.$value;

				/* Check for the first char before adding vars. */
				$url .= ( strstr($file, '?') === false ) ? '?' : $andChr;

				/* Adding vars. */
				$url .= implode($andChr, $vars);
			}

			return $url;
		}

		/**
		 * Encode un tableau PHP en chaîne de caractères fiable pour les URL.
		 *
		 * @param array $cleanArray Un tableau à encoder.
		 * @return string
		 */
		static public function arrayFormEncode (array $cleanArray): string
		{
			/* Array to string */
			$encodedString = serialize($cleanArray);
			/* Text compression */
			$encodedString = gzcompress($encodedString, 9);
			/* Base 64 encoding */
			$encodedString = base64_encode($encodedString);
			/* Safe URL conversion */
			return strtr($encodedString, '+/=', '-_,');
		}

		/**
		 * Décode une chaîne de caractères précédemment encodée avec Path::arrayFormEncode() pour récupérer le tableau.
		 *
		 * @param string $encodedString Une chaîne de caractères à décoder.
		 * @return array
		 */
		static public function arrayFormDecode (string $encodedString): array
		{
			/* Get back base64 chars */
			$cleanArray = strtr($encodedString, '-_,', '+/=');
			/* Base 64 decoding */
			$cleanArray = base64_decode($cleanArray);
			/* Text decompression */
			$cleanArray = gzuncompress($cleanArray);
			/* String to array */
			return unserialize($cleanArray);
		}

		/**
		 * Retire les variables d'une URL.
		 *
		 * @param string $url une chaîne de caractères contenant le lien à nettoyer.
		 * @return string
		 */
		static public function removeVarsFromURL (string $url): string
		{
			/* Check for HTML format URL. */
			$url = htmlspecialchars_decode($url, ENT_QUOTES);

			/* Look about the '?' character. */
			$position = strpos($url, '?');

			return ( $position !== false ) ? substr($url, 0, $position) : $url;
		}

		/**
		 * Retourne un tableau contenant les variables d'une URL.
		 *
		 * @param string $url une chaîne de caractères contenant le lien.
		 * @return array
		 */
		static public function extractVarsFromURL (string $url): array
		{
			$results = [];

			/* Check for HTML format URL. */
			$url = htmlspecialchars_decode($url, ENT_QUOTES);

			/* Look about the '?' character. */
			$position = strpos($url, '?');

			if ( $position !== false )
			{
				$string = substr($url, $position + 1);

				if ( $elements = explode('&', $string) )
				{
					foreach ( $elements as $element )
					{
						$tmp = explode('=', $element);

						if ( count($tmp) == 2 )
						{
							$matches = [];

							if ( preg_match('#\[([^]]*)]#', $tmp[0], $matches) )
							{
								$key = str_replace($matches[0], null, $tmp[0]);

								/* Simple array. */
								if ( $matches[0] == '[]' )
								{
									if ( isset($results[$key]) )
										$results[$key][] = $tmp[1];
									else
										$results[$key] = array($tmp[1]);
								}
								/* Key array. */
								else
								{
									if ( isset($results[$key]) )
										$results[$key][$matches[1]] = $tmp[1];
									else
										$results[$key] = array($matches[1] => $tmp[1]);
								}
							}
							else
							{
								/* Simple variable. */
								$results[$tmp[0]] = $tmp[1];
							}
						}
					}
				}
			}

			return $results;
		}

		/**
		 * Convertit un texte en une chaîne de caractères compatible avec les URL.
		 *
		 * @param string $string Une chaîne de caractères à convertir.
		 * @param string $charset charset. Default 'UTF-8'.
		 * @return string
		 */
		static public function stringToURL (string $string, string $charset = 'UTF-8'): string
		{
			$input = [];
			$output = [];

			/* Remove all HTML entities from the string. */
			$string = html_entity_decode($string, ENT_QUOTES, $charset);

			/* Lower case. */
			$string = strtolower($string);

			/* Replaced  chars. */
			$input[] = array('á', 'à', 'ä', 'â');
			$output[] = 'a';
			$input[] = array('é', 'è', 'ë', 'ê');
			$output[] = 'e';
			$input[] = array('í', 'ì', 'ï', 'î');
			$output[] = 'i';
			$input[] = array('ó','ò','ö','ô');
			$output[] = 'o';
			$input[] = array('ú', 'ù', 'ü', 'û');
			$output[] = 'u';
			$input[] = array('ý', 'ÿ');
			$output[] = 'y';
			$input[] = 'ç';
			$output[] = 'c';
			$input[] = 'œ';
			$output[] = 'oe';
			$input[] = '$';
			$output[] = 'dollars';
			$input[] = '€';
			$output[] = 'euros';

			/* Replacement. */
			for ( $i = 0; $i < count($input); $i++ )
				$string = str_replace($input[$i], $output[$i], $string);

			/* Replace all char wich is not alphanumeric by '-' char. */
			$string = preg_replace('#([^A-Za-z0-9-]+)#', '-', $string);

			/* Remove undesirable '-' char. */
			$string = preg_replace('#[-]{2,}#', '-', $string);
			$string = trim($string, '-');

			return $string;
		}
	};
