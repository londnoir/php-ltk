<?php

	/*
	* Libraries/php/LTK/AbstractEnumeration.php
	* This file is part of PHP-LTK 
	*
	* Copyright (C) 2019 - LondNoir <londnoir@gmail.com>
	*
	* PHP-LTK is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2.1 of the License, or (at your option) any later version.
	* 
	* PHP-LTK is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	* 
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	*/

	namespace LTK;

	use JsonSerializable;

	/**
	 * Classe abstraite permettant d'émuler une énumération.
	 *
	 * @author "LondNoir" <londnoir@gmail.com>
	 */
	abstract class AbstractEnumeration implements JsonSerializable
	{
		private string $name;
		private array $values;
		private string $selectedKey;
		
		/**
		 * Construteur privé. 
		 * C'est l'héritage de cette classe qui cadenasse le nom du champ et les options possibles.
		 * 
		 * @param string $name Le nom de l'enumeration.
		 * @param array $values Un tableau associatifs des clés/valeurs disponibles.
		 * @param string $key Clé sélectionnée par défaut.
		 */
		protected function __construct (string $name, array $values, string $key = '')
		{
			$this->name = $name;
			$this->values = $values;

			if ( empty($key) )
				return;

			if ( !$this->setKey($key) )
			{
				$keys = [];
				
				foreach ( $this->values as $key => $value )
					$keys[] = '"'.$key.'"';
				
				trigger_error(__METHOD__.', enumeration "'.$this->name().'" cannot have key "'.$key.'" ! Possibles keys are : '.implode(', ', $keys).'.');

				return;
			}
		}

		/**
		 * Méthode *magique* pour un affichage pratique avec les fonctions PHP print_r() et var_dump().
		 * Le tableau contiendra 'name' pour le nom de l'énumération, 'selected_key' pour la clé choisie
		 * et 'selected_value' pour la valeur réelle derrière la clé choisie.
		 *
		 * @return array
		 */
		public function __debugInfo (): array
		{
			return [
				'name' => $this->name,
				'selected_key' => $this->selectedKey,
				'selected_value' => $this->value()
			];
		}

		/**
		 * Méthode *magique* qui donne la valeur finale quand l'objet est traité comme une chaîne de caractères.
		 *
		 * @return string
		 */
		public function __toString (): string
		{
			return strval($this->value());
		}

		/**
		 * Retourne le nom de l'énumération.
		 *
		 * @return string
		 */
		public function name (): string
		{
			return $this->name;
		}

		/**
		* Retourne le tableau des clés/valeurs disponibles dans l'énumération.
		*
		* @return array
		*/
		public function values (): array
		{
			return $this->values;
		}

		/**
		 * Sélectionne une clé dans celles disponibles. 
		 * La liste des clés peuvent être consultées via la méthode AbstractEnumeration::values().
		 *
		 * @param string $key La clé.
		 * @return bool
		 */
		public function setKey (string $key): bool
		{
			if ( array_key_exists($key, $this->values) )
			{
				$this->selectedKey = $key;

				return true;
			}

			return false;
		}

		/**
		 * Retourne la clé sélectionnée.
		 *
         * @note Peut-être vide.
		 * @return string
		 */
		public function selectedKey (): string
		{
			return $this->selectedKey;
		}

		/**
		 * Retourne la valeur de clé sélectionnée.
		 *
		 * @return mixed
		 */
		public function value (): mixed
		{
			if ( empty($this->selectedKey) )
				return null;

			return $this->values[$this->selectedKey];
		}

		/**
		 * Méthode magique pour l'usage avec json_encode().
		 *
		 * @return mixed
		 */
		public function jsonSerialize (): mixed
		{
			return $this->value();
		}
	};
